<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property string $driver_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile_number
 * @property integer $is_active
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['first_name', 'last_name', 'email', 'mobile_number'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'driver_id' => 'Driver ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'mobile_number' => 'Mobile Number',
            'is_active' => 'Is Active',
        ];
    }
    
    public static function getDriver($driver_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    
    	if(isset($driver_id) && $driver_id!='')
    	{
    		$condition .=  $join.'d.driver_id=:driver_id' ;
    		$params[':driver_id'] = $driver_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'d.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$driverlist = (new \yii\db\Query())
    	->select('d.*')
    	->from('driver d')
    	->where($condition,$params)
    	->all();
    	return $driverlist;
    }
    
    
    
    /* Validate user and return user data
     * @param unknown $api_key_data
     * @return unknown
     */
    public static function validateUser($api_key_data)
    { 
    	$condition='';
    	$params=array();
    	$join='';
    	if(isset($api_key_data[0]) && $api_key_data[0]){
    		$condition.=$join.'d.driver_id=:driver_id';
    		$params[':driver_id']=trim($api_key_data[0],'#');
    		$join=' AND ';
    	}
    	if(isset($api_key_data[2]) && $api_key_data[2]){
    		$condition.=$join.'d.last_password_changed=:last_password_changed';
    		$last_login_date=trim($api_key_data[2],"#");
    		$params[':last_password_changed']=date('Y-m-d H:i:s',strtotime($last_login_date));
    		$join=' AND ';
    	}
    	$validateuser = (new \yii\db\Query())
    	->select('d.*')
    	->from('driver d')
    	->where($condition,$params)
    	->all();
    	return $validateuser;
    }
}
