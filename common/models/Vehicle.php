<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vehicle".
 *
 * @property string $vehicle_id
 * @property string $vehicletype_id
 * @property string $shop_id
 * @property string $vehicle_name
 * @property string $vehiclereg_no
 * @property string $created_on
 * @property string $updated_on
 * @property string $created_by
 * @property string $updated_by
 * @property integer $is_active
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vehicletype_id', 'shop_id', 'vehicle_name', 'vehiclereg_no'], 'required'],
            [['vehicletype_id', 'shop_id', ], 'integer'],
            
            [['vehicle_name', 'vehiclereg_no'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vehicle_id' => 'Vehicle ID',
            'vehicletype_id' => 'Vehicletype ID',
            'shop_id' => 'Shop ID',
            'vehicle_name' => 'Vehicle Name',
            'vehiclereg_no' => 'Vehiclereg No',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
        ];
    }
    
    public static function getVehicle($vehicletype_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if(!empty($vehicletype_id))
    	{
    		$condition .=  $join.'v.vehicletype_id=:vehicletype_id' ;
    		$params[':vehicletype_id'] = $vehicletype_id;
    		$join = ' and ' ;
    		 
    	}
    	if($is_active!='')
    	{
    		$condition .=  $join.'v.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    
    	$categorylist = (new \yii\db\Query())
    	->select('v.*')
    	->from('vehicle v')
    	->where($condition,$params)
    	->all();
    	return $categorylist;
    }
}
