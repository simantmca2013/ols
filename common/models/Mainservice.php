<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mainservice".
 *
 * @property integer $mainservice_id
 * @property string $mainservice_name
 * 
 * @property string $mainservice_description
 * @property string $mainservice_img_path
 * @property string $created_on
 * @property string $updated_on
 * @property string $created_by
 * @property string $updated_by
 * @property integer $is_defaultservice
 * @property integer $is_active
 */
class Mainservice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mainservice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mainservice_name', 'mainservice_img_path'], 'required'],
            [['mainservice_name'], 'string', 'max' => 156],
            [['mainservice_description', 'mainservice_img_path'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mainservice_id' => 'Mainservice ID',
            'mainservice_name' => 'Mainservice Name',
            
            'mainservice_description' => 'Mainservice Description',
            'mainservice_img_path' => 'Mainservice Img Path',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'is_defaultservice' => 'Is Defaultservice',
            'is_active' => 'Is Active',
        ];
    }
    
    
    public static function getMainservice($is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if($is_active!='')
    	{
    		$condition .=  $join.'ms.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    
    	$mainservicelist = (new \yii\db\Query())
    	->select('ms.*')
    	->from('mainservice ms')
    	->where($condition,$params)
    	->all();
    	return $mainservicelist;
    }
    
    public static function getDefailtMainservice($mainservice_id=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if(!empty($mainservice_id))
    	{
    		$condition .=  $join.'ms.mainservice_id !=:mainservice_id' ;
    		$params[':mainservice_id'] = $mainservice_id;
    		$join = ' and ' ;
    		 
    	}
    	$condition .=  $join.'ms.is_defaultservice =:is_defaultservice' ;
    	$params[':is_defaultservice'] = 1;
    	$join = ' and ' ;
    	
    	$mainservicelist = (new \yii\db\Query())
    	->select('count(ms.mainservice_id) as isdefaultcount')
    	->from('mainservice ms')
    	->where($condition,$params)
    	->all();
    	return $mainservicelist[0]['isdefaultcount'];
    }
}
