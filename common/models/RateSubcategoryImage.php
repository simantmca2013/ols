<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rate_subcategory_image".
 *
 * @property string $rate_subcategory_image_id
 * @property string $subcategory_id
 * @property string $img_path
 */
class RateSubcategoryImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rate_subcategory_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'img_path'], 'required'],
            [['subcategory_id'], 'integer'],
            [['img_path'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rate_subcategory_image_id' => 'Rate Subcategory Image ID',
            'subcategory_id' => 'Subcategory ID',
            'img_path' => 'Img Path',
        ];
    }
    public static function getRateSubcategoryImage($shop_id=null,$subcategory_id=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if(isset($shop_id) && $shop_id!='')
    	{
    		$condition .=  $join.'rsi.shop_id=:shop_id' ;
    		$params[':shop_id'] = $shop_id;
    		$join = ' and ' ;
    		 
    	}
    	
    	if(isset($subcategory_id) && $subcategory_id!='')
    	{
    		$condition .=  $join.'rsi.subcategory_id=:subcategory_id' ;
    		$params[':subcategory_id'] = $subcategory_id;
    		$join = ' and ' ;
    		 
    	}
    	 
    	$images = (new \yii\db\Query())
    	->select('rsi.*')
    	->from('rate_subcategory_image rsi')
    	->where($condition,$params)
    	->all();
    	return $images;
    }
}
