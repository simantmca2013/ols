<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "measurement".
 *
 * @property string $measurement_id
 * @property string $measurement_name
 * @property string $measurement_sort_name
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_active
 */
class Measurement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measurement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['measurement_name', 'measurement_sort_name'], 'required'],
            [['created_by', 'is_active'], 'integer'],
            [['measurement_name', 'measurement_sort_name'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'measurement_id' => 'Measurement ID',
            'measurement_name' => 'Measurement Name',
            'measurement_sort_name' => 'Measurement Sort Name',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     * Get all country filtered by parameter
     * @param unknown $country_id
     * @param unknown $is_active
     */
    public static function getMeasurement($measurement_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	 
    	if(isset($measurement_id) && $measurement_id!='')
    	{
    		$condition .=  $join.'m.measurement_id=:measurement_id' ;
    		$params[':measurement_id'] = $measurement_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'m.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$measurementlist = (new \yii\db\Query())
    	->select('m.*')
    	->from('measurement m')
    	->where($condition,$params)
    	->all();
    	return $measurementlist;
    }
}
