<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rate".
 *
 * @property string $rate_id
 * @property string $subcategory_id
 * @property string $measurement_id
 * @property string $cost
 * @property string $description
 * @property string $location
 */
class Rate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'measurement_id', 'cost', 'description', 'location'], 'required'],
            [['subcategory_id', 'measurement_id'], 'integer'],
            [['cost'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 512],
            [['location'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rate_id' => 'Rate ID',
            'subcategory_id' => 'Subcategory ID',
            'measurement_id' => 'Measurement ID',
            'cost' => 'Cost',
            'description' => 'Description',
            'location' => 'Location',
        ];
    }
    public static function getRate($shop_id=null,$subcategory_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if($shop_id!=''){
    		$condition .=  $join.'r.shop_id=:shop_id' ;
    		$params[':shop_id'] = $shop_id;
    		$join = ' and ' ;
    	}
    	if(isset($subcategory_id) && $subcategory_id!='')
    	{
    		$condition .=  $join.'r.subcategory_id=:subcategory_id' ;
    		$params[':subcategory_id'] = $subcategory_id;
    		$join = ' and ' ;
    		 
    	}
    	
    	$ratedetails = (new \yii\db\Query())
    	->select('r.*')
    	->from('rate r')
    	->where($condition,$params)
    	->all();
    	return $ratedetails;
    }
}
