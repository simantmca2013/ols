<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property string $country_id
 * @property string $country_name
 * @property string $country_sort_name
 * @property string $country_code
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_active
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_name', 'country_sort_name', 'country_code','is_active'], 'required'],
            [['country_name'], 'string', 'max' => 256],
            [['country_sort_name', 'country_code'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => 'Country ID',
            'country_name' => 'Country Name',
            'country_sort_name' => 'Country Sort Name',
            'country_code' => 'Country Code',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     * Get all country filtered by parameter
     * @param unknown $country_id
     * @param unknown $is_active
     */
    public static function getCountry($country_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	
    	if(isset($country_id) && $country_id!='')
    	{
    		$condition .=  $join.'c.country_id=:country_id' ;
    		$params[':country_id'] = $country_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'c.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$countrylist = (new \yii\db\Query())
    	->select('c.*')
    	->from('country c')
    	->where($condition,$params)
    	->all();
    	return $countrylist;
    }
}
