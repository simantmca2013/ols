<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property string $city_id
 * @property string $country_id
 * @property string $state_id
 * @property string $city_name
 * @property string $city_sort_name
 * @property string $city_code
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_active
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'state_id', 'city_name', 'city_sort_name', 'city_code'], 'required'],
            [['country_id', 'state_id'], 'integer'],
            [['city_name'], 'string', 'max' => 512],
            [['city_sort_name', 'city_code'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'City ID',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'city_name' => 'City Name',
            'city_sort_name' => 'City Sort Name',
            'city_code' => 'City Code',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     *  get all city filtered by parameter
     * @param unknown $country_id
     * @param unknown $state_id
     * @param unknown $city_id
     * @param unknown $is_active
     */
    public static function getCity($country_id=null,$state_id=null,$city_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    
    	if(isset($country_id) && $country_id!='')
    	{
    		$condition .=  $join.'con.country_id=:country_id' ;
    		$params[':country_id'] = $country_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($state_id) && $state_id!='')
    	{
    		$condition .=  $join.'con.state_id=:state_id' ;
    		$params[':state_id'] = $state_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($city_id) && $city_id!='')
    	{
    		$condition .=  $join.'con.city_id=:city_id' ;
    		$params[':city_id'] = $city_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'con.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$statelist = (new \yii\db\Query())
    	->select('con.*,c.country_name,c.country_sort_name,c.country_code,s.state_name,s.state_sort_name,s.state_code')
    	->from('city con')
    	->Leftjoin('state s','s.state_id=con.state_id')
    	->Leftjoin('country c','c.country_id=con.country_id')
    	->where($condition,$params)
    	->all();
    	return $statelist;
    }
}
