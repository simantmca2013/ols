<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vehicletype".
 *
 * @property string $vehicletype_id
 * @property string $vehicletype_name
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_active
 */
class Vehicletype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicletype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vehicletype_name'], 'required'],
            [['created_on', 'updated_on'], 'safe'],
            [['vehicletype_name'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vehicletype_id' => 'Vehicletype ID',
            'vehicletype_name' => 'Vehicletype Name',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_active' => 'Is Active',
        ];
    }
    
    public static function getVehicletype($is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if($is_active!='')
    	{
    		$condition .=  $join.'v.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    
    	$categorylist = (new \yii\db\Query())
    	->select('v.*')
    	->from('vehicletype v')
    	->where($condition,$params)
    	->all();
    	return $categorylist;
    }
}
