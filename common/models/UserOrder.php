<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_order".
 *
 * @property string $order_id
 * @property string $shop_id
 * @property string $mainservice_id
 * @property string $category_id
 * @property string $subcategory_id
 * @property string $measurement_id
 * @property string $quantity
 * @property string $location
 * @property string $created_at
 * @property integer $is_active
 */
class UserOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'category_id', 'subcategory_id', 'measurement_id', 'is_active'], 'integer'],
            [['created_at'], 'safe'],
            [['quantity'], 'string', 'max' => 156],
            [['location'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'shop_id' => 'Shop ID',
            'category_id' => 'Category ID',
            'subcategory_id' => 'Subcategory ID',
            'measurement_id' => 'Measurement ID',
            'quantity' => 'Quantity',
            'location' => 'Location',
            'created_at' => 'Created At',
            'is_active' => 'Is Active',
        ];
    }
}
