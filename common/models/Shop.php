<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shop".
 *
 * @property string $shop_id
 * @property string $shop_name
 * @property string $shop_sort_name
 * @property string $mobile_number
 * @property string $contact_number
 * @property string $email
 * @property string $country_id
 * @property string $state_id
 * @property string $city_id
 * @property string $pin_code
 * @property string $address
 * @property string $govt_id_proof
 * @property string $shop_image
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_active
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           /* [['shop_name', 'mobile_number', 'contact_number', 'email', 'country_id', 'state_id', 'city_id', 'pin_code', 'address'], 'required'],
            [['country_id', 'state_id', 'city_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['shop_name'], 'string', 'max' => 256],
            [[ 'mobile_number', 'contact_number', 'email', 'pin_code'], 'string', 'max' => 156],
            [['address'], 'string', 'max' => 512]*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           
            'shop_name' => 'Shop Name',
            
            'mobile_number' => 'Mobile Number',
            'contact_number' => 'Contact Number',
            'email' => 'Email',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'city_id' => 'City ID',
            'pin_code' => 'Pin Code',
            'address' => 'Address',
            'govt_id_proof' => 'Govt Id Proof',
            'shop_image' => 'Shop Image',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     * Get shop filtered by parameter
     * @param unknown $shop_id
     * @param unknown $is_active
     */
    public static function getShop($shop_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	 
    	if(isset($shop_id) && $shop_id!='')
    	{
    		$condition .=  $join.'s.shop_id=:shop_id' ;
    		$params[':shop_id'] = $shop_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'s.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$shoplist = (new \yii\db\Query())
    	->select('s.*')
    	->from('shop s')
    	->where($condition,$params)
    	->all();
    	return $shoplist;
    }
    
    
  /* Validate user and return user data
    * @param unknown $api_key_data
    * @return unknown
    */
    public static function validateUser($api_key_data)
    {
    	$condition='';
    	$params=array();
    	$join='';
    	if(isset($api_key_data[0]) && $api_key_data[0]){
    		$condition.=$join.'v.shop_id=:shop_id';
    		$params[':shop_id']=trim($api_key_data[0],'#');
    		$join=' AND ';
    	}
    	if(isset($api_key_data[2]) && $api_key_data[2]){
    		$condition.=$join.'v.last_password_changed=:last_password_changed';
    		$last_login_date=trim($api_key_data[2],"#");
    		$params[':last_password_changed']=date('Y-m-d H:i:s',strtotime($last_login_date));
    		$join=' AND ';
    	}
    	$validateuser = (new \yii\db\Query())
    	->select('v.*')
    	->from('shop v')
    	->where($condition,$params)
    	->all();
    	return $validateuser;
    }
}
