<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property string $category_image
 * @property integer $is_active
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name', 'category_image'], 'required'],
            [['category_name'], 'string', 'max' => 256],
            [['category_image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'category_image' => 'Category Image',
            'is_active' => 'Is Active',
        ];
    }
    
    public static function getCategory($is_active=null,$mainservice_id=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if($mainservice_id!='')
    	{
    		$condition .=  $join.'c.mainservice_id=:mainservice_id' ;
    		$params[':mainservice_id'] = $mainservice_id;
    		$join = ' and ' ;
    		 
    	}
    	if($is_active!='')
    	{
    		$condition .=  $join.'c.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	 
    	$categorylist = (new \yii\db\Query())
    	->select('c.*,m.mainservice_name')
    	->from('category c')
    	->Leftjoin('mainservice m','m.mainservice_id=c.mainservice_id')
    	->where($condition,$params)
    	->all();
    	return $categorylist;
    }
}
