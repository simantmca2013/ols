<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "servicemeasurement".
 *
 * @property string $servicemeasurement_id
 * @property string $category_id
 * @property string $measurement_id
 */
class Servicemeasurement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicemeasurement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'measurement_id'], 'required'],
            [['category_id', 'measurement_id'], 'integer'],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'servicemeasurement_id' => 'Servicemeasurement ID',
            'category_id' => 'Category ID',
            'measurement_id' => 'Measurement ID',
           
        ];
    }
    
    public static function getServiceMeasurement($category_id=null,$measurement_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if($category_id!='')
    	{
    		$condition .=  $join.'sm.category_id=:category_id' ;
    		$params[':category_id'] = $category_id;
    		$join = ' and ' ;
    		 
    	}
    	if($measurement_id!='')
    	{
    		$condition .=  $join.'sm.measurement_id=:measurement_id' ;
    		$params[':measurement_id'] = $measurement_id;
    		$join = ' and ' ;
    		 
    	}
    	if($is_active!='')
    	{
    		$condition .=  $join.'sm.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    
    	$categorylist = (new \yii\db\Query())
    	->select('sm.*,m.measurement_name')
    	->from('servicemeasurement sm')
    	->Leftjoin('measurement m','m.measurement_id=sm.measurement_id')
    	->where($condition,$params)
    	->all();
    	return $categorylist;
    }
}
