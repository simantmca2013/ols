<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property string $state_id
 * @property string $country_id
 * @property string $state_name
 * @property string $state_sort_name
 * @property string $state_code
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_active
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'state_name', 'state_sort_name', 'state_code','is_active'], 'required'],
            [['country_id','is_active'], 'integer'],
            [['state_name'], 'string', 'max' => 512],
            [['state_sort_name', 'state_code'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'state_id' => 'State ID',
            'country_id' => 'Country ID',
            'state_name' => 'State Name',
            'state_sort_name' => 'State Sort Name',
            'state_code' => 'State Code',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     * Get all state filter by parameter
     * @param unknown $country_id
     * @param unknown $state_id
     * @param unknown $is_active
     */
    public static function getState($country_id=null,$state_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	 
    	if(isset($country_id) && $country_id!='')
    	{
    		$condition .=  $join.'s.country_id=:country_id' ;
    		$params[':country_id'] = $country_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($state_id) && $state_id!='')
    	{
    		$condition .=  $join.'s.state_id=:state_id' ;
    		$params[':state_id'] = $state_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'s.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$statelist = (new \yii\db\Query())
    	->select('s.*,c.country_name,c.country_sort_name,c.country_code')
    	->from('state s')
    	->Leftjoin('country c','c.country_id=s.country_id')
    	->where($condition,$params)
    	->all();
    	return $statelist;
    }
}
