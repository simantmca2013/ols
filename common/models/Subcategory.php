<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subcategory".
 *
 * @property integer $subcategory_id
 * @property integer $category_id
 * @property string $subcategory_name
 * @property string $subcategory_image
 * @property integer $is_active
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_on
 * @property string $updated_on
 */
class Subcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'subcategory_name', 'subcategory_image', 'is_active'], 'required'],
            [['category_id', 'is_active'], 'integer'],
            [['subcategory_name'], 'string', 'max' => 256],
            [['subcategory_image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subcategory_id' => 'Subcategory ID',
            'category_id' => 'Category ID',
            'subcategory_name' => 'Subcategory Name',
            'subcategory_image' => 'Subcategory Image',
            'is_active' => 'Is Active',
           
        ];
    }
    /**
     *  get all subcategory filterd by parameter
     * @param unknown $subcategory_id
     * @param unknown $category_id
     * @param unknown $is_active
     */
    public static function getSubcategory($subcategory_id=null,$category_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if(isset($subcategory_id) && $subcategory_id!='')
    	{
    		$condition .=  $join.'sc.subcategory_id=:subcategory_id' ;
    		$params[':subcategory_id'] = $subcategory_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($category_id) && $category_id!='')
    	{
    		$condition .=  $join.'sc.category_id=:category_id' ;
    		$params[':category_id'] = $category_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'sc.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$subcategorylist = (new \yii\db\Query())
    	->select('sc.subcategory_id,sc.subcategory_name,sc.subcategory_image,sc.is_active,c.category_id,c.category_name,m.mainservice_id,m.mainservice_name')
    	->from('subcategory sc')
    	->Leftjoin('category c','c.category_id=sc.category_id')
    	->Leftjoin('mainservice m','m.mainservice_id=sc.mainservice_id')
    	->where($condition,$params)
    	->all();
    	return $subcategorylist;
    }
}
