<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shop_bid_reply".
 *
 * @property string $vendre_bid_reply_id
 * @property string $app_user_bid_id
 * @property string $shop_id
 * @property string $cost
 */
class ShopBidReply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_bid_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['app_user_bid_id', 'shop_id'], 'integer'],
            [['cost'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendre_bid_reply_id' => 'Vendre Bid Reply ID',
            'app_user_bid_id' => 'App User Bid ID',
            'shop_id' => 'Shop ID',
            'cost' => 'Cost',
        ];
    }
}
