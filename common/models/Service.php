<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property string $service_id
 * @property string $category_id
 * @property string $subcategory_id
 * @property string $shop_id
 * @property integer $rating
 * @property string $service_location
 * @property string $service_description
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_multimeasurement
 * @property integer $is_active
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'subcategory_id', 'shop_id'], 'required'],
            [['category_id', 'subcategory_id', 'shop_id'], 'integer'],
           
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'category_id' => 'Category ID',
            'subcategory_id' => 'Subcategory ID',
            'shop_id' => 'Shop ID',
            'rating' => 'Rating',
            'service_location' => 'Service Location',
            'service_description' => 'Service Description',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'is_multimeasurement' => 'Is Multimeasurement',
            'is_active' => 'Is Active',
        ];
    }
    
    
    public static function getService($category_id=null,$subcategory_id=null,$shop_id=null,$is_active=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	 
    	if(isset($category_id) && $category_id!='')
    	{
    		$condition .=  $join.'s.category_id=:category_id' ;
    		$params[':category_id'] = $category_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($subcategory_id) && $subcategory_id!='')
    	{
    		$condition .=  $join.'s.subcategory_id=:subcategory_id' ;
    		$params[':subcategory_id'] = $subcategory_id;
    		$join = ' and ' ;
    		 
    	}
    	
    	if(isset($shop_id) && $shop_id!='')
    	{
    		$condition .=  $join.'s.shop_id=:shop_id' ;
    		$params[':shop_id'] = $shop_id;
    		$join = ' and ' ;
    		 
    	}
    	if(isset($is_active) && $is_active!='')
    	{
    		$condition .=  $join.'s.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    		 
    	}
    	$countrylist = (new \yii\db\Query())
    	->select('s.*')
    	->from('service s')
    	->where($condition,$params)
    	->all();
    	return $countrylist;
    }
    
   
    
}
