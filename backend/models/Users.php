<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $time_limit
 * @property integer $is_active
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'password', 'role', 'is_active'], 'required'],
            [['role'], 'string'],
            [['time_limit'], 'safe'],
            [['is_active'], 'integer'],
            [['name', 'email', 'password'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'role' => 'Role',
            'time_limit' => 'Time Limit',
            'is_active' => 'Is Active',
        ];
    }
    /**
     *  this function return login reply
     * @return number|boolean
     */
    public function login($username,$password)
    { 
    	$users = Users::findAll(array('email'=>$username  , 'password'=>md5($password)));
    	if(count($users) > 0)
    	{
    		if($users[0]->is_active==0){
    			return 3;
    		}else{
    			Yii::$app->session->set('email', $users[0]->email);
    			Yii::$app->session->set('name', $users[0]->name);
    			Yii::$app->session->set('user_id', $users[0]->user_id);
    			Yii::$app->session->set('role', $users[0]->role);
    			Yii::$app->session->set('is_active', $users[0]->is_active);
    			return 1;
    		}
    		 
    	}
    	else
    	{
    		return false;
    
    	}
    }
    
    /**
     *  Check Duplicate Email Id
     */
    public static  function isduplicateUser($email=null,$user_id=null)
    {  
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if(isset($user_id) && $user_id)
    	{
    		$condition .=  $join.'u.user_id !=:user_id' ;
    		$params[':user_id'] = $user_id ;
    		$join = ' and ' ;
    	
    	}
    	if(isset($email) && $email)
    	{
    		$condition .=  $join.'u.email=:email' ;
    		$params[':email'] = $email ;
    		$join = ' and ' ;
    	
    	}
    	$userlist = (new \yii\db\Query())
    	->select('u.*')
    	->from('user u')
    	->where($condition,$params)
    	->all();
    	if(count($userlist)>0)
    	{ 
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    /**
     * User List
     */
    public function getUser($is_active=null,$user_id=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	if(isset($is_active) && $is_active)
    	{
    		$condition .=  $join.'u.is_active=:is_active' ;
    		$params[':is_active'] = $is_active ;
    		$join = ' and ' ;
    
    	}
    	if(isset($user_id) && $user_id)
    	{
    		$condition .=  $join.'u.user_id=:user_id' ;
    		$params[':user_id'] = $user_id ;
    		$join = ' and ' ;
    
    	}
    	$userlist = (new \yii\db\Query())
    	->select('u.*')
    	->from('user u')
    	->where($condition,$params)
    	->all();
    	return $userlist;
    }
    
    /**
     *  User Authentication 
     */
    public static function getauthverificationstatus($email,$user_id){
    	$params=array();
    	$condition="";
    	$join="";
    	if(isset($email) && $email)
    	{
    		$condition.=$join." u.email=:email";
    		$params[':email']=$email;
    		$join=" and";
    		 
    	}
    	if(isset($user_id) && $user_id)
    	{
    		$condition.=$join." u.user_id=:user_id";
    		$params[':user_id']=$user_id;
    		$join=" and";
    		 
    	}
    	 
    	$register_users = (new \yii\db\Query())
    	->select('u.*')
    	->from('user u')
    	->where($condition,$params)
    	->all();
    	if(count($register_users)>0)
    	{
    		return true;
    	}
    	else{
    		 
    		return false;
    	}
    	
    }
}
