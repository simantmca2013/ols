
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['state_id']) && $_REQUEST['state_id']){?>
			Edit State
			<?php }else{?>
			Add State
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managestate/onpost" method="post" id="state_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
							<label class="col-sm-3 control-label">Country</label>
								<div class="col-sm-5">
									<select class="form-control" name="country_id" required="">
										<option value>Select Country</option>
										<?php foreach ($countrylist as $country){?>
										<option value="<?php echo $country['country_id'];?>" <?php if(isset($statedetails->country_id) && $statedetails->country_id==$country['country_id']){?> selected <?php }?>>
										<?php echo $country['country_name'];?></option>
										<?php }?>					
									</select>
								</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">State Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="state_name"
								id="statename" placeholder="State Name" 
								value="<?php if(isset($statedetails->state_name) && $statedetails->state_name){ echo $statedetails->state_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">State Sort Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="state_sort_name"
								id="statesortname" placeholder="State Sort Name" 
								value="<?php if(isset($statedetails->state_sort_name) && $statedetails->state_sort_name){ echo $statedetails->state_sort_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">State Code </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="state_code"
								id="statecodename" placeholder="State Code" 
								value="<?php if(isset($statedetails->state_code) && $statedetails->state_code){ echo $statedetails->state_code;}?>" Required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($statedetails->is_active) && $statedetails->is_active==1){ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($statedetails->state_id) && $statedetails->state_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($statedetails->state_id) && $statedetails->state_id){ ?> 
										<input type="hidden"
											value="<?php echo $statedetails->state_id; ?>" name="state_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});


</script>

