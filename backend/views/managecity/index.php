<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['city_id']) && $_REQUEST['city_id']){?>
			Edit City
			<?php }else{?>
			Add City
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managecity/onpost" method="post" id="city_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Country</label>
							<div class="col-sm-5">
								<select class="form-control" name="country_id" id="countryId" required="">
									<option value>Select Country</option>
									<?php foreach ($countrylist as $country){?>
									<option value="<?php echo $country['country_id'];?>" <?php if(isset($citydetails->country_id) && $citydetails->country_id==$country['country_id']){?> selected <?php }?>>
									<?php echo $country['country_name'];?></option>
									<?php }?>					
								</select>
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">State</label>
							<div class="col-sm-5">
								<select class="form-control" name="state_id" id="stateId" required="">
									<option value>Select State</option>
												
								</select>
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">City Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="city_name"
								id="cityname" placeholder="City Name" 
								value="<?php if(isset($citydetails->city_name) && $citydetails->city_name){ echo $citydetails->city_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">City Sort Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="city_sort_name"
								id="citysortname" placeholder="City Sort Name" 
								value="<?php if(isset($citydetails->city_sort_name) && $citydetails->city_sort_name){ echo $citydetails->city_sort_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">City Code </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="city_code"
								id="citycodename" placeholder="City Code" 
								value="<?php if(isset($citydetails->city_code) && $citydetails->city_code){ echo $citydetails->city_code;}?>" Required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($citydetails->is_active) && $citydetails->is_active==1){ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($citydetails->city_id) && $citydetails->city_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($citydetails->city_id) && $citydetails->city_id){ ?> 
										<input type="hidden"
											value="<?php echo $citydetails->city_id; ?>" name="city_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});
$( document ).ready(function() {
	getState($("#countryId").val());
});
$("#countryId").change(function(){
	getState($("#countryId").val());
	
});


function getState(country_id)
{
	 $.ajax({
	        type: "POST",
	        url: "<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/managestate/getstatelist",
	        data: {'country_id':  country_id},
	        context: this,
	 		success: function(response){
	 		 var obj =  jQuery.parseJSON(response) ;
	 	     $("#stateId").empty();
	 	     $("#stateId").append(new Option("select city",""));
	 	     if(obj.length > 0 )
	 	     {
	 	         for(var i = 0 ; i < obj.length ; i++)
	 	         {
	 		     	  $("#stateId").append(new Option(obj[i].state_name, obj[i].state_id));
	 	               <?php if(isset($citydetails->state_id) && $citydetails->state_id){?>
	 	                if(obj[i].state_id == "<?php echo $citydetails->state_id; ?>") {  $("#stateId").val(obj[i].state_id);} 
	 	               <?php }?>
	 	          }
	 	                     
	 	      }
	 		
			}
	      }) ;
	
}

</script>

