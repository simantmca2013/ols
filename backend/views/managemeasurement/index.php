
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['measurement_id']) && $_REQUEST['measurement_id']){?>
			Edit Measurement
			<?php }else{?>
			Add Measurement
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managemeasurement/onpost" method="post" id="measurement_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Measurement Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="measurement_name"
								id="measurementname" placeholder="Measurement Name" 
								value="<?php if(isset($measurementdetails->measurement_name) && $measurementdetails->measurement_name){ echo $measurementdetails->measurement_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Measurement Sort Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="measurement_sort_name"
								id="measurementsortname" placeholder="Measurement Sort Name" 
								value="<?php if(isset($measurementdetails->measurement_sort_name) && $measurementdetails->measurement_sort_name){ echo $measurementdetails->measurement_sort_name;}?>" Required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($measurementdetails->is_active) && $measurementdetails->is_active==1){ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($measurementdetails->measurement_id) && $measurementdetails->measurement_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($measurementdetails->measurement_id) && $measurementdetails->measurement_id){ ?> 
										<input type="hidden"
											value="<?php echo $measurementdetails->measurement_id; ?>" name="measurement_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});


</script>

