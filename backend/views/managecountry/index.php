
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['country_id']) && $_REQUEST['country_id']){?>
			Edit Country
			<?php }else{?>
			Add Country
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managecountry/onpost" method="post" id="country_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Country Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="country_name"
								id="countryname" placeholder="Country Name" 
								value="<?php if(isset($countrydetails->country_name) && $countrydetails->country_name){ echo $countrydetails->country_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Country Sort Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="country_sort_name"
								id="countrysortname" placeholder="Country Sort Name" 
								value="<?php if(isset($countrydetails->country_sort_name) && $countrydetails->country_sort_name){ echo $countrydetails->country_sort_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Country Code </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="country_code"
								id="countrycode" placeholder="Country Code" 
								value="<?php if(isset($countrydetails->country_code) && $countrydetails->country_code){ echo $countrydetails->country_code;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($countrydetails->is_active) && $countrydetails->is_active==1){ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($countrydetails->country_id) && $countrydetails->country_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($countrydetails->country_id) && $countrydetails->country_id){ ?> 
										<input type="hidden"
											value="<?php echo $countrydetails->country_id; ?>" name="country_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});


</script>

