
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['vehicle_id']) && $_REQUEST['vehicle_id']){?>
			Edit Vehicle
			<?php }else{?>
			Add Vehicle
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managevehicle/onpost" method="post" id="vehicle_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Vehicle Type</label>
							<div class="col-sm-5">
								<select class="form-control" name="vehicletype_id" id="vehicletypeId" required="">
									<option value>Select Vehicle Type</option>
									<?php foreach ($vehicletypelist as $vehicletype){?>
									<option value="<?php echo $vehicletype['vehicletype_id'];?>" <?php if(isset($vehicledetails->vehicletype_id) && $vehicledetails->vehicletype_id==$vehicletype['vehicletype_id']){?> selected <?php }?>>
									<?php echo $vehicletype['vehicletype_name'];?></option>
									<?php }?>					
								</select>
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Shop</label>
							<div class="col-sm-5">
								<select class="form-control" name="shop_id" id="shopId" required="">
									<option value>Select State</option>
									<?php foreach ($shoplist as $shop){?>
									<option value="<?php echo $shop['shop_id'];?>" <?php if(isset($vehicledetails->vehicletype_id) && $vehicledetails->vehicletype_id==$shop['shop_id']){?> selected <?php }?>>
									<?php echo $shop['shop_name'];?></option>
									<?php }?>			
								</select>
							</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Vehicle Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="vehicle_name"
								id="vehiclename" placeholder="Vehicle Name" 
								value="<?php if(isset($vehicledetails->vehicle_name) && $vehicledetails->vehicle_name){ echo $vehicledetails->vehicle_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Vehicle Registration Number </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="vehiclereg_no"
								id="vehicleregno" placeholder="Vehicle Registration Number" 
								value="<?php if(isset($vehicledetails->vehiclereg_no) && $vehicledetails->vehiclereg_no){ echo $vehicledetails->vehiclereg_no;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($vehicledetails->is_active) && $vehicledetails->is_active==1){ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($vehicledetails->vehicle_id) && $vehicledetails->vehicle_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($vehicledetails->vehicle_id) && $vehicledetails->vehicle_id){ ?> 
										<input type="hidden"
											value="<?php echo $vehicledetails->vehicle_id; ?>" name="vehicle_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});


</script>

