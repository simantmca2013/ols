<?php if(Yii::$app->session->getFlash('msg')) { ?>
		<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
		<?php  echo Yii::$app->session->getFlash('msg'); ?>
		</div>
<?php }?>
<h3>Subservice type Listing</h3>
		<br />
		<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/managesubcategory?act=add" class="btn btn-primary mb10">
			<i class="entypo-plus"></i>
			Add Subservice type
		</a>
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>S.No</th>
					<th>Subservice type Name </th>
					<th>Mainservice Name </th>
					<th>Subservice Name </th>
					<th>Subservice type Image</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=0; foreach ($subcategorylist as $subcategory){   ?>
					<tr class="gradeA">
						<td><?php echo $i+1;?></td>
						<td><?php echo $subcategory['subcategory_name'];?></td>
						<td><?php echo $subcategory['mainservice_name'];?></td>
						<td><?php echo $subcategory['category_name'];?></td>
						<td>
						<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $subcategory['subcategory_image'];?>' height="40" width="60">
						</td>
						<td><?php if(isset($subcategory['is_active']) && $subcategory['is_active']==1){ echo "Active"; }else{ echo "Inactive";}?></td>
						<td class="center">
						
						<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/managesubcategory?act=edit&subcategory_id=<?php echo $subcategory['subcategory_id'];?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a> 
						<a  href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/managesubcategory?act=delete&subcategory_id=<?php echo $subcategory['subcategory_id'];?>" class="btn btn-danger btn-sm btn-icon icon-left delt">
							<i class="entypo-cancel"></i>
							Delete
						</a>
						
						</td>
					</tr>
				<?php $i++;}?>
			</tbody>
			
		</table>
		
		<script type="text/javascript">
		jQuery(document).ready(function($)
				{
					var table = $("#table-4").dataTable({
						"sPaginationType": "bootstrap",
						//"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
						"oTableTools": {
						},
						
					});
				});
			$(".delt").click(function(delt)
					{
				var href= $(this).attr("href") ;
				if (window.confirm("Are you sure you want to delete this subcategory?")) {
					  window.location.href = href ; 	
					}
				return false ;
					}
			);
		</script>
		