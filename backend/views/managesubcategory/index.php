<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>
<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/validation/subcategory.js"> </script>
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['subcategory_id']) && $_REQUEST['subcategory_id']){?>
			Edit Subservice type
			<?php }else{?>
			Add Subservice type
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managesubcategory/onpost" method="post" id="subcategory_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
							<label class="col-sm-3 control-label">Main service <span class="valError">*</span></label>
								<div class="col-sm-5">
									<select class="form-control" name="mainservice_id" required="" id="mainserviceId">
										<option value>Select mainservice</option>
										<?php foreach ($mainservicelist as $mainservice){?>
										<option value="<?php echo $mainservice['mainservice_id'];?>" <?php if(isset($subcategorydetail->mainservice_id) && $subcategorydetail->mainservice_id==$mainservice['mainservice_id']){?> selected <?php }?>>
										<?php echo $mainservice['mainservice_name'];?></option>
										<?php }?>					
									</select>
								</div>
								<div class="valError">
									<span class="val_mainserviceId"></span>
								</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Subservice <span class="valError">*</span></label>
								<div class="col-sm-5">
									<select class="form-control" name="category_id" required="" id="categoryId">
										<option value>Select Subservice</option>
															
									</select>
								</div>
								<div class="valError">
									<span class="val_categoryId"></span>
								</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Subservice type Name <span class="valError">*</span></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="subcategory_name"
								id="subcategoryname" placeholder="Subservice type Name" 
								value="<?php if(isset($subcategorydetail->subcategory_name) && $subcategorydetail->subcategory_name){ echo $subcategorydetail->subcategory_name;}?>" Required>
						</div>
						<div class="valError">
							<span class="val_subcategoryname"></span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Subservice type Description</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="subcategory_description"
								id="subcategorysortname" placeholder="Subservice type Description" 
								value="<?php if(isset($subcategorydetail->subcategory_description) && $subcategorydetail->subcategory_description){ echo $subcategorydetail->subcategory_description;}?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Subservice type Image <span class="valError">*</span></label>
						<div class="col-sm-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<input type="hidden" id="doc" name="subcategory_image" value="<?php if(isset($subcategorydetail->subcategory_image)){echo $subcategorydetail->subcategory_image;}?>">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span> <span class="fileinput-exists">Change</span> <input type="file" name="document" id="documentId">

									</span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
								<div style="margin-top: 10px; display:none;" id="loader">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
								</div>													
							</div>
									
							<div class="col-sm-12 nopadding" id="errormsg">
							<?php if(isset($subcategorydetail->subcategory_image)){?>
								<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $subcategorydetail->subcategory_image;?>' height="100" width="100">
							<?php }?>
							</div>
					  </div>
					  <div class="valError">
							<span class="val_doc"></span>
						</div>
					</div>
					
					
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($subcategorydetail->is_active) && $subcategorydetail->is_active==0){ }else{?>
									checked <?php }?>>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default" id="save">
							<?php if(isset($subcategorydetail->subcategory_id) && $subcategorydetail->subcategory_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($subcategorydetail->subcategory_id) && $subcategorydetail->subcategory_id){ ?> 
										<input type="hidden"
											value="<?php echo $subcategorydetail->subcategory_id; ?>" name="subcategory_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">



$("#cancel" ).click(function() {
	window.history.back();
});

$( document ).ready(function() {
	getCategory($("#mainserviceId").val());
});
$("#mainserviceId").change(function(){
	getCategory($("#mainserviceId").val());
	
});


function getCategory(mainservice_id)
{
	 $.ajax({
	        type: "POST",
	        url: "<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/managecategory/getcategorylist",
	        data: {'mainservice_id':  mainservice_id},
	        context: this,
	 		success: function(response){
	 		 var obj =  jQuery.parseJSON(response) ;
	 	     $("#categoryId").empty();
	 	     $("#categoryId").append(new Option("Select Category",""));
	 	     if(obj.length > 0 )
	 	     {
	 	         for(var i = 0 ; i < obj.length ; i++)
	 	         {
	 		     	  $("#categoryId").append(new Option(obj[i].category_name, obj[i].category_id));
	 	               <?php if(isset($subcategorydetail->category_id) && $subcategorydetail->category_id){?>
	 	                if(obj[i].category_id == "<?php echo $subcategorydetail->category_id; ?>") {  $("#categoryId").val(obj[i].category_id);} 
	 	               <?php }?>
	 	          }
	 	                     
	 	      }
	 		
			}
	      }) ;
	
}



$(function(){
	$("#documentId").change(function() { 
		$("#loader").show();
		$("#isdocument").hide();
		 $("#errormsg").hide();
				uploadfile();
	});
});






function uploadfile()
{ 
	$("#image_priview").show();
	$("#save").attr("disabled","disabled")  ;
	$("#subcategory_form")
	.ajaxSubmit({ 
					url : 'managecategory/uploadimage',
					type : 'post',
					success : function(response) {
						 var obj =  jQuery.parseJSON(response) ;
						 $("#loader").hide();
						 if(obj.status==1){
							 if(typeof obj.image!='undefined'){
								 $("#doc").val(obj.image) ;
								 $("#errormsg").show();
								 $("#errormsg").empty();
								// var success="<span style='color: green; '>File uploded successfully, filename :- "+ obj.id +"</span>";
								var success="<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/"+ obj.image+"' height='100' width='100'/>";
								 $("#errormsg").append(success);
							 }
						 }else{
							 $("#errormsg").show();
							$("#errormsg").empty();
							var error="<span style='color: red; '>"+ obj.error +"</span>";
							$("#errormsg").append(error);
						 }
						 $("#save").removeAttr("disabled") ;
						
									
					}
				});
}
</script>