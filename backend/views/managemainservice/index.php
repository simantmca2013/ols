<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>
<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/validation/mainservice.js"> </script>
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['mainservice_id']) && $_REQUEST['mainservice_id']){?>
			Edit Maiservice
			<?php }else{?>
			Add Maiservice
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managemainservice/onpost" method="post" id="mainservice_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Maiservice Name <span class="valError">*</span></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="mainservice_name" label="hellow"
								id="mainservicename" placeholder="Mainservice Name" 
								value="<?php if(isset($mainservicedetails->mainservice_name) && $mainservicedetails->mainservice_name){ echo $mainservicedetails->mainservice_name;}?>" Required >
						</div>
						<div class="valError">
							<span class="val_mainservicename"></span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Maiservice Description  </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="mainservice_description"
								id="mainservicedescription" placeholder="Mainservice description" 
								value="<?php if(isset($mainservicedetails->mainservice_description) && $mainservicedetails->mainservice_description){ echo $mainservicedetails->mainservice_description;}?>" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Mainservice Image <span class="valError">*</span></label>
						<div class="col-sm-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<input type="hidden" id="doc" name="mainservice_img_path" value="<?php if(isset($mainservicedetails->mainservice_img_path)){echo $mainservicedetails->mainservice_img_path;}?>">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span> <span class="fileinput-exists">Change</span> <input type="file" name="document" id="documentId">
					
									</span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
									
								</div>
								<div style="margin-top: 10px; display:none;" id="loader">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
								</div>											
							</div>
									
							<div class="col-sm-12 nopadding" id="errormsg">
							<?php if(isset($mainservicedetails->mainservice_img_path)){?>
								<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $mainservicedetails->mainservice_img_path;?>' height="100" width="100">
							<?php }?>
							</div>
					  </div>
					  <div class="valError">
							<span class="val_doc"></span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Default Service</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_defaultservice" id="chk-23"
								<?php if(isset($mainservicedetails->is_defaultservice) && $mainservicedetails->is_defaultservice==1){?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($mainservicedetails->is_active) && $mainservicedetails->is_active==0){ }else{?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default" id="save">
							<?php if(isset($mainservicedetails->mainservice_id) && $mainservicedetails->mainservice_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($mainservicedetails->mainservice_id) && $mainservicedetails->mainservice_id){ ?> 
										<input type="hidden"
											value="<?php echo $mainservicedetails->mainservice_id; ?>" name="mainservice_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});

$(function(){
	$("#documentId").change(function() { 
		$("#loader").show();
		$("#isdocument").hide();
		 $("#errormsg").hide();
				uploadfile();
	});
});
function uploadfile()
{ 
	$("#image_priview").show();
	$("#save").attr("disabled","disabled")  ;
	$("#mainservice_form")
	.ajaxSubmit({ 
					url : 'managecategory/uploadimage',
					type : 'post',
					success : function(response) {
						 var obj =  jQuery.parseJSON(response) ;
						 $("#loader").hide();
						 if(obj.status==1){
							 if(typeof obj.image!='undefined'){
								
								 $("#doc").val(obj.image) ;
								 $("#errormsg").show();
								 $("#errormsg").empty();
								// var success="<span style='color: green; '>File uploded successfully, filename :- "+ obj.id +"</span>";
								var success="<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/"+ obj.image+"' height='100' width='100'/>";
								 $("#errormsg").append(success);
							 }
						 }else{
							 $("#errormsg").show();
							$("#errormsg").empty();
							var error="<span style='color: red; '>"+ obj.error +"</span>";
							$("#errormsg").append(error);
						 }
						 $("#save").removeAttr("disabled") ;
						
									
					}
				});
}

</script>

