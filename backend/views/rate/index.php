<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg6f14Y8VuDv_vFQ8vy0RmrET8xNFyOGw&v=3.exp&libraries=places"></script>
<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			Rate for subservice type <?php if(isset($subcategorydetail->subcategory_name)){ echo $subcategorydetail->subcategory_name; }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="rate/onpost" method="post" id="rate_form"
					class="form-horizontal form-groups-bordered">
					<?php if(isset($measurementtype) && !empty($measurementtype)){?>
					<?php foreach ($measurementtype as $mtype){?>
					<div class="form-group">
						<label class="col-sm-3 control-label">Rate for <?php echo $mtype['measurement_name']?> </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="cost[<?php echo $mtype['measurement_id']?>]" 
								 placeholder="Cost" 
								value="<?php if(isset($ratedetails['rate'][$mtype['measurement_id']])){echo $ratedetails['rate'][$mtype['measurement_id']] ;}?>" Required>
						</div>
					</div>
					
					<?php }?>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Description </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="description"
								 placeholder="Description" 
								value="<?php if(isset($ratedetails['description'])){echo $ratedetails['description'];}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Location </label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" name="location" id="location"
								 placeholder="Location" 
								value="<?php if(isset($ratedetails['location'])){echo $ratedetails['location']; }?>" Required>
								<input type="hidden" id="latlanforaddress" /> 
						</div>
						<div class="col-sm-3">
						<span style="display:none;" id="loader">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
						</span></label>
						<label class=" control-label"><a
								style='cursor: pointer;color: #147fce;' id="selectlocation">change location</a></label>
						</div>
					</div>
					<?php if(isset($rateimgdetails) && !empty($rateimgdetails)){
						$i=0;
						foreach ($rateimgdetails as $rateimg){?>
						<div class="form-group">
						<label class="col-sm-3 control-label">Upload Image</label>
						<div class="col-sm-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<input type="hidden" id="doc_<?php echo $i;?>" name="img_path[]" value="<?php if(isset($rateimg['img_path'])){echo $rateimg['img_path'];}?>">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span> 
									<span class="fileinput-exists">Change</span>
									<input type="file" name="document_<?php echo $i;?>" id="document_<?php echo $i;?>" class="docfileupload"> 
									</span>
									 <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
									
								</div>
								
								<div style="margin-top: 10px; display:none;" id="loader_<?php echo $i;?>">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
								</div>											
							</div>
									
							<div class="col-sm-12 nopadding" id="errormsg_<?php echo $i;?>">
							<?php if(isset($rateimg['img_path'])){?>
								<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $rateimg['img_path'];?>' height="100" width="100">
							<?php }?>
							</div>
					  </div>
					</div>
					<?php $i++; }}else{?>
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Image</label>
						<div class="col-sm-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<input type="hidden" id="doc_1" name="img_path[]" value="<?php if(isset($categorydetail->category_image)){echo $categorydetail->category_image;}?>">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span> 
									<span class="fileinput-exists">Change</span>
									<input type="file" name="document_1" id="document_1" class="docfileupload"> 
									</span>
									 <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
									
								</div>
								
								<div style="margin-top: 10px; display:none;" id="loader_1">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
								</div>											
							</div>
									
							<div class="col-sm-12 nopadding" id="errormsg_1">
							<?php if(isset($categorydetail->category_image)){?>
								<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $categorydetail->category_image;?>' height="100" width="100">
							<?php }?>
							</div>
					  </div>
					</div>
					<?php }?>
					<div id="append_section">
						<!-- Here Add  div to add more Image -->
					</div>
					<div class="col-sm-5">
								<button type="button" class="btn btn-link btn-md" id="add_more">
										<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/plus.png">Add More
									</button>
								</div>
					<?php }else{?>
					<div class="form-group">
						<label class="col-sm-5 control-label">
						Oops There is no measurement found  for this Subservice Type
						 </label>
						
					</div>
					<?php }?>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
						<?php if(isset($measurementtype) && !empty($measurementtype)){?>
							<button type="submit" class="btn btn-default">
							Save
							</button>
							<?php }?>
							<input type="hidden" name="numberof_documents"
							id="numberof_documents" value="1"> 
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($subcategorydetail->subcategory_id) && $subcategorydetail->subcategory_id ){ ?> 
										<input type="hidden"
											value="<?php echo $subcategorydetail->subcategory_id ?>" name="subcategory_id" /> 
											<input type="hidden"
											value="<?php echo $_REQUEST['shop_id'] ?>" name="shop_id" /> 
											<input
											type="hidden" value="rate" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
var numberof_document=1;
var counter_loader=0;
<?php if(isset($rateimgdetails) && count($rateimgdetails) >0){?>
	 numberof_document=<?php echo count($rateimgdetails);?>;
<?php }?>

$("#cancel" ).click(function() {
	window.history.back();
});
$("#add_more").click(function(){
	if(numberof_document >1){
		$("#remove").removeClass('hidden');
	}
	numberof_document=numberof_document+1;
	var html = '<div class="form-group">';
	    html +='<label class="col-sm-3 control-label">Upload Image</label>';
		html +='<div class="col-sm-3">';
		html +='<div class="fileinput fileinput-new" data-provides="fileinput">';
		html +='<input type="hidden" id="doc_'+numberof_document+'"  name="img_path[]" value="">';
		html +='<div class="input-group">';
		html +='<div class="form-control uneditable-input" data-trigger="fileinput">';
		html +='<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>';
		html +='</div>';
		html +='<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span>'; 
		html +='<span class="fileinput-exists">Change</span>';
		html +='<input type="file" name="document_'+numberof_document+'" class="docfileupload" id="document_'+numberof_document+'">'; 
		html +='</span>';
	 	html +='<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>';
		html +='</div>';
		html +='<div style="margin-top: 10px; display:none;" id="loader_'+numberof_document+'">';
		html +='<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />';
		html +='</div>';
		html +='<div class="col-sm-12 nopadding" id="errormsg_'+numberof_document+'">';
		html +='<?php if(isset($categorydetail->category_image)){?>';
		html +='<img src="<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $categorydetail->category_image;?>" height="100" width="100">';
		html +='<?php }?>';
		html +='</div>';
		html +='</div>';
		html +='</div>';
	    html +='</div>';
	    $("#append_section").append(html);
		$("#numberof_documents").val(numberof_document);
});

$(document).on("change" , '.docfileupload', function() { 
	var fileid=$(this).attr('id');  
	var div_number=fileid.split("_");
	counter_loader= counter_loader + 1;
		uploadfile(div_number[1],fileid);
	
});
function uploadfile(set_hidden_path,fileid)
{
	 
	$("#validation_error").hide();
	$("#document_"+fileid).removeAttr('required');
	$("#errormsg"+set_hidden_path).empty();
	$("#errormsg"+set_hidden_path).show();
	$("#loader_"+set_hidden_path).show();
	$("#save").attr("disabled","disabled")  ;
	
	$("#rate_form").ajaxSubmit({ url : 'rate/uploadimage',
			type : 'POST',
			data: { "document_field": fileid },						
			success : function(response) { 
				$("#loader_"+set_hidden_path).hide();
				$("#"+fileid).val("");
				  var obj =  jQuery.parseJSON(response) ; 
				  if(obj.status==1){
					 if(typeof obj.image!='undefined'){ 
						 $("#doc_"+set_hidden_path).val(obj.image) ;
						 $("#errormsg_"+set_hidden_path).show();
						 $("#errormsg_"+set_hidden_path).empty();
						 var success="<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/"+ obj.image+"' height='100' width='100'/>";
						 $("#errormsg_"+set_hidden_path).append(success);
					 }
				 }else{
					 $("#errormsg_"+set_hidden_path).show();
					$("#errormsg_"+set_hidden_path).empty();
					var error="<span style='color: red; '>"+ obj.error +"</span>";
					$("#errormsg_"+set_hidden_path).append(error);
				 }
				 counter_loader = counter_loader - 1;
				/** Enable Save button */
				 if(counter_loader == 0){
					 $("#save").removeAttr("disabled") ;
				} 		 
			}
	} );
}
$(document).ready(function()
{
	<?php if(!isset($ratedetails['location'])){ ?>
		getLocation();
	<?php }elseif (empty($ratedetails['location'])){?>
		getLocation();
	<?php }?>
	
	$("#selectlocation").click(function(){
		$("#loader").hide();
		$("#location").val('');
	});
    locationlatlanPicker();
});
function locationlatlanPicker() {
	
    $('#location').focus(function () {
    autocomplete = new google.maps.places.Autocomplete(this);
    searchbox = this;

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var thisplace = autocomplete.getPlace(); 
        if (thisplace.geometry.location != null) { 
            var lat=thisplace.geometry.location.lat();
            var lan=thisplace.geometry.location.lng();
           
            
        }
    });
});
}
function getLocation() {
	console.log(navigator.geolocation);
	var x = document.getElementById("latlanforaddress");
	if (navigator.geolocation) { console.log("yes");
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
    
}

function showPosition(position) {
   var lat=position.coords.latitude;
   var lan=position.coords.longitude; 
   $.ajax({
       type: "POST",
       url: "<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/rate/getcurrentlocation",
      	data:{ "lat" : lat,"lan" : lan}
		})
		 .done(function( response ) {
			 $("#loader").hide();
			 var obj =  jQuery.parseJSON(response) ;
			 if(typeof obj.currentlocation!='undefined'){
			 	$("#location").val(obj.currentlocation );
			 }
			 
			 
			
		 });
  
}



</script>