<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>

<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['servicemeasurement_id']) && $_REQUEST['servicemeasurement_id']){?>
			Edit Subcategory
			<?php }else{?>
			Add Subcategory
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="manageservicemeasurement/onpost" method="post" id="subcategory_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
							<label class="col-sm-3 control-label">Service</label>
								<div class="col-sm-5">
									<select class="form-control" name="service_id" required="">
										<option value>Select Service</option>
										<?php foreach ($servicelist as $service){?>
										<option value="<?php echo $service['service_id'];?>" <?php if(isset($servicemeasurementdetails->service_id) && $servicemeasurementdetails->service_id==$service['service_id']){?> selected <?php }?>>
										<?php echo $service['service_location'];?></option>
										<?php }?>					
									</select>
								</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Measurement</label>
								<div class="col-sm-5">
									<select class="form-control" name="measurement_id" required="">
										<option value>Select Measurement</option>
										<?php foreach ($measurementlist as $measurement){?>
										<option value="<?php echo $measurement['measurement_id'];?>" <?php if(isset($servicemeasurementdetails->measurement_id) && $servicemeasurementdetails->measurement_id==$measurement['measurement_id']){?> selected <?php }?>>
										<?php echo $measurement['measurement_name'];?></option>
										<?php }?>					
									</select>
								</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Cost</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="cost"
								id="cost" placeholder="Cost" 
								value="<?php if(isset($servicemeasurementdetails->cost) && $servicemeasurementdetails->cost){ echo $servicemeasurementdetails->cost;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($servicemeasurementdetails->is_active) && $servicemeasurementdetails->is_active==1){ ?>
									checked <?php }?>>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($servicemeasurementdetails->servicemeasurement_id) && $servicemeasurementdetails->servicemeasurement_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($servicemeasurementdetails->servicemeasurement_id) && $servicemeasurementdetails->servicemeasurement_id){ ?> 
										<input type="hidden"
											value="<?php echo $servicemeasurementdetails->servicemeasurement_id; ?>" name="servicemeasurement_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">



$("#cancel" ).click(function() {
	window.history.back();
});


</script>