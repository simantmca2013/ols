<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>

<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['category_id']) && $_REQUEST['category_id']){?>
			Edit Service provider
			<?php }else{?>
			Add Service provider
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="manageshop/onpost" method="post" id="shop_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Service provider First Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="first_name"
								id="shopname" placeholder="Shop Name" 
								value="<?php if(isset($shopdetail->first_name) && $shopdetail->first_name){ echo $shopdetail->first_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Service provider Last Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="last_name"
								id="shopname" placeholder="Shop Name" 
								value="<?php if(isset($shopdetail->last_name) && $shopdetail->last_name){ echo $shopdetail->last_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Shop Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="shop_name"
								id="shopname" placeholder="Shop Name" 
								value="<?php if(isset($shopdetail->shop_name) && $shopdetail->shop_name){ echo $shopdetail->shop_name;}?>" Required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Email </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email"
								id="email" placeholder="Email"  <?php if(isset($shopdetail->email) && !empty($shopdetail->email)){?> readonly <?php }?>
								value="<?php if(isset($shopdetail->email) && $shopdetail->email){ echo $shopdetail->email;}?>"  Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Mobile Number </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="mobile_number"
								id="mobilenumber" placeholder="Mobile Number" 
								value="<?php if(isset($shopdetail->mobile_number) && $shopdetail->mobile_number){ echo $shopdetail->mobile_number;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Contact Number </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="contact_number"
								id="contactnumber" placeholder="Contact Number" 
								value="<?php if(isset($shopdetail->contact_number) && $shopdetail->contact_number){ echo $shopdetail->contact_number;}?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Country</label>
							<div class="col-sm-5">
								<select class="form-control" name="country_id" id="countryId" required="">
									<option value>Select Country</option>
								<?php foreach ($countrylist as $country){?>	
									<option value="<?php echo $country['country_id']?>" <?php if(isset($shopdetail->country_id) && $shopdetail->country_id==$country['country_id']){?> selected <?php }?>>
									<?php echo $country['country_name'];?></option>	
								<?php }?>				
								</select>
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">State 
						<span id="lodingstate" style="margin-left: 5px;display:none;"><img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" /></span></label>
							<div class="col-sm-5">
								<select class="form-control" name="state_id" id="stateId" required="">
									<option value>Select State</option>
														
								</select>
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">City
						<span id="lodingcity" style="margin-left: 5px;display:none;"><img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" /></span></label></label>
							<div class="col-sm-5">
								<select class="form-control" name="city_id" id="cityId" required="">
									<option value>Select City</option>
														
								</select>
							</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Pin Code </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="pin_code"
								id="pincode" placeholder="Pin Code" 
								value="<?php if(isset($shopdetail->pin_code) && $shopdetail->pin_code){ echo $shopdetail->pin_code;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Government Id proof</label>
						<div class="col-sm-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<input type="hidden" id="doc" name="govt_id_proof" value="<?php if(isset($shopdetail->govt_id_proof)){echo $shopdetail->govt_id_proof;}?>">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span> <span class="fileinput-exists">Change</span> <input type="file" name="document" id="documentId">
									</span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
								<div style="margin-top: 10px; display:none;" id="loader2">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
								</div>													
							</div>
									
							<div class="col-sm-12 nopadding" id="errormsg">
							<?php if(isset($shopdetail->govt_id_proof)){?>
								<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $shopdetail->govt_id_proof;?>' height="100" width="100">
							<?php }?>
							</div>
					  </div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"> Image</label>
						<div class="col-sm-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<input type="hidden" id="doc1" name="shop_image" value="<?php if(isset($shopdetail->shop_image)){echo $shopdetail->shop_image;}?>">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select File</span> <span class="fileinput-exists">Change</span> <input type="file" name="document" id="shopimageId">

									</span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
								<div style="margin-top: 10px; display:none;" id="loader1">
									<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/process.gif" />
								</div>												
							</div>
									
							<div class="col-sm-12 nopadding" id="errormsg1">
							<?php if(isset($shopdetail->shop_image)){?>
								<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/<?php echo $shopdetail->shop_image;?>' height="100" width="100">
							<?php }?>
							</div>
					  </div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Address </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="address"
								id="address" placeholder="Address" 
								value="<?php if(isset($shopdetail->address) && $shopdetail->address){ echo $shopdetail->address;}?>" Required>
						</div>
					</div>
				    <div class="form-group">
						<label class="col-sm-3 control-label">Estimated Time </label>
						<div class="col-sm-5">
						
							<input type="number" class="form-control" name="estimated_time"
								id="address" placeholder="Estimated Time in hour" 
								value="<?php if(isset($shopdetail->estimated_time) && $shopdetail->estimated_time){ echo $shopdetail->estimated_time;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-2311"
								<?php if(isset($shopdetail->is_active) && $shopdetail->is_active==0){}else{ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default" id="save">
							<?php if(isset($shopdetail->shop_id) && $shopdetail->shop_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($shopdetail->shop_id) && $shopdetail->shop_id){ ?> 
										<input type="hidden"
											value="<?php echo $shopdetail->shop_id; ?>" name="shop_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">

$( document ).ready(function() {
	getState($("#countryId").val());
});
$("#cancel" ).click(function() {
	window.history.back();
});

$("#countryId").change(function(){
	$("#lodingstate").show();
	getState($("#countryId").val());
	
});
$("#stateId").change(function(){
	$("#lodingcity").show();
	getCity($("#countryId").val(),$("#stateId").val());
	
});

function getState(country_id)
{
	 $.ajax({
	        type: "POST",
	        url: "<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/managestate/getstatelist",
	        data: {'country_id':  country_id},
	        context: this,
	 		success: function(response){
	 		 var obj =  jQuery.parseJSON(response) ;
	 	     $("#stateId").empty();
	 	    $("#lodingstate").hide();
	 	     $("#stateId").append(new Option("Select State",""));
	 	     if(obj.length > 0 )
	 	     {
	 	         for(var i = 0 ; i < obj.length ; i++)
	 	         {
	 		     	  $("#stateId").append(new Option(obj[i].state_name, obj[i].state_id));
	 	               <?php if(isset($shopdetail->state_id) && $shopdetail->state_id){?>
	 	                if(obj[i].state_id == "<?php echo $shopdetail->state_id; ?>") {  $("#stateId").val(obj[i].state_id);} 
	 	               <?php }?>
	 	          }
	 	                     
	 	      }
	 	    <?php if(isset($shopdetail->state_id) && $shopdetail->state_id){?>
	 	   		getCity($("#countryId").val(),$("#stateId").val());
	 	    <?php }?>
			}
	      }) ;
	
}
function getCity(country_id,state_id)
{
	 $.ajax({
	        type: "POST",
	        url: "<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/managecity/getcitylist",
	        data: {'country_id':  country_id,'state_id':  state_id},
	        context: this,
	 		success: function(response){
	 		 var obj =  jQuery.parseJSON(response) ;
	 	     $("#cityId").empty();
	 	    $("#lodingcity").hide();
	 	     $("#cityId").append(new Option("Select City",""));
	 	     if(obj.length > 0 )
	 	     {
	 	         for(var i = 0 ; i < obj.length ; i++)
	 	         {
	 		     	  $("#cityId").append(new Option(obj[i].city_name, obj[i].city_id));
	 	               <?php if(isset($shopdetail->city_id) && $shopdetail->city_id){?>
	 	                if(obj[i].city_id == "<?php echo $shopdetail->city_id; ?>") {  $("#cityId").val(obj[i].city_id);} 
	 	               <?php }?>
	 	          }
	 	                     
	 	      }
	 		
			}
	      }) ;
	
}

$(function(){
	$("#documentId").change(function() { 
		$("#loader2").show();
		 $("#errormsg").hide();
				uploadfile();
	});
});
$(function(){
	$("#shopimageId").change(function() { 
		$("#loader1").show();
		 $("#errormsg1").hide();
				uploadimage();
	});
});

function uploadfile()
{ 
	$("#image_priview").show();
	$("#save").attr("disabled","disabled")  ;
	$("#shop_form")
	.ajaxSubmit({ 
					url : 'manageshop/uploaddocs',
					type : 'post',
					success : function(response) {
						 var obj =  jQuery.parseJSON(response) ;
						 $("#loader2").hide();
						 if(obj.status==1){
							 if(typeof obj.image!='undefined'){
								 $("#doc").val(obj.image) ;
								 $("#errormsg").show();
								 $("#errormsg").empty();
								// var success="<span style='color: green; '>File uploded successfully, filename :- "+ obj.id +"</span>";
								var success="<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/"+ obj.image+"' height='100' width='100'/>";
								 $("#errormsg").append(success);
							 }
						 }else{
							 $("#errormsg").show();
							$("#errormsg").empty();
							var error="<span style='color: red; '>"+ obj.error +"</span>";
							$("#errormsg").append(error);
						 }
						 $("#save").removeAttr("disabled") ;
						
									
					}
				});
}
function uploadimage()
{ 
	$("#image_priview").show();
	$("#save").attr("disabled","disabled")  ;
	$("#shop_form")
	.ajaxSubmit({ 
					url : 'managecategory/uploadimage',
					type : 'post',
					success : function(response) {
						 var obj =  jQuery.parseJSON(response) ;
						 $("#loader1").hide();
						 if(obj.status==1){
							 if(typeof obj.image!='undefined'){
								 $("#doc1").val(obj.image) ;
								 $("#errormsg1").show();
								 $("#errormsg1").empty();
								// var success="<span style='color: green; '>File uploded successfully, filename :- "+ obj.id +"</span>";
								var success="<img src='<?php echo str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl());?>/backend/web/"+ obj.image+"' height='100' width='100'/>";
								 $("#errormsg1").append(success);
							 }
						 }else{
							 $("#errormsg1").show();
							$("#errormsg1").empty();
							var error="<span style='color: red; '>"+ obj.error +"</span>";
							$("#errormsg1").append(error);
						 }
						 $("#save").removeAttr("disabled") ;
						
									
					}
				});
}

</script>

