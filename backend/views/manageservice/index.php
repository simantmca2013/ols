<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>

<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['service_id']) && $_REQUEST['service_id']){?>
			Edit Service
			<?php }else{?>
			Add Service
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="manageservice/onpost" method="post" id="subcategory_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
							<label class="col-sm-3 control-label">Category</label>
								<div class="col-sm-5">
									<select class="form-control" name="category_id" id="categoryId" required="">
										<option value>Select category</option>
										<?php foreach ($categorylist as $category){?>
										<option value="<?php echo $category['category_id'];?>" <?php if(isset($servicedetails->category_id) && $servicedetails->category_id==$category['category_id']){?> selected <?php }?>>
										<?php echo $category['category_name'];?></option>
										<?php }?>					
									</select>
								</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Subcategory</label>
								<div class="col-sm-5">
									<select class="form-control" name="subcategory_id" id="subcategoryId" required="">
										<option value>Select Subcategory</option>
															
									</select>
								</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Shop</label>
								<div class="col-sm-5">
									<select class="form-control" name="shop_id"  required="">
										<option value>Select Shop</option>
										<?php foreach ($shoplist as $shop){?>
										<option value="<?php echo $shop['shop_id'];?>" <?php if(isset($servicedetails->shop_id) && $servicedetails->shop_id==$shop['shop_id']){?> selected <?php }?>>
										<?php echo $shop['shop_name'];?></option>
										<?php }?>					
									</select>
								</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Service Rating</label>

						<div class="col-sm-5">
							<input type="number" class="form-control" name="rating" min ="0" max="5"
								id="servicename" placeholder="Service Rating" 
								value="<?php if(isset($servicedetails->rating) && $servicedetails->rating){ echo $servicedetails->rating;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Service Location</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="service_location"
								id="servicename" placeholder="Service Location" 
								value="<?php if(isset($servicedetails->service_location) && $servicedetails->service_location){ echo $servicedetails->service_location;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Service Description</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="service_description"
								id="servicedescriptionname" placeholder="Service Description" 
								value="<?php if(isset($servicedetails->service_description) && $servicedetails->service_description){ echo $servicedetails->service_description;}?>" Required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-23"
								<?php if(isset($servicedetails->is_active) && $servicedetails->is_active==1){ ?>
									checked <?php }?>>

							</div>

						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Multimeasurement</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_multimeasurement" id="chk-23"
								<?php if(isset($servicedetails->is_multimeasurement) && $servicedetails->is_multimeasurement==1){ ?>
									checked <?php }?>>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							<?php if(isset($servicedetails->service_id) && $servicedetails->service_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($servicedetails->service_id) && $servicedetails->service_id){ ?> 
										<input type="hidden"
											value="<?php echo $servicedetails->service_id; ?>" name="service_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">



$("#cancel" ).click(function() {
	window.history.back();
});
$( document ).ready(function() {
	getSubcategory($("#categoryId").val());
});
$("#categoryId").change(function(){
	getSubcategory($("#categoryId").val());
	
});


function getSubcategory(category_id)
{
	 $.ajax({
	        type: "POST",
	        url: "<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/manageservice/getsubcategorylist",
	        data: {'category_id':  category_id},
	        context: this,
	 		success: function(response){
	 		 var obj =  jQuery.parseJSON(response) ;
	 	     $("#subcategoryId").empty();
	 	     $("#subcategoryId").append(new Option("Select Subcategory",""));
	 	     if(obj.length > 0 )
	 	     {
	 	         for(var i = 0 ; i < obj.length ; i++)
	 	         {
	 		     	  $("#subcategoryId").append(new Option(obj[i].subcategory_name, obj[i].subcategory_id));
	 	               <?php if(isset($servicedetails->subcategory_id) && $servicedetails->subcategory_id){?>
	 	                if(obj[i].subcategory_id == "<?php echo $servicedetails->subcategory_id; ?>") {  $("#subcategoryId").val(obj[i].subcategory_id);} 
	 	               <?php }?>
	 	          }
	 	                     
	 	      }
	 		
			}
	      }) ;
	
}


</script>