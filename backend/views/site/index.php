

<div class="login-container">
	
	<div class="login-header login-caret" style="padding: 10px 0;">
		
		<div class="login-content">
			<a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl();?>" class="logo">
				<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl();?>/images/olslogo.jpg" width="150" alt="logo" />
			</a>
		 
			<p class="description">Welcom to Ols Login panel</p>
			
		</div>
		
	</div>
	
	
	
	<div class="login-form">
		
		<div class="login-content">
			<?php  if(trim(Yii::$app->session->getFlash('msg'))!='') { ?>
			<div class="">
				<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
			</div>
			<?php } ?> 
			<div class="form-login-error">
				<h3>Invalid login</h3>
				<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>
			</div>
			
			<form method="post" role="form" action="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/login" id="form_login">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
				<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login In
					</button>
				</div>
				
				</form>
			
			
			<div class="login-bottom-links">
				
				<a href="extra-forgot-password.html" class="link">Forgot your password?</a>
				
				<br />
				
				
				
			</div>
			
		</div>
		
	</div>
	
</div>


	
