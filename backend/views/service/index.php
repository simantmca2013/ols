<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>

<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			Manage service for "<?php echo $serviceproviderdetails->shop_name;?>"
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="service/onpost" method="post" id="service_form"
					class="form-horizontal form-groups-bordered">
					<?php foreach ($subservicelist as $category_id => $subservice) {?>
					<?php if(isset($subservicetype[$category_id]) && count($subservicetype[$category_id]) > 0){?>
					<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $mainservicelist[$subservice['mainservice_id']]." (".$subservice['category_name']." )"?></label>
					<div class="col-sm-5">
					<?php }?>
					<?php if(isset($subservicetype[$category_id])){
					foreach ($subservicetype[$category_id] as $subservicetype_id => $servicetypedata){?>
						<div class="checkbox checkbox-replace color-green ">
							<input type="checkbox" id="chk-23" name="service_id[]" value="<?php echo $servicetypedata['mainservice_id']."_".$servicetypedata['category_id']."_".$servicetypedata['subcategory_id'];?>"
							<?php if(isset($serviceDetails[$servicetypedata['mainservice_id']][$servicetypedata['category_id']]) && !empty($serviceDetails[$servicetypedata['mainservice_id']][$servicetypedata['category_id']]) && in_array($servicetypedata['subcategory_id'], $serviceDetails[$servicetypedata['mainservice_id']][$servicetypedata['category_id']])){?> 
							checked <?php }?>>
							<label><?php echo $servicetypedata['subcategory_name'];?></label> 
							<a class="linkmargin" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl()."/rate?act=rate&shop_id=".$serviceproviderdetails->shop_id."&subcategory_id=".$servicetypedata['subcategory_id'];?>">Add Rate</a>
						</div>
					<?php }}?>
					<?php if(isset($subservicetype[$category_id]) && count($subservicetype[$category_id]) > 0){?>
					</div>
					</div>
					<?php }?>
					<?php }?>
					
					
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">
							Save
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($serviceproviderdetails->shop_id) && $serviceproviderdetails->shop_id ){ ?> 
										<input type="hidden"
											value="<?php echo $serviceproviderdetails->shop_id ?>" name="shop_id" /> <input
											type="hidden" value="service" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
$("#cancel" ).click(function() {
	window.history.back();
});
</script>