<script
	src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/js/jquery.form.js"> </script>

<div class="row">
	<div class="col-md-12">
			<?php if(Yii::$app->session->getFlash('msg')) { ?>
					<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
					<p><?php  echo Yii::$app->session->getFlash('msg'); ?></p>
					</div>
			<?php }?>
		<div class="col-md-12">
			<h2>
			<?php if(isset($_REQUEST['driver_id']) && $_REQUEST['driver_id']){?>
			Edit Driver
			<?php }else{?>
			Add Driver
			<?php }?>
			</h2>
		</div>
		<div class="panel panel-primary" data-collapsed="0">


			<div class="panel-body">

				<form role="form" action="managedriver/onpost" method="post" id="driver_form"
					class="form-horizontal form-groups-bordered">
					<div class="form-group">
						<label class="col-sm-3 control-label">Driver First Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="first_name"
								id="shopname" placeholder="Shop Name" 
								value="<?php if(isset($driverDetails->first_name) && $driverDetails->first_name){ echo $driverDetails->first_name;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Driver Last Name </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="last_name"
								id="shopname" placeholder="Shop Name" 
								value="<?php if(isset($driverDetails->last_name) && $driverDetails->last_name){ echo $driverDetails->last_name;}?>" Required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Email </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email"
								id="email" placeholder="Email"  <?php if(isset($driverDetails->email) && !empty($driverDetails->email)){?> readonly <?php }?>
								value="<?php if(isset($driverDetails->email) && $driverDetails->email){ echo $driverDetails->email;}?>"  Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Mobile Number </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="mobile_number"
								id="mobilenumber" placeholder="Mobile Number" 
								value="<?php if(isset($driverDetails->mobile_number) && $driverDetails->mobile_number){ echo $driverDetails->mobile_number;}?>" Required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Is Active</label>

						<div class="col-sm-5">
							<div class="checkbox checkbox-replace color-green">
								<input type="checkbox" name="is_active" id="chk-2311"
								<?php if(isset($driverDetails->is_active) && $driverDetails->is_active==0){}else{ ?>
									checked <?php }?>/>

							</div>

						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default" id="save">
							<?php if(isset($driverDetails->driver_id) && $driverDetails->driver_id){ ?>
							Update
							<?php }else{?>
							Save
							<?php }?>
							
							</button>
							<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
							<button type="button" class="btn btn-default" id="cancel">Close</button>
									<?php if(isset($driverDetails->driver_id) && $driverDetails->driver_id){ ?> 
										<input type="hidden"
											value="<?php echo $driverDetails->driver_id; ?>" name="driver_id" /> <input
											type="hidden" value="edit" name="act" />
									 <?php }else{?>
									 <input type="hidden" value="add" name="act" />
									 <?php }?>
								</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</div>


