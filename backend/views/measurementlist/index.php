<?php if(Yii::$app->session->getFlash('msg')) { ?>
		<div class="<?php if(Yii::$app->session->getFlash('status')==0){?>alert alert-danger<?php }else{?> alert alert-success <?php }?>">
		<?php  echo Yii::$app->session->getFlash('msg'); ?>
		</div>
<?php }?>
<h3>Measurement Listing</h3>
		<br />
		<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/managemeasurement?act=add" class="btn btn-primary mb10">
			<i class="entypo-plus"></i>
			Add Measurement
		</a>
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>S.no</th>
					<th>Measurement Name ( Sort name)</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=0; foreach ($measurementlist as $measurement){  ?>
					<tr class="gradeA">
						<td><?php echo $i+1;?></td>
						<td><?php echo $measurement['measurement_name']." ( " .$measurement['measurement_sort_name']." )"?></td>
						<td><?php if(isset($measurement['is_active']) && $measurement['is_active']==1){ echo "Active"; }else{ echo "Inactive";}?></td>
						<td class="center">
						
						<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/managemeasurement?act=edit&measurement_id=<?php echo $measurement['measurement_id'];?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a> 
						<a  href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/managemeasurement?act=delete&measurement_id=<?php echo $measurement['measurement_id'];?>" class="btn btn-danger btn-sm btn-icon icon-left delt">
							<i class="entypo-cancel"></i>
							Delete
						</a>
						</td>
					</tr>
				<?php $i++;}?>
			</tbody>
			
		</table>
		
		<script type="text/javascript">
		jQuery(document).ready(function($)
				{
					var table = $("#table-4").dataTable({
						"sPaginationType": "bootstrap",
						//"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
						"oTableTools": {
						},
						
					});
				});
			$(".delt").click(function(delt)
					{
				var href= $(this).attr("href") ;
				if (window.confirm("Are you sure you want to delete this Country" )) {
					  window.location.href = href ; 	
					}
				return false ;
					}
			);
		</script>
		
