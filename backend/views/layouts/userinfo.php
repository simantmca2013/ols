
<div class="main-content">
	<div class="row">

		<!-- Profile Info and Notifications -->
		<div class="col-md-6 col-sm-8 clearfix">

			<ul class="user-info pull-left pull-none-xsm">

				
			</ul>


		</div>

		<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
					
					<li class="profile-info dropdown">
						<ul class="user-info pull-left pull-none-xsm">

						<!-- Profile Info -->
						<li class="profile-info dropdown">
							<!-- add class "pull-right" if you want to place this from right -->

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php if(Yii::$app->session->get('email')!=''){ echo ucwords(trim(Yii::$app->session->get('email')));}?>
							<i
						class="entypo-down-open"></i>
						</a>

							<ul class="dropdown-menu">

								<!-- Reverse Caret -->
								<li class="caret"></li>

								<!-- Profile sub-links -->
								<li><a
									href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/manageprofile">
										<i class="entypo-user"></i> Edit profile
								</a></li>
								<li><a
									href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/changepassword">
										<i class="entypo-user"></i> Change password
								</a></li>

							</ul>
						</li>
						

					</ul> 
					</li>
		
					<li class="sep"></li>
		
					<li>
						<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/login/logout" >
						Log out
						<i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		

	