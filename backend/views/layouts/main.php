<?php 
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\controllers\AdminController;
use app\models\Users;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php 
$admin=new AdminController(1, new Users());
$admin->beforeAction("");
$adminlanguage=$admin->admin_cookie_language;

?>
<?php include('header.php');?>
<?php if( Yii::$app->session->get('email')!="" && Yii::$app->session->get('user_id')!=""){?>
<?php include('sidebar.php');?>
<?php include('userinfo.php')?>
<?php }?>
     <?= $content ?>
<?php include ('footer.php');?>
<?php //$this->render("//layouts/footer",$response); ?>


