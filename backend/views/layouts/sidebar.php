<?php 
$categorytab=array('categorylist','managecategory','subcategorylist','managesubcategory','mainservicelist','managemainservice','rate');
$shoptab = array('shoplist','manageshop','service');
$countrytab = array('countrylist','managecountry');
$statetab = array('statelist','managestate');
$citytab = array('citylist','managecity');
$measurementtab = array('measurementlist','managemeasurement','servicemeasurementlist','manageservicemeasurement');
$vehicletypetab = array('vehicletyplist','managevehicletype');
$vehicletab = array('vehiclelist','managevehicle');
$servicetab = array('servicelist','manageservice');
$drivertab = array('driverlist','managedriver');

?>
	<div class="sidebar-menu"> 

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">
				
				<!-- Logo -->
				<div class="logo custom-logo">
					<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/dashboard">
						<img src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/images/olslogo.jpg" alt="">
					</a>
				</div>
			
				<!-- logo collapse icon -->
				<div class="sidebar-collapse btn-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu" style="">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="root-level">
					<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/dashboard">
						<i class="entypo-home"></i>
						<span class="title">Dashboard</span>
					</a>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$categorytab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Service</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$categorytab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="mainservicelist" || trim(Yii::$app->controller->id)=="managemainservice") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/mainservicelist">
								<span class="title">Main Service List</span>
								
							</a>
						</li>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="categorylist" || trim(Yii::$app->controller->id)=="managecategory") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/categorylist">
								<span class="title">Sub Service List</span>
								
							</a>
						</li>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="subcategorylist" || trim(Yii::$app->controller->id)=="managesubcategory") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/subcategorylist">
								<span class="title">Sub Service Type slist</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$shoptab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Service provider</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$shoptab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="shoplist" || trim(Yii::$app->controller->id)=="manageshop") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/shoplist">
								<span class="title">Service provider List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$countrytab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Country List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$countrytab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="countrylist" || trim(Yii::$app->controller->id)=="managecountry") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/countrylist">
								<span class="title">Country List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$statetab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">State List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$statetab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="statelist" || trim(Yii::$app->controller->id)=="managestate") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/statelist">
								<span class="title">State List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$citytab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">City List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$citytab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="citylist" || trim(Yii::$app->controller->id)=="managecity") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/citylist">
								<span class="title">City List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$measurementtab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Measurement List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$measurementtab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="measurementlist" || trim(Yii::$app->controller->id)=="managemeasurement") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/measurementlist">
								<span class="title">Measurement List</span>
								
							</a>
						</li>
						<!-- 
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="servicemeasurementlist" || trim(Yii::$app->controller->id)=="manageservicemeasurement") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/servicemeasurementlist">
								<span class="title">Service Measurement list</span>
								
							</a>
						</li>  -->
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$vehicletypetab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Vehicletype List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$vehicletypetab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="vehicletypelist" || trim(Yii::$app->controller->id)=="managevehicletype") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/vehicletypelist">
								<span class="title">Vehicletype List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$vehicletab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Vehicle List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$vehicletab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="vehiclelist" || trim(Yii::$app->controller->id)=="managevehicle") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/vehiclelist">
								<span class="title">Vehicle List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$drivertab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Driver List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$drivertab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="driverlist" || trim(Yii::$app->controller->id)=="managedriver") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/driverlist">
								<span class="title">Driver List</span>
								
							</a>
						</li>
						
					</ul>
				</li>
				<!-- 
				<li class="root-level has-sub <?php if(isset(Yii::$app->controller->id) && trim(in_array(trim(Yii::$app->controller->id),$servicetab))){?> active opened<?php }?>">
					<a href="#">
						<i class="entypo-menu"></i>
						<span class="title">Service List</span>
					</a>
					<ul <?php if(isset(Yii::$app->controller->id) && in_array(trim(Yii::$app->controller->id),$servicetab)){ ?> class="visible" <?php }else{?>  <?php }?>>
						<li class="<?php if(isset(Yii::$app->controller->id) && (trim(Yii::$app->controller->id)=="servicelist" || trim(Yii::$app->controller->id)=="managedriver") ){?> active <?php }?>">
							<a href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/servicelist">
								<span class="title">Service List</span>
								
							</a>
						</li>
						
					</ul>
				</li> ---->
			</ul>
			
		</div>

	</div>	 
	
	
