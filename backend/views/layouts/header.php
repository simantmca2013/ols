<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo ucwords(Yii::$app->name ); ?></title>
<meta name="" content="" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="" content="" />

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Le styles -->
	<link rel="shortcut icon" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ;?>/images/favicon.ico" />
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/css/custom.css">

	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/jquery-1.11.0.min.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	
	<!--  script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/jquery.validate.min.js"></script----->
	<!--  script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/neon-login.js"></script-->
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/neon-custom.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/gsap/main-gsap.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/resizeable.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/neon-api.js"></script>
	
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/datatables/TableTools.min.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/dataTables.bootstrap.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/datatables/lodash.min.js"></script>
	<script src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/datatables/responsive/js/datatables.responsive.js"></script>
	<script type="text/javascript" src="<?php echo  Yii::$app->getUrlManager()->getBaseUrl();?>/js/datatable_update.js"></script>
	
	
<style>
#content
{
background:none !important;
}
</style>

<body class="loginPage page-body login-page login-form-fall loaded login-form-fall-init" > 


	
	

<body class="page-body page-fade"> 
<div class="page-container">
