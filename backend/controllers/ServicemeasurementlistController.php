<?php

namespace backend\controllers;

use common\models\Servicemeasurement;

class ServicemeasurementlistController extends AdminController
{
    public function actionIndex()
    {
       $response=array();
    	$servicemeasurementlist =Servicemeasurement::getServiceMeasurement('','','');
    	$response['servicemeasurementlist']=$servicemeasurementlist;
    	return $this->render('index',$response);
    }

}
