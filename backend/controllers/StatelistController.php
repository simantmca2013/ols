<?php

namespace backend\controllers;



use common\models\State;

class StatelistController extends AdminController
{
    public function actionIndex()
    {
      	$response=array();
        $statelist = State::getState('','','');
    	$response['statelist']=$statelist;
    	return $this->render('index',$response);
    }

}
