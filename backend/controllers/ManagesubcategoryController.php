<?php

namespace backend\controllers;
use yii;
use common\models\Category;
use common\models\Subcategory;
use common\models\Mainservice;
class ManagesubcategoryController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$categorylist=Category::getCategory(1);
    	$mainservicelist = Mainservice::getMainservice(1);
    	$response['categorylist']=$categorylist; 
    	$response['mainservicelist'] = $mainservicelist;
    	if(isset($_REQUEST['subcategory_id']) && $_REQUEST['act']=="edit"){
    		$subcategory_id=$_REQUEST['subcategory_id'];
    		$subcategorydetails=Subcategory::findOne($subcategory_id);
    		$response['subcategorydetail']=$subcategorydetails;
    		 
    	}elseif(isset($_REQUEST['subcategory_id']) && $_REQUEST['act']=="delete"){
    		$subcategorydetails=Subcategory::findOne($_REQUEST['subcategory_id']);
    		if($subcategorydetails->delete()){
    			$msg="Subservice type has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/subcategorylist");
    		}
    	}
    	return $this->render('index',$response);
    }
    
    /**
     *  add and edit subcategory
     */
    public function actionOnpost(){
    	$msg="Oops There is some technical issue";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['subcategory_id'])){
    		$subcategory_id=$_REQUEST['subcategory_id'];
    		$subcategoryModel=Subcategory::findOne($subcategory_id);
    			
    	}else{
    		$subcategoryModel=new Subcategory();
    	}
    	$subcategoryModel->mainservice_id=$_REQUEST['mainservice_id'];
    	$subcategoryModel->category_id=$_REQUEST['category_id'];
    	$subcategoryModel->subcategory_name=$_REQUEST['subcategory_name'];
    	$subcategoryModel->subcategory_description=$_REQUEST['subcategory_description'];
    	$subcategoryModel->subcategory_image=$_REQUEST['subcategory_image'];
    	
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$subcategoryModel->is_active=1;
    	}else{
    		$subcategoryModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['subcategory_id'])){
    		$subcategoryModel->updated_on= date('Y-m-d H:i:s');
    		$subcategoryModel->updated_by=Yii::$app->session['user_id'];
    		if($subcategoryModel->update()){
    			$status=1;
    			$msg="Subservice type has been successfully edited";
    		}else{
    			$status=1;
    			$msg="There is no change to save";
    		}
    	}else{
    		$subcategoryModel->created_on= date('Y-m-d H:i:s');
    		$subcategoryModel->created_by=Yii::$app->session['user_id'];
    		if($subcategoryModel->save()){
    			$status=1;
    			$msg="Subservice type has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/subcategorylist");
    }
    

}
