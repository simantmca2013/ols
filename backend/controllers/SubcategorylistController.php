<?php
namespace backend\controllers;
use common\models\Subcategory;
class SubcategorylistController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$subcategorylist=Subcategory::getSubcategory('','','');
    	$response['subcategorylist']=$subcategorylist;
    	return $this->render('index',$response);
    }

}
