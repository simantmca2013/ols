<?php

namespace backend\controllers;
use Yii;
use common\models\Subcategory;
use common\models\Servicemeasurement;
use common\models\Rate;
use common\models\RateSubcategoryImage;

class RateController extends AdminController
{
	public function beforeAction($action) {
		$this->enableCsrfValidation = ($action->id !== "getcurrentlocation"); // <-- here
		return parent::beforeAction($action);
	}
    public function actionIndex()
    {
    	$response = array();
    	if(isset($_REQUEST['subcategory_id']) && !empty($_REQUEST['subcategory_id']) && isset($_REQUEST['shop_id']) && !empty($_REQUEST['shop_id'])){
    		$ratedet = array();
    		$subcategorydetail=Subcategory::findOne($_REQUEST['subcategory_id']);
    		$measurementtype = Servicemeasurement::getServiceMeasurement($subcategorydetail->category_id);
    		//print_r($subcategorydetail);die();
    		$ratedetails = Rate::getRate($_REQUEST['shop_id'],$_REQUEST['subcategory_id']);
    		foreach ($ratedetails as $rate){
    			$ratedet['description'] =$rate['description'];
    			$ratedet['location'] =$rate['location'];
    			$ratedet['rate'][$rate['measurement_id']] =$rate['cost'];
    		}
    		$rateimages = RateSubcategoryImage::getRateSubcategoryImage($_REQUEST['shop_id'],$_REQUEST['subcategory_id']);
    		
    		$response['rateimgdetails'] = $rateimages;
    		$response['ratedetails'] = $ratedet;
    		$response['subcategorydetail'] = $subcategorydetail;
    		$response['measurementtype'] = $measurementtype;
    	}
        return $this->render('index',$response);
    }
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	$subcategory_id=0;
    	$shop_id =0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="rate" && isset($_REQUEST['subcategory_id']) && !empty($_REQUEST['subcategory_id']) && isset($_REQUEST['shop_id']) && !empty($_REQUEST['shop_id'])){ 
    		$subcategory_id=$_REQUEST['subcategory_id'];
    		$shop_id = $_REQUEST['shop_id'];
    		$costArr = $_REQUEST['cost'];
    		Rate::deleteAll('subcategory_id = :subcategory_id and shop_id = :shop_id',[':subcategory_id' => $subcategory_id,':shop_id'=>$shop_id]);
	    	foreach ($costArr as $measurement_id => $cost){
	    		$rateModel = new Rate();
	    		$rateModel->measurement_id = $measurement_id;
	    		$rateModel->cost = $cost;
	    		$rateModel->subcategory_id = $subcategory_id;
	    		$rateModel->shop_id = $shop_id;
	    		$rateModel->description = $_REQUEST['description'];
	    		$rateModel->location = $_REQUEST['location'];
	    		$rateModel->save();
	    		$status=1;
	    		$msg= "Rate has been successfully updated";
	    	}
	    	if(isset($_REQUEST['img_path']) && !empty($_REQUEST['img_path'])){
	    		$imageArr = $_REQUEST['img_path'];
		    	RateSubcategoryImage::deleteAll('subcategory_id = :subcategory_id',[':subcategory_id' => $subcategory_id]);
		    	foreach ($imageArr as $imgpath){
		    		$catimgModel = new RateSubcategoryImage();
		    		$catimgModel->subcategory_id = $subcategory_id;
		    		$catimgModel->shop_id = $shop_id;
		    		$catimgModel->img_path = $imgpath;
		    		$catimgModel->save();
		    
		    	}
	    	}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/rate?act=rate&shop_id=".$shop_id."&subcategory_id=".$subcategory_id);
    }
    
    
    public function actionUploadimage(){ 
    	$field_name = $_REQUEST['document_field'] ;
    	$max_width = 100 ;
    	$max_file=10;
    	$scale =1 ;
    	error_reporting(E_ERROR ) ; 
    	$logoDir = 'uploads/image' ;
    	if (isset($_FILES[$field_name])) { 
    		//Get the file information
    		$userfile_name = $_FILES[$field_name]['name'];
    		$userfile_tmp =  $_FILES[$field_name]['tmp_name'];
    		$userfile_size = $_FILES[$field_name]['size'];
    		$userfile_type = $_FILES[$field_name]['type'];
    		$filename = basename($_FILES[$field_name]['name']);
    		$ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
    		 
    		$allowedExt = array('jpg','jpeg','png','gif','doc','docs','pdf');
    		 
    		$docment = array();
    		$docment['id'] = date("Y-m-d").uniqid();
    		$docment['ext'] = $ext ;
    		 
    		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
    		if((!empty($_FILES[$field_name])) && ($_FILES[$field_name]['error'] == 0)) {
    
    			if(in_array($ext, $allowedExt)){
    				$error = "";
    			}else{
    				$error = "Only jpg,png,jpeg,gif,doc,docs,pdf file accepted for upload<br />";
    			}
    			if ($userfile_size > ($max_file*1048576)) {
    				$error.= "Images must be under ".$max_file."MB in size";
    			}
    			$docment['status']=0;
    			if(strlen($error)!=0){
    				$docment['error']=$error;
    			}
    
    		}else{
    			$error= "Select an file for upload";
    		}
    		 
    		if (strlen($error)==0){
    			
    			if (isset($_FILES[$field_name]['name'])){
    				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
    					
    				$newName = $docment['id'] . '.' . $ext;
    				$tempnewName = 'temp'.$docment['id'] . '.' . $ext;
    				$logo_location = $logoDir . DIRECTORY_SEPARATOR . $newName;
    				move_uploaded_file($userfile_tmp, $logo_location);
    				$docment['status']=1;
    				$docment['image'] = $logo_location;
    			}
    
    		}
    		echo  json_encode($docment) ;
    		exit();
    	}
    }
    public function actionGetcurrentlocation()
    { 
    	$citydetails=array();
    	$lat=$_REQUEST['lat'];
    	$lan=$_REQUEST['lan'];
    	$latlan=$lat.",".$lan;
    	$data=file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=".$latlan);
    	$city=json_decode($data);
    
    	if(isset($city->results[0]) && count($city->results[0])>0){
    		if(isset($city->results[0]->formatted_address) && $city->results[0]->formatted_address){
    			$citydetails['currentlocation']=$city->results[0]->formatted_address;
    		}
    		if(isset($city->results[0]->geometry->location->lat) && $city->results[0]->geometry->location->lat){
    			$citydetails['latitude']=$city->results[0]->geometry->location->lat;
    		}
    		if(isset($city->results[0]->geometry->location->lng) && $city->results[0]->geometry->location->lng){
    			$citydetails['longitude']=$city->results[0]->geometry->location->lng;
    		}
    			
    	}
    	echo json_encode($citydetails);
    }    

}
