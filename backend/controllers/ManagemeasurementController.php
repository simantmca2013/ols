<?php

namespace backend\controllers;
use yii;
use common\models\Country;
use common\models\Measurement;

class ManagemeasurementController extends AdminController
{
    public function actionIndex()
    {
       $response=array();
    	if(isset($_REQUEST['measurement_id']) && $_REQUEST['act']=="edit"){
    		$measurement_id=$_REQUEST['measurement_id'];
    		$measurementdetails= Measurement::findOne($measurement_id);
    		$response['measurementdetails']=$measurementdetails;
    		 
    	}elseif(isset($_REQUEST['measurement_id']) && $_REQUEST['act']=="delete"){
    		$measurementdetails= Measurement::findOne($_REQUEST['measurement_id']);
    		if($measurementdetails->delete()){
    			$msg="Measurement has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/measurementlist");
    		}
    	}
    	return $this->render('index',$response);
    }
    
    /**
     *  Add and Edit Country
     */
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['measurement_id'])){
    		$measurement_id=$_REQUEST['measurement_id'];
    		$measurementModel=Measurement::findOne($measurement_id);
    		 
    	}else{
    		$measurementModel=new Measurement();
    	}
    	$measurementModel->measurement_name=$_REQUEST['measurement_name'];
    	$measurementModel->measurement_sort_name=$_REQUEST['measurement_sort_name'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$measurementModel->is_active=1;
    	}else{
    		$measurementModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['measurement_id'])){
    		$measurementModel->updated_on = date('Y-m-d H:i:s');
    		$measurementModel->updated_by = Yii::$app->session['user_id'];
    		if($measurementModel->update()){
    			$status=1;
    			$msg="Measurement has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$measurementModel->created_on = date('Y-m-d H:i:s');
    		$measurementModel->created_by = Yii::$app->session['user_id'];;
    		if($measurementModel->save()){
    			$status=1;
    			$msg="Measurement has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/measurementlist");
    }
    

}
