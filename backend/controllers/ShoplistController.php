<?php

namespace backend\controllers;

use common\models\Shop;

class ShoplistController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
    	$shoplist=Shop::getShop('','');
    	$response['shoplist']=$shoplist;
    	return $this->render('index',$response);
    }

}
