<?php

namespace backend\controllers;

use common\models\Vehicletype;

class VehicletypelistController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
    	$vehicletypelist=Vehicletype::getVehicletype('');
    	$response['vehicletypelist']=$vehicletypelist;
    	return $this->render('index',$response);
    }

}
