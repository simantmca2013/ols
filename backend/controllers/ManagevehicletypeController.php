<?php

namespace backend\controllers;
use yii;
use common\models\Vehicletype;

class ManagevehicletypeController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
    	if(isset($_REQUEST['vehicletype_id']) && $_REQUEST['act']=="edit"){
    		$vehicletype_id=$_REQUEST['vehicletype_id'];
    		$vehicletypedetails= Vehicletype::findOne($vehicletype_id);
    		$response['vehicletypedetails']=$vehicletypedetails;
    		 
    	}elseif(isset($_REQUEST['vehicletype_id']) && $_REQUEST['act']=="delete"){
    		$vehicletypedetails=Vehicletype::findOne($_REQUEST['vehicletype_id']);
    		if($vehicletypedetails->delete()){
    			$msg="Vehicletype has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/vehicletypelist");
    		}
    	}
    	return $this->render('index',$response);
    }
    
   
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['vehicletype_id'])){
    		$vehicletype_id=$_REQUEST['vehicletype_id'];
    		$vehicletypeModel=Vehicletype::findOne($vehicletype_id);
    		 
    	}else{
    		$vehicletypeModel=new Vehicletype();
    	}
    	$vehicletypeModel->vehicletype_name=$_REQUEST['vehicletype_name'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$vehicletypeModel->is_active=1;
    	}else{
    		$vehicletypeModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['vehicletype_id'])){
    		$vehicletypeModel->updated_on = date('Y-m-d H:i:s');
    		$vehicletypeModel->updated_by = Yii::$app->session['user_id'];
    		if($vehicletypeModel->update()){
    			$status=1;
    			$msg="Vehicletype has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$vehicletypeModel->created_on = date('Y-m-d H:i:s');
    		$vehicletypeModel->created_by = Yii::$app->session['user_id'];;
    		if($vehicletypeModel->save()){
    			$status=1;
    			$msg="Vehicletype has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/vehicletypelist");
    }
    

}
