<?php

namespace backend\controllers;

use common\models\Driver;
use yii;
class ManagedriverController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
       if(isset($_REQUEST['driver_id']) && $_REQUEST['act']=="edit"){
    		$driver_id=$_REQUEST['driver_id'];
    		$driverDetails=Driver::findOne($driver_id);
    		$response['driverDetails']=$driverDetails;
    		 
    	}elseif(isset($_REQUEST['driver_id']) && $_REQUEST['act']=="delete"){
    		$driverDetails=Driver::findOne($_REQUEST['driver_id']);
    		if($driverDetails->delete()){
    			$msg="Driver has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/driverlist");
    		}
    	}
    	return $this->render('index',$response);
    }
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['driver_id'])){
    		$driver_id=$_REQUEST['driver_id'];
    		$driverModel=Driver::findOne($driver_id);
    		 
    	}else{
    		$driverModel=new Driver();
    	}
    	$driverModel->first_name=$_REQUEST['first_name'];
    	$driverModel->last_name=$_REQUEST['last_name'];
    	$driverModel->mobile_number=$_REQUEST['mobile_number'];
    	$driverModel->email=$_REQUEST['email'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$driverModel->is_active=1;
    	}else{
    		$driverModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['driver_id'])){
    		$driverModel->updated_on = date('Y-m-d H:i:s');
    		if($driverModel->update()){
    			$status=1;
    			$msg="Driver has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$driverModel->created_on = date('Y-m-d H:i:s');
    		
    		if($driverModel->save()){
    			$status=1;
    			$msg="Driver has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/driverlist");
    }
    

}
