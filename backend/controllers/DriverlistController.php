<?php

namespace backend\controllers;

use common\models\Driver;

class DriverlistController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$driverlist=Driver::getDriver('','');
    	$response['driverlist']=$driverlist; 
        return $this->render('index',$response);
    }

}
