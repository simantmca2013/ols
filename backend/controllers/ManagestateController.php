<?php

namespace backend\controllers;
use yii;
use common\models\State;
use common\models\Country;

class ManagestateController extends AdminController
{
	public function beforeAction($action) {
		$this->enableCsrfValidation = ($action->id !== "getstatelist"); // <-- here
		return parent::beforeAction($action);
	}
    public function actionIndex()
    {
       $response=array();
       $countrylist = Country::getCountry('',1);
    	if(isset($_REQUEST['state_id']) && $_REQUEST['act']=="edit"){
    		$state_id=$_REQUEST['state_id'];
    		$statedetails=State::findOne($state_id); 
    		$response['statedetails']=$statedetails;
    		 
    	}elseif(isset($_REQUEST['state_id']) && $_REQUEST['act']=="delete"){
    		$statedetails=State::findOne($_REQUEST['state_id']);
    		if($statedetails->delete()){
    			$msg="State has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/statelist");
    		}
    	}
    	$response['countrylist']=$countrylist;
    	return $this->render('index',$response);
    }

    
    /**
     *  Add and Edit Country
     */
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['state_id'])){
    		$state_id=$_REQUEST['state_id'];
    		$stateModel=State::findOne($_REQUEST['state_id']);
    		 
    	}else{
    		$stateModel=new State();
    	}
    	$stateModel->country_id=$_REQUEST['country_id'];
    	$stateModel->state_name=$_REQUEST['state_name'];
    	$stateModel->state_sort_name=$_REQUEST['state_sort_name'];
    	$stateModel->state_code=$_REQUEST['state_code'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$stateModel->is_active=1;
    	}else{
    		$stateModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['state_id'])){
    		$stateModel->updated_on = date('Y-m-d H:i:s');
    		$stateModel->updated_by = Yii::$app->session['user_id'];
    		if($stateModel->update()){
    			$status=1;
    			$msg="State has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$stateModel->created_on = date('Y-m-d H:i:s');
    		$stateModel->created_by = Yii::$app->session['user_id'];;
    		if($stateModel->save()){
    			$status=1;
    			$msg="State has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/statelist");
    }
   
    public function actionGetstatelist(){
    	$statelist =array();
    	if(isset($_REQUEST['country_id']) && !empty($_REQUEST['country_id'])){
	    	$country_id=$_REQUEST['country_id'];
	    	$statelistdata=State::getState($country_id,'',1);
	    	$i=0;
	    	foreach ($statelistdata as $state){
	    		$statelist[$i]['state_id']=$state['state_id'];
	    		$statelist[$i]['state_name']=$state['state_name'];
	    		$i++;
	    	}
    	}
    	echo  json_encode($statelist) ;
    }
}
