<?php

namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use app\models\Users;



class LoginController extends AdminController
{
    public function actionIndex()
    {   
       $msg = "There is some technical issue.";
		$status = 0;
		if (isset ( $_REQUEST ['password'] ) && $_REQUEST ['username'] != '') {
			$username = $_REQUEST ['username'];
			$password = $_REQUEST ['password'];
			$model=new Users();
			$modalval = $model->login ($username, $password);
			if ($modalval == 2) {
				$msg = "Please verify your account.";
				$status = 0;
				echo 2;
			} elseif ($modalval == 3) {
				$msg = "Your account has been deactivated please contact admin.";
				$status = 0;
				echo 3;
			} elseif ($modalval == 1) {
				$this->redirect ( Yii::$app->getUrlManager ()->getBaseUrl ()."/dashboard" );
				break;
				
			} elseif ($modalval == 0) {
				$msg = "Wrong email id and password.";
				$status = 0;
			}
		}
		Yii::$app->getSession ()->setFlash ('status', $status );
		Yii::$app->getSession ()->setFlash ('msg', $msg );
		$this->redirect ( Yii::$app->getUrlManager ()->getBaseUrl() );
    }
	
    
    /**
     *  Log out
     */
    public function actionLogout()
    { 
	    $session = Yii::$app->session;
	    if (! $session->isActive) {
	    	$session->open ();
	    }
	    $session->destroy ();
	    $this->redirect ( Yii::$app->getUrlManager ()->getBaseUrl () );
	    //$this->redirect(array("admin/index"));
    }
}
