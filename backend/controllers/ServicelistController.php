<?php

namespace backend\controllers;

use common\models\Service;

class ServicelistController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
    	$servicelist=Service::getService('','','','');
    	$response['servicelist']=$servicelist;
    	return $this->render('index',$response);
    }

}
