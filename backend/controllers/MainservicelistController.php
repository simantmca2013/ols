<?php

namespace backend\controllers;

use common\models\Mainservice;

class MainservicelistController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
    	$mainservicelist=Mainservice::getMainservice('');
    	$response['mainservicelist']=$mainservicelist;
    	return $this->render('index',$response);
    }

}
