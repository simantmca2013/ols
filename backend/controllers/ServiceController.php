<?php
namespace backend\controllers;

use common\models\Shop;
use common\models\Mainservice;
use common\models\Category;
use common\models\Subcategory;
use common\models\Service;
use yii;

class ServiceController extends AdminController
{
    public function actionIndex()
    {
    	$response = array();
    	$mainservice = array();
    	$subservice = array();
    	$subservicetype = array();
    	if(isset($_REQUEST['shop_id']) && !empty($_REQUEST['shop_id'])){
    		$serviceProviderDetails=Shop::findOne($_REQUEST['shop_id']);
    		$mainServiceList = Mainservice::getMainservice(1);
    		$subServiceList = Category::getCategory(1,'');
    		$subServicetypeList = Subcategory::getSubcategory('','',1);
    		
    		foreach($mainServiceList as $mslist){
    			$mainservice[$mslist['mainservice_id']] = $mslist['mainservice_name'];
    		}
    		foreach($subServiceList as $slist){
    			$subservice[$slist['category_id']] = $slist;
    		}
    		foreach($subServicetypeList as $sslist){ 
    			$subservicetype[$sslist['category_id']][$sslist['subcategory_id']] = $sslist;
    		}
    		/**** IN EDIT case SHOW PREV RECORD  ******/
    		$serviceDetails= array();
    		$allServices= Service::getService('','',$_REQUEST['shop_id'],'');
    		foreach ($allServices as $services){
    			$serviceDetails[$services['mainservice_id']][$services['category_id']][] = $services['subcategory_id'];
    		}
    		//print_r($serviceDetails);die();
    		
    		/*********** END OF EDIT CASE  ********/
    		//print_r($subservice);print_r($subservicetype);die();
    		$response['serviceDetails'] = $serviceDetails;
    		$response['serviceproviderdetails'] = $serviceProviderDetails;
    		$response['mainservicelist'] = $mainservice;
    		$response['subservicelist'] = $subservice;
    		$response['subservicetype'] = $subservicetype;
    	}
    	
        return $this->render('index',$response);
    }
    
    public function actionOnpost(){
    	$msg= "Oops There is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['service_id']) && !empty($_REQUEST['service_id'])){
    		$serviceIds = $_REQUEST['service_id'];
    		$serviceData = array();
    		Service::deleteAll('shop_id = :shop_id',[':shop_id' => $_REQUEST['shop_id']]);
    		foreach ($serviceIds as $service_idstr){
    			$seviceModel = new Service();
    			$mixarr = explode("_", $service_idstr);
    			$mainservice_id = $mixarr[0];
    			$subservice_id = $mixarr[1];
    			$subservicetype_id = $mixarr[2];
    			$seviceModel->mainservice_id = $mainservice_id;
    			$seviceModel->category_id = $subservice_id;
    			$seviceModel->subcategory_id = $subservicetype_id;
    			$seviceModel->shop_id = $_REQUEST['shop_id'];
    			$seviceModel->created_by = Yii::$app->session['user_id'];
    			$seviceModel->created_on = date('Y-m-d H:i:s');
    			$seviceModel->save();
    			$msg="Service has been successfully updated";
    			$status=1;
    		}
    		
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/service?act=service&shop_id=".$_REQUEST['shop_id']);
    }

}
