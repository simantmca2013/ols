<?php

namespace backend\controllers;
use yii;
use common\models\Mainservice;

class ManagemainserviceController extends AdminController
{
    public function actionIndex()
    {
       $response=array();
    	if(isset($_REQUEST['mainservice_id']) && $_REQUEST['act']=="edit"){
    		$mainservice_id=$_REQUEST['mainservice_id'];
    		$mainservicedetails=Mainservice::findOne($mainservice_id);
    		$response['mainservicedetails']=$mainservicedetails;
    		 
    	}elseif(isset($_REQUEST['mainservice_id']) && $_REQUEST['act']=="delete"){
    		$mainservicedetails=Mainservice::findOne($_REQUEST['mainservice_id']);
    		if($mainservicedetails->delete()){
    			$msg="Main Service has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/mainservicelist");
    		}
    	}
    	return $this->render('index',$response);
    }
    
    
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['mainservice_id'])){
    		$mainservice_id=$_REQUEST['mainservice_id'];
    		$mainserviceModel=Mainservice::findOne($mainservice_id);
    		 
    	}else{
    		$mainserviceModel=new Mainservice();
    	}
    	$mainserviceModel->mainservice_name=$_REQUEST['mainservice_name'];
    	
    	$mainserviceModel->mainservice_description=$_REQUEST['mainservice_description'];
    	$mainserviceModel->mainservice_img_path=$_REQUEST['mainservice_img_path'];
    	if(isset($_REQUEST['is_defaultservice']) && $_REQUEST['is_defaultservice']=="on"){
    		$mainserviceModel->is_defaultservice=1;
    	}else{
    		$mainserviceModel->is_defaultservice=0;
    	}
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$mainserviceModel->is_active=1;
    	}else{
    		$mainserviceModel->is_active=0;
    	}
    	/**** Check any other mainservice is under is_default flag **********/
    	$mainservice_id=0;
    	if(isset($_REQUEST['mainservice_id']) && !empty($_REQUEST['mainservice_id'])){
    		$mainservice_id = $_REQUEST['mainservice_id'];
    	}
    	$isdefault = Mainservice::getDefailtMainservice($mainservice_id);
    	
    	if($isdefault && isset($_REQUEST['is_defaultservice']) && $_REQUEST['is_defaultservice']=="on"){
    		$status = 0;
    		$msg = "You have already assign other mainservice as default, first remove that ";
    	}else{
    	
	    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['mainservice_id'])){
	    		$mainserviceModel->updated_on = date('Y-m-d H:i:s');
	    		$mainserviceModel->updated_by = Yii::$app->session['user_id'];
	    		if($mainserviceModel->update()){
	    			$status=1;
	    			$msg="Main Service has been successfully updated";
	    		}else{
	    			$status=1;
	    			$msg="There is No change to save";
	    		}
	    	}else{
	    		$mainserviceModel->created_on = date('Y-m-d H:i:s');
	    		$mainserviceModel->created_by = Yii::$app->session['user_id'];;
	    		if($mainserviceModel->save()){
	    			$status=1;
	    			$msg="Main service has been successfully added";
	    		}
	    	}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/mainservicelist");
    }
    
    
}
