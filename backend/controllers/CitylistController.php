<?php

namespace backend\controllers;

use common\models\City;

class CitylistController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
        $citylist =City::getCity('','','','');
    	$response['citylist']=$citylist;
    	return $this->render('index',$response);
    }

}
