<?php

namespace backend\controllers;
use yii;
use common\models\Country;

class ManagecountryController extends AdminController
{
    public function actionIndex()
    {
       $response=array();
    	if(isset($_REQUEST['country_id']) && $_REQUEST['act']=="edit"){
    		$country_id=$_REQUEST['country_id'];
    		$countrydetails= Country::findOne($country_id);
    		$response['countrydetails']=$countrydetails;
    		 
    	}elseif(isset($_REQUEST['country_id']) && $_REQUEST['act']=="delete"){
    		$countrydetails=Country::findOne($_REQUEST['country_id']);
    		if($countrydetails->delete()){
    			$msg="Country has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/countrylist");
    		}
    	}
    	return $this->render('index',$response);
    }
    
    /**
     *  Add and Edit Country
     */
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['country_id'])){
    		$country_id=$_REQUEST['country_id'];
    		$countryModel=Country::findOne($country_id);
    		 
    	}else{
    		$countryModel=new Country();
    	}
    	$countryModel->country_name=$_REQUEST['country_name'];
    	$countryModel->country_sort_name=$_REQUEST['country_sort_name'];
    	$countryModel->country_code=$_REQUEST['country_code'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$countryModel->is_active=1;
    	}else{
    		$countryModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['country_id'])){
    		$countryModel->updated_on = date('Y-m-d H:i:s');
    		$countryModel->updated_by = Yii::$app->session['user_id'];
    		if($countryModel->update()){
    			$status=1;
    			$msg="Country has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$countryModel->created_on = date('Y-m-d H:i:s');
    		$countryModel->created_by = Yii::$app->session['user_id'];;
    		if($countryModel->save()){
    			$status=1;
    			$msg="Country has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/countrylist");
    }
    

}
