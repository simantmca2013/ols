<?php

namespace backend\controllers;
use yii;

use common\models\Vehicle;
use common\models\Vehicletype;
use common\models\Shop;

class ManagevehicleController extends AdminController
{
    public function actionIndex()
    {
         $response=array();
        $vehicletypelist = Vehicletype::getVehicletype(1);
        $shoplist = Shop::getShop('',1);
    	if(isset($_REQUEST['vehicle_id']) && $_REQUEST['act']=="edit"){
    		$vehicle_id=$_REQUEST['vehicle_id'];
    		$vehicledetails=Vehicle::findOne($vehicle_id);
    		$response['vehicledetails']=$vehicledetails;
    		 
    	}elseif(isset($_REQUEST['vehicle_id']) && $_REQUEST['act']=="delete"){
    		$vehicledetails=Vehicle::findOne($_REQUEST['vehicle_id']);
    		if($vehicledetails->delete()){
    			$msg="Vehicle has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/vehiclelist");
    		}
    	}
    	$response['vehicletypelist'] = $vehicletypelist;
    	$response['shoplist'] = $shoplist;
    	return $this->render('index',$response);
    }
    
    

    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['vehicle_id'])){
    		$vehicle_id=$_REQUEST['vehicle_id'];
    		$vehicleModel=Vehicle::findOne($vehicle_id);
    		 
    	}else{
    		$vehicleModel=new Vehicle();
    	}
    	$vehicleModel->vehicletype_id=$_REQUEST['vehicletype_id'];
    	$vehicleModel->shop_id=$_REQUEST['shop_id'];
    	$vehicleModel->vehicle_name=$_REQUEST['vehicle_name'];
    	$vehicleModel->vehiclereg_no=$_REQUEST['vehiclereg_no'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$vehicleModel->is_active=1;
    	}else{
    		$vehicleModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['vehicle_id'])){
    		$vehicleModel->updated_on = date('Y-m-d H:i:s');
    		$vehicleModel->updated_by = Yii::$app->session['user_id'];
    		if($vehicleModel->update()){
    			$status=1;
    			$msg="Vehicle has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$vehicleModel->created_on = date('Y-m-d H:i:s');
    		$vehicleModel->created_by = Yii::$app->session['user_id'];;
    		if($vehicleModel->save()){
    			$status=1;
    			$msg="Vehicle has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/vehiclelist");
    }
    
    

}
