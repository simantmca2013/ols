<?php

namespace backend\controllers;
use yii;
use common\models\Service;
use common\models\Category;
use common\models\Shop;
use common\models\Subcategory;
class ManageserviceController extends AdminController
{
	public function beforeAction($action) {
		$this->enableCsrfValidation = ($action->id !== "getsubcategorylist"); // <-- here
		return parent::beforeAction($action);
	}
    public function actionIndex()
    {
        $response=array();
        $categorylist = Category::getCategory(1);
        $shoplist = Shop::getShop('',1);
    	if(isset($_REQUEST['service_id']) && $_REQUEST['act']=="edit"){
    		$service_id=$_REQUEST['service_id'];
    		$servicedetails= Service::findOne($service_id);
    		$response['servicedetails']=$servicedetails;
    		 
    	}elseif(isset($_REQUEST['service_id']) && $_REQUEST['act']=="delete"){
    		$servicedetails=Service::findOne($_REQUEST['service_id']);
    		if($servicedetails->delete()){
    			$msg="Service has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/servicelist");
    		}
    	}
    	$response['categorylist'] = $categorylist;
    	$response['shoplist'] = $shoplist;
    	return $this->render('index',$response);
    }
    
    /**
     *  add and edit Service
     */
    public function actionOnpost(){
    	$msg="Oops There is some technical issue";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['service_id'])){
    		$service_id=$_REQUEST['service_id'];
    		$serviceModel=Service::findOne($service_id);
    		 
    	}else{
    		$serviceModel=new Service();
    	}
    	 
    	$serviceModel->category_id=$_REQUEST['category_id'];
    	$serviceModel->subcategory_id=$_REQUEST['subcategory_id'];
    	$serviceModel->shop_id=$_REQUEST['shop_id'];
    	$serviceModel->rating=$_REQUEST['rating'];
    	$serviceModel->service_location=$_REQUEST['service_location'];
    	$serviceModel->service_description=$_REQUEST['service_description'];
    	if(isset($_REQUEST['is_multimeasurement']) && $_REQUEST['is_multimeasurement']=="on"){
    		$serviceModel->is_multimeasurement=1;
    	}else{
    		$serviceModel->is_multimeasurement=0;
    	}
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$serviceModel->is_active=1;
    	}else{
    		$serviceModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['service_id'])){
    		$serviceModel->updated_on= date('Y-m-d H:i:s');
    		$serviceModel->updated_by=Yii::$app->session['user_id'];
    		if($serviceModel->update()){
    			$status=1;
    			$msg="Subcategory has been successfully edited";
    		}else{
    			$status=1;
    			$msg="There is no change to save";
    		}
    	}else{
    		$serviceModel->created_on= date('Y-m-d H:i:s');
    		$serviceModel->created_by=Yii::$app->session['user_id'];
    		if($serviceModel->save()){
    			$status=1;
    			$msg="Subcategory has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/servicelist");
    }
    
    
    public function actionGetsubcategorylist(){
    	$subcategorylist =array();
    	if(isset($_REQUEST['category_id']) && !empty($_REQUEST['category_id'])){
    		$category_id=$_REQUEST['category_id']; 
    		$subcategorylistdata=Subcategory::getSubcategory('',$category_id,1);
    		$i=0;
    		foreach ($subcategorylistdata as $subcategory){
    			$subcategorylist[$i]['subcategory_id']=$subcategory['subcategory_id'];
    			$subcategorylist[$i]['subcategory_name']=$subcategory['subcategory_name'];
    			$i++;
    		}
    	}
    	echo  json_encode($subcategorylist) ;
    }
}
