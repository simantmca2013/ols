<?php

namespace backend\controllers;
use yii\web;
use yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class AdminController extends Controller
{
    public function actionIndex()
    { 
        return $this->render('index');
    }
    
    public $admin_cookie_language=array();
    
    
    public function beforeAction($action)
    { 
    		
    	$controllername=Yii::$app->controller->id;
    	$lan='english';
    	/**
    	 * This array contain controller.there is no need of session to open the page which is listed in this array
    	 */
    	 $open_withour_session=array(
    			"site","login","forgotpassword","resetpassword"
    	); 
	$all_profile_page = array("login","triplist","dashboard",'frontuserlist','managefrontuser','managestatus','bloglist','manageblog','quiz','quizreport','easymiles','manageeasymiles','wishlist','managewishlist','sponsorshiplist','managesponsorship') ;
	$super_profile_page = array("manageuser","userlist" ) ;
    	if(!in_array($controllername, $open_withour_session)){ 
    		if(Yii::$app->session->get('email')!='' && Yii::$app->session->get('user_id')!=''){
    		if(Yii::$app->session->get('role') == "sales" && !in_array($controllername, $all_profile_page))
			{
				 $this->redirect (Yii::$app->getUrlManager ()->getBaseUrl()."/dashboard");
			}	
			if(Yii::$app->session->get('role') != 'admin' && in_array($controllername,$super_profile_page))
			{
				$this->redirect (Yii::$app->getUrlManager ()->getBaseUrl()."/dashboard");
			}
    		}else{
    			$msg="Please Login.";
    			Yii::$app->getSession()->setFlash ('msg', $msg );
    			$this->redirect (Yii::$app->getUrlManager ()->getBaseUrl());
    		}
    	} 
    	if (parent::beforeAction($action)) {
    		if ($this->enableCsrfValidation && Yii::$app->getErrorHandler()->exception === null && !Yii::$app->getRequest()->validateCsrfToken()) {
    			throw new BadRequestHttpException(Yii::t('yii', 'Unable to verify your data submission.'));
    		}
    		
    	} 
    	
	return true ;
    }

}
