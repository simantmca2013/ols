<?php

namespace backend\controllers;
use yii;
use common\models\Country;
use common\models\State;
use common\models\City;

class ManagecityController extends \yii\web\Controller
{
	public function beforeAction($action) {
		$this->enableCsrfValidation = ($action->id !== "getcitylist"); // <-- here
		return parent::beforeAction($action);
	}
    public function actionIndex()
    {
    	$response=array();
    	$countrylist = Country::getCountry('',1);
    	if(isset($_REQUEST['city_id']) && $_REQUEST['act']=="edit"){
    		$city_id=$_REQUEST['city_id'];
    		$citydetails=City::findOne($_REQUEST['city_id']);
    		$response['citydetails']=$citydetails;
    		 
    	}elseif(isset($_REQUEST['city_id']) && $_REQUEST['act']=="delete"){
    		$citydetails=City::findOne($_REQUEST['city_id']);
    		if($citydetails->delete()){
    			$msg="City has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/citylist");
    		}
    	}
    	$response['countrylist']=$countrylist;
    	return $this->render('index',$response);
    }
    
    
    /**
     *  Add and Edit Country
     */
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['city_id'])){
    		$city_id=$_REQUEST['city_id'];
    		$cityModel=City::findOne($_REQUEST['city_id']);
    	}else{
    		$cityModel=new City();
    	}
    	$cityModel->country_id=$_REQUEST['country_id'];
    	$cityModel->state_id=$_REQUEST['state_id'];
    	$cityModel->city_name=$_REQUEST['city_name'];
    	$cityModel->city_sort_name=$_REQUEST['city_sort_name'];
    	$cityModel->city_code=$_REQUEST['city_code'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$cityModel->is_active=1;
    	}else{
    		$cityModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['city_id'])){
    		$cityModel->updated_on = date('Y-m-d H:i:s');
    		$cityModel->updated_by = Yii::$app->session['user_id'];
    		if($cityModel->update()){
    			$status=1;
    			$msg="City has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$cityModel->created_on = date('Y-m-d H:i:s');
    		$cityModel->created_by = Yii::$app->session['user_id'];;
    		if($cityModel->save()){
    			$status=1;
    			$msg="City has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/citylist");
    }
    
    public function actionGetcitylist(){
    	$citylist =array();
    	if(isset($_REQUEST['country_id']) && !empty($_REQUEST['country_id']) && isset($_REQUEST['state_id']) && !empty($_REQUEST['state_id'])){
    		$country_id=$_REQUEST['country_id'];
    		$state_id=$_REQUEST['state_id'];
    		$citylistdata=City::getCity($country_id,$state_id,'',1);
    		$i=0;
    		foreach ($citylistdata as $city){
    			$citylist[$i]['city_id']=$city['city_id'];
    			$citylist[$i]['city_name']=$city['city_name'];
    			$i++;
    		}
    	}
    	echo  json_encode($citylist) ;
    }

}
