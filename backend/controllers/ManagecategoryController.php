<?php
namespace backend\controllers;
use yii;
use common\models\Category;
use common\models\Mainservice;
use common\models\Measurement;
use common\models\Servicemeasurement;
class ManagecategoryController extends AdminController
{
	public function beforeAction($action) {
		$this->enableCsrfValidation = ($action->id !== "getcurrentCityBylatlan"); // <-- here
		$this->enableCsrfValidation = ($action->id !== "getcategorylist");
		return parent::beforeAction($action);
	}
    public function actionIndex()
    {
    	$response=array();
    	$mainservicelist = Mainservice::getMainservice(1);
    	$measurementlist = Measurement::getMeasurement('',1);
    	if(isset($_REQUEST['category_id']) && $_REQUEST['act']=="edit"){
    		$category_id=$_REQUEST['category_id'];
    		$measurementIds = array();
    		$allmeasurement = Servicemeasurement::getServiceMeasurement($category_id); 
    		foreach ($allmeasurement as $measurements){
    			$measurementIds[] = $measurements['measurement_id'];
    		}
    		$categorydetails=Category::findOne($category_id);
    		$response['measurementIds'] = $measurementIds;
    		$response['categorydetail']=$categorydetails;
    		 
    	}elseif(isset($_REQUEST['category_id']) && $_REQUEST['act']=="delete"){
    		$categorydetails=Category::findOne($_REQUEST['category_id']);
    		if($categorydetails->delete()){
    			$msg="Subservice has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/categorylist");
    		}
    	}
    	$response['measurementlist']=$measurementlist;
    	$response['mainservicelist'] = $mainservicelist;
    	return $this->render('index',$response);
    }
	
    /**
     *  Add and Edit Category
     */
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	$category_id =0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['category_id'])){
    		$category_id=$_REQUEST['category_id'];
    		$categoryModel=Category::findOne($category_id);
    		 
    	}else{
    		$categoryModel=new Category();
    	}
    	$categoryModel->mainservice_id=$_REQUEST['mainservice_id'];
    	$categoryModel->category_name=$_REQUEST['category_name'];
    	
    	$categoryModel->category_description=$_REQUEST['category_description'];
    	$categoryModel->category_image=$_REQUEST['category_img_path'];
    	
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$categoryModel->is_active=1;
    	}else{
    		$categoryModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['category_id'])){
    		$categoryModel->updated_on = date('Y-m-d H:i:s');
    		$categoryModel->updated_by = Yii::$app->session['user_id'];
    		if($categoryModel->update()){
    			$status=1;
    			$msg="Subservice has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$categoryModel->created_on = date('Y-m-d H:i:s');
    		$categoryModel->created_by = Yii::$app->session['user_id'];;
    		if($categoryModel->save()){ 
    			$category_id=$categoryModel->category_id;
    			$status=1;
    			$msg="Subservice has been successfully added";
    		}
    	}
    	Servicemeasurement::deleteAll('category_id = :category_id',[':category_id' => $category_id]);
    	foreach ($_REQUEST['measurement_id'] as $measurement_id){
    		$mappingModel = new Servicemeasurement();
    		$mappingModel->category_id = $category_id;
    		$mappingModel->measurement_id = $measurement_id;
    		$mappingModel->save();
    		
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/categorylist");
    }
    
    public function actionGetcategorylist(){
    	$categorylist =array();
    	if(isset($_REQUEST['mainservice_id']) && !empty($_REQUEST['mainservice_id'])){
    		$mainservice_id=$_REQUEST['mainservice_id'];
    		$categorylistdata=Category::getCategory(1,$mainservice_id);
    		$i=0;
    		foreach ($categorylistdata as $category){
    			$categorylist[$i]['category_id']=$category['category_id'];
    			$categorylist[$i]['category_name']=$category['category_name'];
    			$i++;
    		}
    	}
    	echo  json_encode($categorylist) ;
    }
    
    
    
    
    
    /**
     *  This function responsible to upload a file and return file path
     */
    public function actionUploadimage()
    {
    	$max_width = 100 ;
    	$max_file=10;
    	$detail_max_width = 100;
    	//$detail_max_file = 100;
    	$scale =1 ;
    	$scaleDetail =1 ;
    	error_reporting(E_ERROR ) ;
    	$logoDir = 'uploads/image' ;
    	$thumbDir ='uploads/thumb' ;
    	$detailDir = 'uploads/appdetail' ;
    	if (isset($_FILES['document'])) {
    		//Get the file information
    		$userfile_name = $_FILES['document']['name'];
    		$userfile_tmp =  $_FILES['document']['tmp_name'];
    		$userfile_size = $_FILES['document']['size'];
    		$userfile_type = $_FILES['document']['type'];
    		$filename = basename($_FILES['document']['name']);
    		$ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
    
    		$allowedExt = array('jpg','jpeg','png','gif');
    
    		$docment = array();
    		$docment['id'] = date("Y-m-d").uniqid();
    		$docment['ext'] = $ext ;
    		 
    		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
    		if((!empty($_FILES["document"])) && ($_FILES['document']['error'] == 0)) {
    
    			if(in_array($ext, $allowedExt)){
    				$error = "";
    			}else{
    				$error = "Only <strong>jpg,jpeg,png,gif</strong> file accepted for upload<br />";
    			}
    			if ($userfile_size > ($max_file*1048576)) {
    				$error.= "Images must be under ".$max_file."MB in size";
    			}
    			$docment['status']=0;
    			if(strlen($error)!=0){
    				$docment['error']=$error;
    			}
    
    		}else{
    			$error= "Select an file for upload";
    		}
    		 
    		if (strlen($error)==0){
    
    			if (isset($_FILES['document']['name'])){
    				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
    
    				$newName = $docment['id'] . '.' . $ext;
    				$tempnewName = 'temp'.$docment['id'] . '.' . $ext;
    				$logo_location = $logoDir . DIRECTORY_SEPARATOR . $newName;
    				//$detailDir_location = $detailDir. DIRECTORY_SEPARATOR . $newName;
    				$thumb_location = $thumbDir . DIRECTORY_SEPARATOR . $newName;
    				move_uploaded_file($userfile_tmp, $logo_location);
    				copy($logo_location, $thumb_location);
    				//copy($logo_location, $detailDir_location);
    				
    				//$detailWidth = $this->getWidth($detailDir_location);
    				//$detailHeight = $this->getHeight($detailDir_location);
    				
    				$width = $this->getWidth($thumb_location);
    				$height = $this->getHeight($thumb_location);
    				
    				/*if ($detailWidth > $max_width){
    					$scale = $max_width/$detailWidth;
    					$uploaded = $this->resizeImage($detailDir_location,$detailWidth,$detailHeight,$scaleDetail);
    				
    				}else{
    					$scale = 1;
    					$uploaded = $this->resizeImage($detailDir_location,$detailWidth,$detailHeight,$scaleDetail);
    				}*/
    				if ($width > $max_width){
    					$scale = $max_width/$width;
    					$uploaded = $this->resizeImage($thumb_location,$width,$height,$scale);
    						
    				}else{
    					$scale = 1;
    					$uploaded = $this->resizeImage($thumb_location,$width,$height,$scale);
    				}
    				$docment['status']=1;
    				$docment['image'] = $logo_location;
    			}
    
    		}
    		echo  json_encode($docment) ;
    		exit();
    	}
    
    
    }
    private function resizeImage($image, $width, $height, $scale) {
    	list ( $imagewidth, $imageheight, $imageType ) = getimagesize ( $image );
    	$imageType = image_type_to_mime_type ( $imageType );
    	$newImageWidth = ceil ( $width * $scale );
    	$newImageHeight = ceil ( $height * $scale );
    	$newImage = imagecreatetruecolor ( $newImageWidth, $newImageHeight );
    	switch ($imageType) {
    		case "image/gif" :
    			$source = imagecreatefromgif ( $image );
    			break;
    		case "image/pjpeg" :
    		case "image/jpeg" :
    		case "image/jpg" :
    			$source = imagecreatefromjpeg ( $image );
    			break;
    		case "image/png" :
    		case "image/x-png" :
    			$source = imagecreatefrompng ( $image );
    			break;
    	}
    	imagecopyresampled ( $newImage, $source, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $width, $height );
    
    	switch ($imageType) {
    		case "image/gif" :
    			imagegif ( $newImage, $image );
    			break;
    		case "image/pjpeg" :
    		case "image/jpeg" :
    		case "image/jpg" :
    			imagejpeg ( $newImage, $image, 90 );
    			break;
    		case "image/png" :
    		case "image/x-png" :
    			imagepng ( $newImage, $image );
    			break;
    	}
    
    	chmod ( $image, 0777 );
    	return $image;
    }
    function getWidth($image) {
    	$size = getimagesize ( $image );
    	$width = $size [0];
    	return $width;
    }
    private function getHeight($image) {
    	$size = getimagesize ( $image );
    	$height = $size [1];
    	return $height;
    }
    private function saveThumb($pathname, $savePathName) {
    	$img = PhpThumbFactory::create ( $pathname );
    	$img->adaptiveResize ( 180, 180 );
    	$img->save ( $savePathName );
    }
    
    
}
