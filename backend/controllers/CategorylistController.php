<?php
namespace backend\controllers;
use common\models\Category;
class CategorylistController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$categorylist=Category::getCategory('');
    	$response['categorylist']=$categorylist;
    	return $this->render('index',$response);
    }

}
