<?php

namespace backend\controllers;
use yii;
use common\models\Shop;
use common\models\Country;


class ManageshopController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
        $countrylist = Country::getCountry('',1);
    	if(isset($_REQUEST['shop_id']) && $_REQUEST['act']=="edit"){
    		$shop_id=$_REQUEST['shop_id'];
    		$shopdetails=Shop::findOne($shop_id);
    		$response['shopdetail']=$shopdetails;
    		 
    	}elseif(isset($_REQUEST['shop_id']) && $_REQUEST['act']=="delete"){
    		$shopdetails=Shop::findOne($_REQUEST['shop_id']);
    		if($shopdetails->delete()){
    			$msg="Service provider has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/shoplist");
    		}
    	}
    	$response['countrylist'] = $countrylist;
    	return $this->render('index',$response);
    }

    /**
     *  Add and Edit Category
     */
    
    public function actionOnpost(){
    	$msg="Oops there is some technical problem";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['shop_id'])){
    		$shop_id=$_REQUEST['shop_id'];
    		$shopModel=Shop::findOne($shop_id);
    		 
    	}else{
    		$shopModel=new Shop();
    	}
    	$shopModel->shop_name=$_REQUEST['shop_name'];
    	$shopModel->first_name=$_REQUEST['first_name'];
    	$shopModel->last_name=$_REQUEST['last_name'];
    	$shopModel->mobile_number=$_REQUEST['mobile_number'];
    	if(isset($_REQUEST['contact_number'])){
    		$shopModel->contact_number=$_REQUEST['contact_number'];
    	}
    	$shopModel->email=$_REQUEST['email'];
    	$shopModel->country_id=$_REQUEST['country_id'];
    	
    	$shopModel->state_id=$_REQUEST['state_id'];
    	$shopModel->city_id=$_REQUEST['city_id'];
    	$shopModel->pin_code=$_REQUEST['pin_code'];
    	$shopModel->address=$_REQUEST['address'];
    	$shopModel->govt_id_proof=$_REQUEST['govt_id_proof'];
    	$shopModel->shop_image=$_REQUEST['shop_image'];
    	$shopModel->estimated_time=$_REQUEST['estimated_time'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$shopModel->is_active=1;
    	}else{
    		$shopModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['shop_id'])){
    		$shopModel->updated_on = date('Y-m-d H:i:s');
    		$shopModel->updated_by = Yii::$app->session['user_id'];
    		if($shopModel->update()){
    			$status=1;
    			$msg="Service provider has been successfully updated";
    		}else{
    			$status=1;
    			$msg="There is No change to save";
    		}
    	}else{
    		$shopModel->created_on = date('Y-m-d H:i:s');
    		$shopModel->created_by = Yii::$app->session['user_id'];
    		if($shopModel->save()){
    			$status=1;
    			$msg="Service provider has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/shoplist");
    }
    
    
    
    
    /**
     *  This function responsible to upload a file and return file path
     */
    public function actionUploaddocs()
    {
    	$max_width = 100 ;
    	$max_file=10;
    	$scale =1 ;
    	error_reporting(E_ERROR ) ;
    	$logoDir = 'uploads/docs' ;
    	if (isset($_FILES['document'])) {
    		//Get the file information
    		$userfile_name = $_FILES['document']['name'];
    		$userfile_tmp =  $_FILES['document']['tmp_name'];
    		$userfile_size = $_FILES['document']['size'];
    		$userfile_type = $_FILES['document']['type'];
    		$filename = basename($_FILES['document']['name']);
    		$ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
    
    		$allowedExt = array('jpg','jpeg','png','gif');
    
    		$docment = array();
    		$docment['id'] = date("Y-m-d").uniqid();
    		$docment['ext'] = $ext ;
    		 
    		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
    		if((!empty($_FILES["document"])) && ($_FILES['document']['error'] == 0)) {
    
    			if(in_array($ext, $allowedExt)){
    				$error = "";
    			}else{
    				$error = "Only <strong>jpg,jpeg,png,gif</strong> file accepted for upload<br />";
    			}
    			if ($userfile_size > ($max_file*1048576)) {
    				$error.= "Images must be under ".$max_file."MB in size";
    			}
    			$docment['status']=0;
    			if(strlen($error)!=0){
    				$docment['error']=$error;
    			}
    
    		}else{
    			$error= "Select an file for upload";
    		}
    		 
    		if (strlen($error)==0){
    
    			if (isset($_FILES['document']['name'])){
    				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
    
    				$newName = $docment['id'] . '.' . $ext;
    				$tempnewName = 'temp'.$docment['id'] . '.' . $ext;
    				$logo_location = $logoDir . DIRECTORY_SEPARATOR . $newName;
    				move_uploaded_file($userfile_tmp, $logo_location);
    				$docment['status']=1;
    				$docment['image'] = $logo_location;
    			}
    
    		}
    		echo  json_encode($docment) ;
    		exit();
    	}
    
    
    }
    private function resizeImage($image, $width, $height, $scale) {
    	list ( $imagewidth, $imageheight, $imageType ) = getimagesize ( $image );
    	$imageType = image_type_to_mime_type ( $imageType );
    	$newImageWidth = ceil ( $width * $scale );
    	$newImageHeight = ceil ( $height * $scale );
    	$newImage = imagecreatetruecolor ( $newImageWidth, $newImageHeight );
    	switch ($imageType) {
    		case "image/gif" :
    			$source = imagecreatefromgif ( $image );
    			break;
    		case "image/pjpeg" :
    		case "image/jpeg" :
    		case "image/jpg" :
    			$source = imagecreatefromjpeg ( $image );
    			break;
    		case "image/png" :
    		case "image/x-png" :
    			$source = imagecreatefrompng ( $image );
    			break;
    	}
    	imagecopyresampled ( $newImage, $source, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $width, $height );
    
    	switch ($imageType) {
    		case "image/gif" :
    			imagegif ( $newImage, $image );
    			break;
    		case "image/pjpeg" :
    		case "image/jpeg" :
    		case "image/jpg" :
    			imagejpeg ( $newImage, $image, 90 );
    			break;
    		case "image/png" :
    		case "image/x-png" :
    			imagepng ( $newImage, $image );
    			break;
    	}
    
    	chmod ( $image, 0777 );
    	return $image;
    }
    function getWidth($image) {
    	$size = getimagesize ( $image );
    	$width = $size [0];
    	return $width;
    }
    private function getHeight($image) {
    	$size = getimagesize ( $image );
    	$height = $size [1];
    	return $height;
    }
    private function saveThumb($pathname, $savePathName) {
    	$img = PhpThumbFactory::create ( $pathname );
    	$img->adaptiveResize ( 180, 180 );
    	$img->save ( $savePathName );
    }
    
}
