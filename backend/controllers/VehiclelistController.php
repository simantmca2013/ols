<?php

namespace backend\controllers;

use common\models\Vehicle;

class VehiclelistController extends AdminController
{
    public function actionIndex()
    {
        $response=array();
    	$vehiclelist=Vehicle::getVehicle('','');
    	$response['vehiclelist']=$vehiclelist;
    	return $this->render('index',$response);
    }

}
