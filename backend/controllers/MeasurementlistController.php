<?php

namespace backend\controllers;
use common\models\Measurement;

class MeasurementlistController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$measurementlist = Measurement::getMeasurement('','');
    	$response['measurementlist']=$measurementlist;
    	return $this->render('index',$response);
    }

}
