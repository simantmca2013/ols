<?php

namespace backend\controllers;
use yii;
use common\models\Service;
use common\models\Servicemeasurement;
use common\models\Measurement;

class ManageservicemeasurementController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$servicelist=Service::getService('','','',1);
    	$measurementlist = Measurement::getMeasurement('',1);
    	$response['servicelist']=$servicelist;
    	$response['measurementlist']=$measurementlist;
    	 
    	if(isset($_REQUEST['servicemeasurement_id']) && $_REQUEST['act']=="edit"){
    		$servicemeasurement_id=$_REQUEST['servicemeasurement_id'];
    		$servicemeasurementdetails=Servicemeasurement::findOne($servicemeasurement_id);
    		$response['servicemeasurementdetails']=$servicemeasurementdetails;
    		 
    	}elseif(isset($_REQUEST['servicemeasurement_id']) && $_REQUEST['act']=="delete"){
    		$servicemeasurementdetails=Servicemeasurement::findOne($_REQUEST['servicemeasurement_id']);
    		if($servicemeasurementdetails->delete()){
    			$msg="Service Measurement has been successfully deleted";
    			$status=1;
    			Yii::$app->getSession()->setFlash('status',$status);
    			Yii::$app->getSession()->setFlash('msg',$msg);
    			$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/servicemeasurementlist");
    		}
    	}
    	return $this->render('index',$response);
    }
	
    public function actionOnpost(){
    	$msg="Oops There is some technical issue";
    	$status=0;
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['servicemeasurement_id'])){
    		$servicemeasurement_id=$_REQUEST['servicemeasurement_id'];
    		$servicemeasurementModel=Servicemeasurement::findOne($servicemeasurement_id);
    		 
    	}else{
    		$servicemeasurementModel=new Servicemeasurement();
    	}
    	 
    	$servicemeasurementModel->service_id=$_REQUEST['service_id'];
    	$servicemeasurementModel->measurement_id=$_REQUEST['measurement_id'];
    	$servicemeasurementModel->cost=$_REQUEST['cost'];
    	if(isset($_REQUEST['is_active']) && $_REQUEST['is_active']=="on"){
    		$servicemeasurementModel->is_active=1;
    	}else{
    		$servicemeasurementModel->is_active=0;
    	}
    
    	if(isset($_REQUEST['act']) && $_REQUEST['act']=="edit" && isset($_REQUEST['servicemeasurement_id'])){
    		$servicemeasurementModel->updated_on= date('Y-m-d H:i:s');
    		$servicemeasurementModel->updated_by=Yii::$app->session['user_id'];
    		if($servicemeasurementModel->update()){
    			$status=1;
    			$msg="Service Measurement has been successfully edited";
    		}else{
    			$status=1;
    			$msg="There is no change to save";
    		}
    	}else{
    		$servicemeasurementModel->created_on= date('Y-m-d H:i:s');
    		$servicemeasurementModel->created_by=Yii::$app->session['user_id'];
    		if($servicemeasurementModel->save()){
    			$status=1;
    			$msg="Service Measurement has been successfully added";
    		}
    	}
    	Yii::$app->getSession()->setFlash('status',$status);
    	Yii::$app->getSession()->setFlash('msg',$msg);
    	$this->redirect(Yii::$app->getUrlManager()->getBaseUrl()."/servicemeasurementlist");
    }
    
    
}
