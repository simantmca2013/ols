<?php

namespace backend\controllers;

use common\models\Country;

class CountrylistController extends AdminController
{
    public function actionIndex()
    {
    	$response=array();
    	$countrylist = Country::getCountry('','');
    	$response['countrylist']=$countrylist;
    	return $this->render('index',$response);
    }

}
