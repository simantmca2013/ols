/**
 * 
 */
jQuery(function($) {
	var validation_holder;
	
	$("#save").click(function() {
		var validation_holder = 0;
		var mainservice = $("#mainserviceId").val();
		var subservice = $("#categoryId").val();
		var subcategoryname =$("#subcategoryname").val();
		var image_path = $("#doc").val();
		if(mainservice == "" || mainservice==0) {
			$("span.val_mainserviceId").html("Main Service  is required").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_mainserviceId").html("");
		}
		if(subservice == "" || subservice==0) {
			$("span.val_categoryId").html("Subservice  is required").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_categoryId").html("");
		}
		if(subcategoryname == "") {
			$("span.val_subcategoryname").html("Subservice Type name  is required").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_subcategoryname").html("");
		}
		if(image_path == "") {
			$("span.val_doc").html("Subservice Type image is required").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_doc").html("");
		}
		
		if(validation_holder == 1) { 
			return false;
		} else{
			return true;
		}
		
	}); 
}); 