<div class="panel-group" id="accordion" role="tablist"
	aria-multiselectable="true">
	<!--=========== 
							ADD FIELD 
							===============-->
	<div class="panel panel-default" id="form-settings-label">
		<div class="panel-heading add-item" role="tab" id="headingOne">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordion"
					href="#collapseOne" aria-expanded="true"
					aria-controls="collapseOne"> Add Field </a>
			</h4>
		</div>
		<div id="collapseOne" class="panel-collapse collapse in"
			role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				<ul>
					<li class="field-ex input" data-type="input-type-1">Text Field</li>
					<li class="field-ex input" data-type="input-type-2">Long Text Field</li>
					<li class="input" data-type="input-type-3"><span class="sidebar-menu-icon"><img
							src="images/radio-button-icon.png" /></span> Multiple Choice</li>
					<li class="input" data-type="input-type-4"><span class="sidebar-menu-icon"><img
							src="images/checkbox-icon.png" /></span> Check Boxes</li>
					<li class="input" data-type="switch-input"><span class="sidebar-menu-icon"><img
					src="images/date-time-icon.png" /></span>Switch toggle</li>
					<li class="input" data-type="default-button"><span class="sidebar-menu-icon"><img
					src="images/date-time-icon.png" /></span>Button</li>
					<li class="input" data-type="input-type-5"><span class="sidebar-menu-icon"><img
							src="images/dropdown-icon.png" /></span> Dropdown</li>
					<li class="input" data-type="input-type-6"><span class="sidebar-menu-icon"><img
							src="images/email-icon.png" /></span>Rich text field</li>
					<li class="input" data-type="input-type-7"><span class="sidebar-menu-icon"><img
							src="images/password-icon.png" /></span> Datepicker</li>
					<li class="input" data-type="input-type-8"><span class="sidebar-menu-icon"><img
							src="images/ph-number-icon.png" /></span> Timepicker</li>
					<li class="input" data-type="input-type-9"><span class="sidebar-menu-icon"><img
							src="images/number-icon.png" /></span>Range</li>
					<li class="input" data-type="input-type-10"><span class="sidebar-menu-icon"><img src="images/date-icon.png" /></span>
					Camera capture
					</li>
					<li class="input" data-type="input-type-11"><span class="sidebar-menu-icon"><img src="images/time-icon.png" /></span>
						Audio</li>
					<li class="input" data-type="input-type-12"><span class="sidebar-menu-icon"><img
							src="images/date-time-icon.png" /></span>Video</li>
					<li class="input" data-type="input-type-13"><span class="sidebar-menu-icon"><img
							src="images/date-time-icon.png" /></span>File Upload</li>
				</ul>
			</div>
		</div>
	</div>
	<!--==============
							ADD LAYOUT 
							==================-->
	<div class="panel panel-default" id="form-settings-label">
		<div class="panel-heading add-layout" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
					aria-controls="collapseTwo"> add layout </a>
			</h4>
		</div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
			aria-labelledby="headingTwo">
			<div class="panel-body">
				<ul>
					<li class="draggable field-ex" data-type="section-full-width">Full width Section</li>
					<li class="draggable field-ex" data-type="section-header">Section heading</li>
					<li class="input field-ex" data-type="paragraph">HTML Content</li>
					<li class="draggable field-ex" data-type="page-break">Page break</li>
					<!--li class="draggable field-ex" data-type="geo-location">Geo location</li-->
				</ul>
			</div>
		</div>
	</div>
	<!--=======================
							EDIT SELECTED FIELD 
							===========================-->
	<div class="panel panel-default" id="form-settings-label">
		<div class="panel-heading edit-field" role="tab" id="headingThree">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapseThree"
					aria-expanded="false" aria-controls="collapseThree"> Edit selected
					field</a>
			</h4>
		</div>
		<div id="collapseThree" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingThree">
			<div class="panel-body">
				<form id="edit_settings_form">
					<!--div class="form-group">
						<label>Text Validation</label> <select
							class="custom-field form-control">
							<option value="none">None</option>
							<option value="true">True</option>
							<option value="false">False</option>
						</select>
					</div>
					<div class="checkbox">
						<label> <input type="checkbox"> <span class="custom-checkbox">Answer
								Required</span>
						</label>
					</div>
					<div class="form-group">
						<label>Pre Defined Text</label> <input type="text"
							class="custom-field form-control">
					</div>
					<div class="form-group">
						<label>Help Text</label> <input type="text"
							class="custom-field form-control">
					</div-->
				</form>
			</div>
		</div>
	</div>
</div>
<div class="form-group preview">
	<button class="preview-btn btn">
		<img src="images/preview-icon.png" /> Preview
	</button>
</div>