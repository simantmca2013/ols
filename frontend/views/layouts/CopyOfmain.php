<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\controllers\FrontController;
use app\models\Registration;
use common\models\SecretQuestion;
use common\models\Sponsorships;
use common\models\Homebanner;
use common\models\Blogs;
use common\models\Notification;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php 


/**
  *  Pass language Translation in header
 */
$front=new FrontController(1, new Registration());
$front->beforeAction("");
$language=$front->cookie_language;
//$response['language']=$language;

?>

<?php 
/**
 *  Handle Home page Banner
 */
	$banner=Homebanner::getbanner(1);
	
/**
 *  Notification count
 */
	$unreadnotification_count='';
	if(Yii::$app->session->get('registration_id')){
		$registration_id=Yii::$app->session->get('registration_id');
		$notificationcount=Notification::getUnreadNotificationCount('1',$registration_id);
		if(isset($notificationcount[0]['unread'])){
			$unreadnotification_count=$notificationcount[0]['unread'];
		}
	}
?>

    	
<?php 
/**
  *  Manage Login place (to show 8 charactor name only 
 **/
$displayname='';
if(yii::$app->session->get('frontname')!=''){
	if(strlen(Yii::$app->session->get('frontname')) <= 20){
		$displayname = Yii::$app->session->get('frontname');
	}elseif(strlen(Yii::$app->session->get('first_name')) >= 21){
		$displayname = substr(Yii::$app->session->get('first_name'), 0, 20);
	}else{
		$displayname=Yii::$app->session->get('first_name')." ". substr(Yii::$app->session->get('last_name'), 0 , 1);
	}
}elseif(yii::$app->session->get('frontemail')!=''){
	$displayname=substr(Yii::$app->session->get('frontemail'), 0, 20)."...";
}
?>


<?php 	 
 $secretquestionlist=SecretQuestion::getsecretquestionlist('',1);
 ?>
 <?php 
 /********  referal link  for share *********************/ 
 if(isset($_REQUEST['sponsorships_id']) && $_REQUEST['sponsorships_id'] && isset($_REQUEST['act']) && $_REQUEST['act']=="details"){
 	$sponsorships_id=$_REQUEST['sponsorships_id']; 
 	$sponsorshipdetails=Sponsorships::findOne($sponsorships_id);
 	if(count($sponsorshipdetails) >0){
 	$authkey=base64_encode(Yii::$app->session->get('registration_id').'#'.Yii::$app->params['auth_key'].'#'.$sponsorshipdetails->sponsorships_id);
 	$referal_link=$_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].Yii::$app->getUrlManager()->getBaseUrl().'/registration/redirectlinktoregistration?auth='.$authkey;
 	} 
}
/***     For Blog share on FB  ***********/
if(Yii::$app->getRequest()->getQueryParam('blog_url')!=''){
	$blog_url = trim(Yii::$app->getRequest()->getQueryParam('blog_url'));
	$blogdetails=Blogs::getblogbyblogurl($blog_url,1);
	if(count($blogdetails)>0){
		$blogdetailsmeta=$blogdetails;
	}
}
 ?>
	

<?php //$this->render("//layouts/header",$response); ?>
<?php include('header.php');?>
<?php if(Yii::$app->session->get('secret_question_id')!='' && Yii::$app->session->get('secret_answer')!=''  ){?>
<?php include('sidebar.php');?>
<?php }?>
     <?= $content ?>
     
   
     
     
     <?php include ('footer.php');?>
<?php //$this->render("//layouts/footer",$response); ?>


 
 </body>
</html>
