<?php 
$form_data = $GLOBALS['form_data'];
$alert = $GLOBALS['alert']; ?>
	<div class="container">
		<div class="searchbar col-sm-8 col-sm-push-2 col-xs-12">
			<form id="search_rest_form">
				<div class="form-group col-sm-3 nopadding">
					<input type="text" id="location" 
						class="form-control if_alert searchbar-feild border-right" name="loc_name" value="<?php echo $form_data['loc_name'];?>"
						placeholder="Enter Your Locality">
						<input type="hidden"  value="<?php echo $form_data['location'];?>" name="location" id="selected_location"/>
				</div>
				<div class="form-group col-sm-7 nopadding">
					<input type="text" 
						<?php if(isset($alert['type'])) { if($alert['type'] == "rest_name_not_found" && $alert['msg'] != ""){?>
					 	data-toggle="popover" data-placement="bottom" data-content="<?php echo $alert['msg'];?>" 
						<?php } }?>
						class="form-control searchbar-feild" id="restaurant" name="rest_name" value="<?php echo $form_data['rest_name'];?>"
						placeholder="Name of the Restaurant">
						<input type="hidden" name="restaurant_id" value="<?php echo $form_data['restaurant_id'];?>" id="selected_rest"/>
				</div>
				<div class="form-group col-sm-2 nopadding">
					<button id="search_form_submit" type="submit" class="btn find-deals searchbar-feild">find
						deals</button>
				</div>
			</form>
		</div>
	</div>


<?php if(isset($alert['msg']) && $alert['msg'] != "") {?>
<script>
$(function(){
	var alert_type = '<?php echo $alert['type'];?>';
	if(alert_type == "rest_name_not_found")
	{	
		$("#restaurant").popover('show');
	}
	setTimeout(function(){ $(".popover").fadeOut(); }, 5000);
});
</script>
<?php }?>