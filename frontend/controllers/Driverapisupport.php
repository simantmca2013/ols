<?php

namespace frontend\controllers;

use yii;
use app\models\Drivermodelsupport;
class Driverapisupport  {
	
	private $commonModelObj;
	function __construct(){
		$this->commonModelObj = new Drivermodelsupport();
	}
	
	public function driverRegistration($requestData){ 
		$response = array();
		
		$requestData['last_password_changed']=date('Y-m-d H:i:d'); 
		if($this->commonModelObj->isDuplicateEmail($requestData['email'])){ 
			$response['status'] =0;
			$response['message'] = "Email is already exist";
		}else{ 
			$response = $this->commonModelObj->doRegistration($requestData);
		}
		return $response;
	}
	
	public function login($email,$password){
		$loginData = $this->commonModelObj->login($email, $password);
		$response['status'] =0;
		if(!empty($loginData)){
			$response['status'] =1;
			$response['driver_id'] =$loginData->driver_id;
			$response['first_name'] =$loginData->first_name;
			$response['last_name'] =$loginData->last_name;
			$response['full_name'] =ucfirst($loginData->first_name)." ".ucfirst($loginData->last_name);
			$response['mobile'] =$loginData->mobile_number;
			$response['email'] =$loginData->email;
			$response['last_name'] =$loginData->last_name;
			$response['api_key'] = base64_encode($loginData->driver_id."#".Yii::$app->params ['vender_api_key']."#".$loginData->last_password_changed);
			
		}
		return $response;
	}
	
	public function getDriverDetail($driver_id){
		$responseData = array();
		$driverDetails = $this->commonModelObj->getProfileDetails($driver_id);
		if(!empty($driverDetails)){
			$responseData['driver_id'] = $driverDetails->driver_id;
			$responseData['first_name'] = $driverDetails->first_name;
			$responseData['last_name'] = $driverDetails->last_name;
			$responseData['email'] = $driverDetails->email;
			$responseData['mobile'] = $driverDetails->mobile_number;
		}
		return $responseData;
	}
}
