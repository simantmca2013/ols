<?php

namespace frontend\controllers;
use yii;
use common\models\Driver;
class DriverapiController extends FrontController
{
	private $driver_id; 
	private $auth_type;
	public $enableCsrfValidation = false;
	public function actionIndex() { 
		$status = Yii::$app->params['FAIL_STATUS'];
		$statusCode = Yii::$app->params['FAILED_STATUS_CODE'];
		$msg ="Invalid API Call";
		$data = array();
		$jsondata = file_get_contents ( "php://input" );
		$req_arr = json_decode ( $jsondata );
		if (! json_last_error ()) {
			if (! empty ( $req_arr )) {
				$response = array ();
				if (isset ( $req_arr->api_action ) && isset ( $req_arr->api_key ) && $req_arr->api_key && isset ( $req_arr->auth_type ) && ($req_arr->auth_type == 1 || $req_arr->auth_type == 2 || $req_arr->auth_type == 3)) {
					if ($this->validate_api ( $req_arr->api_key, $req_arr->auth_type )) {
						$driverApiSupport = new Driverapisupport();
						
						if ($this->auth_type == 1) {
							switch ($req_arr->api_action) {
								case 'login' :
									{
										if(isset($req_arr->data) && !empty($req_arr->data)){
											$loginValidatedData = $this->loginDataValidate($req_arr->data);
											if($loginValidatedData['status'] == 1){
												$email = $loginValidatedData['email'];
												$password = $loginValidatedData['password'];
												$loginResponse = $driverApiSupport->login($email,$password);
												$status = Yii::$app->params['WARNING_ERROR_STATUS'];
												$msg = "Email or Password is wrong";
												if($loginResponse['status'] == 1){
													unset($loginResponse['status']);
													$status = Yii::$app->params['SUCCESS_MSG_STATUS'];
													$statusCode = Yii::$app->params['SUCCESS_STATUS_CODE'];
													$msg = "Login successfully";
													$data = $loginResponse;
												}
											}else{
												$status = Yii::$app->params['WARNING_ERROR_STATUS'];
												$msg = $loginValidatedData['message'];
											}
										}
										break;
									}
								case 'register' :
									{ 
										$status = Yii::$app->params['NOTICE_ERROR_STATUS'];
										$statusCode = Yii::$app->params['VALIDATION_STATUS_CODE'];
										$msg = "Request data is not given";
										if(isset($req_arr->data) && !empty($req_arr->data)){
											$validData = $this->isValidRegistration($req_arr->data);
											if($validData['status'] == 1){
												unset($validData['status']);
												$msg = "There is some technical problem";
												$venderStatus = $driverApiSupport->driverRegistration($validData);
												if($venderStatus['status']==1){
													unset($venderStatus['status']);
													$status = Yii::$app->params['SUCCESS_MSG_STATUS'];
													$statusCode = Yii::$app->params['SUCCESS_STATUS_CODE'];
													$msg = "Vender registerd successfully";
													$data = $venderStatus;
												}else{
													$status = Yii::$app->params['WARNING_ERROR_STATUS'];
													$msg = $venderStatus['message'];
												}
											}else{
												$status = Yii::$app->params['WARNING_ERROR_STATUS'];
												$msg = $validData['message'];
											}
										}
										break;
									}
								default :
									{
										$msg = "Invalid Api Action";
									}
							}
						} else if ($this->auth_type == 2) {
							switch ($req_arr->api_action) {
								case 'profiledetail' :
									{
										if(isset($req_arr->data->driver_id) && !empty($req_arr->data->driver_id)){
											$data = $driverApiSupport->getDriverDetail($req_arr->data->driver_id);
											$statusCode = Yii::$app->params['SUCCESS_STATUS_CODE'];
											$status = Yii::$app->params['SUCCESS_MSG_STATUS'];
											$msg ="Vender detail success";
										}else{
											$status = Yii::$app->params['NOTICE_ERROR_STATUS'];
											$msg ="vender id is not given";
										}
										break;
									}
								case 'changepassword' :
									{
										/**
										 * todo Write Profile Update Code Here
										 */
										break;
									}
								default :
									{
										$msg = "Invalid api action";
									}
							}
						} else if ($this->auth_type == 3) {
							switch ($req_arr->api_action) {
									
								/**
								 * TODO Write all api having user_id mean user is loged in
								 */
							}
						} else {
							$msg = "Unauthorized access";
						}
					} else {
						$msg = "Invalid Api call";
					}
				} else {
					$msg = "Invalid Api call ";
				}
			} else {
				$msg = "Invalid Json data ";
			}
		} else {
			$msg  = "Invalid Json data ";
		}
	
		$response['status']= $status;
		$response['status_code'] = $statusCode;
		$response['msg'] = $msg;
		$response['data'] = $data;
		echo json_encode ( $response );
	}
	
	
	public function isValidRegistration($requestData){
		$responseData = array();
		$responseData['status'] = 0;
		if(!isset($requestData->first_name)){
			$responseData['message'] = "First name is required";
			return $responseData;
		}
		if(empty($requestData->first_name)){
			$responseData['message'] = "First name is required";
			return $responseData;
		}
		$responseData['first_name'] = $requestData->first_name;
		if(!isset($requestData->last_name)){
			$responseData['message'] = "Last name is required";
			return $responseData;
		}
		if(empty($requestData->last_name)){
			$responseData['message'] = "Last name is required";
			return $responseData;
		}
		$responseData['last_name'] = $requestData->last_name;
		if(!isset($requestData->mobile)){
			$responseData['message'] = "Mobile is required";
			return $responseData;
		}
		if(empty($requestData->mobile) || !is_numeric($requestData->mobile) || strlen($requestData->mobile) < 10){
			$responseData['message'] = "Mobile number is invalid";
			return $responseData;
		}
		$responseData['mobile_number'] = $requestData->mobile;
		if(!isset($requestData->email)){
			$responseData['message'] = "Email  is required";
			return $responseData;
		}
		if(empty($requestData->email)){
			$responseData['message'] = "Email is required";
			return $responseData;
		}
		$responseData['email'] = $requestData->email;
		if(!isset($requestData->password)){
			$responseData['message'] = "Email  is required";
			return $responseData;
		}
		if(empty($requestData->password)){
			$responseData['message'] = "Email is required";
			return $responseData;
		}
		$responseData['password'] = md5($requestData->password);
		$responseData['status'] = 1;
		return $responseData;
	}
	
	public function loginDataValidate($requestData){
		$responseData = array();
		$responseData['status'] = 0;
		if(empty($requestData->email)){
			$responseData['message'] = "Email is required";
			return $responseData;
		}
		$responseData['email'] = $requestData->email;
		if(!isset($requestData->password)){
			$responseData['message'] = "Email  is required";
			return $responseData;
		}
		if(empty($requestData->password)){
			$responseData['message'] = "Email is required";
			return $responseData;
		}
		$responseData['password'] = $requestData->password;
		$responseData['status'] = 1;
		return $responseData;
	}
	
	/**
	 * Validate Api and get User_id
	 *
	 * @param unknown $api_key
	 * @param unknown $auth_type
	 */
	public function validate_api($api_key, $auth_type) {
		$decoded_api_key = base64_decode ( $api_key );
		if ($auth_type == 1) {
			if (trim ( $decoded_api_key ) == trim ( Yii::$app->params ['driver_api_key'] )) {
				$this->auth_type = $auth_type;
				return true;
			} else {
				return false;
			}
		} else if ($auth_type == 2) {
			$api_key_data = explode ( "#", $decoded_api_key );
			$validate = Driver::validateUser ($api_key_data);
			if (count ($validate ) > 0) {
				$this->driver_id = $validate [0] ['driver_id'];
				$this->auth_type = $auth_type;
				return true;
			} else {
				return false;
			}
		} else if ($auth_type == 3) {
			$api_key_data = explode ( Yii::$app->params ['vender_api_key'], $decoded_api_key );
			if (isset ( $api_key_data [0] ) && isset ( $api_key_data [1] ) && $api_key_data [1] != '' && $api_key_data [0] != '') {
				$validate = AppUser::validateUser ( $api_key_data );
				if (count ( $validate ) > 0) {
					$this->driver_id = $validate [0] ['app_user_id'];
					$this->auth_type = $auth_type;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	

}
