<?php

namespace frontend\controllers;

use yii;
use app\models\User;
use app\models\AppUser;

class ApiController extends FrontController {
	private $user_id;
	private $auth_type;
	public $enableCsrfValidation = false;
	public function actionIndex() {  /***** Status code 505 validation error *******/
		$jsondata = file_get_contents("php://input"); 
		$req_arr = json_decode ( $jsondata ); 
		if (! json_last_error ()) { 
			if (! empty ( $req_arr)) {
				$response = array ();
				if (isset ( $req_arr->api_action ) && isset (  $req_arr->api_key ) &&  $req_arr->api_key && isset ($req_arr->auth_type) && ( $req_arr->auth_type== 1 ||  $req_arr->auth_type == 2 ||  $req_arr->auth_type == 3)) {
					if ($this->validate_api ($req_arr->api_key,  $req_arr->auth_type)) { 
						$apisupport = new Apisupport ();
						if ($this->auth_type == 1) { 
							/**
							 * Auth type 1=> Not login 2=>login 3=>forgot pass*
							 */
							switch ( $req_arr->api_action) { 
								case 'mainservicelist' :
									{ 
										$response ['status'] = 'success';
										$response ['status_code'] = 200;
										$response ['msg'] = 'Main service listing success';
										$response ['data'] = $apisupport->getMainserviceList ();
										break;
									}
								case 'categorylist' :
									{
										$mainservice_id = 0;
										if (isset ($req_arr->data->mainservice_id) && ! empty ( $req_arr->data->mainservice_id)) {
											$mainservice_id =  $req_arr->data->mainservice_id;
										}
										$response ['status'] = 'success';
										$response ['status_code'] = 200;
										$response ['msg'] = 'Category listing success';
										$response ['data'] = $apisupport->getCategoryList ( $mainservice_id );
										break;
									}
								case 'subcategorylist' :
									{ 
										$status = "fail";
										$status_code = "500";
										$data = array ();
										$msg = "Opps there is some technical issue ";
										if (isset ($req_arr->data->category_id ) && ! empty ( $req_arr->data->category_id)) {
											$status = "success";
											$status_code = 200;
											$msg = "Category listing success";
											$data = $apisupport->getSubcategorylist($req_arr->data->category_id);
										} else {
											$msg = "fail";
											$status_code = 500;
											$msg = "Category id is required";
										}
										$response ['status'] = $status;
										$response ['status_code'] = $status_code;
										$response ['msg'] = $msg;
										$response ['data'] = $data;
										break;
									}
								case 'measurementlist':
									{
										$status = "fail";
										$status_code = "500";
										$data = array ();
										$msg = "Opps there is some technical issue ";
										if (isset ($req_arr->data->category_id ) && ! empty ( $req_arr->data->category_id)) {
											$status = "success";
											$status_code = 200;
											$msg = "Measurement listing success";
											$data = $apisupport->getMeasurementlist($req_arr->data->category_id);
										} else {
											$msg = "fail";
											$status_code = 500;
											$msg = "Category id is required";
										}
										
										$response ['status'] = $status;
										$response ['status_code'] = $status_code;
										$response ['msg'] = $msg;
										$response ['data'] = $data;
										break;
									}
								case 'servicelist' :
									{
										$status = "fail";
										$status_code = "500";
										$data = array ();
										$msg = "Opps there is some technical issue ";
										
										if (isset($req_arr->data->mainservice_id) && ! empty ($req_arr->data->mainservice_id) && is_numeric ($req_arr->data->mainservice_id )) {
											if (isset($req_arr->data->category_id) && ! empty ($req_arr->data->category_id) && is_numeric ($req_arr->data->category_id)) {
												if (isset ($req_arr->data->subcategory_id) && ! empty ($req_arr->data->subcategory_id) && is_numeric($req_arr->data->subcategory_id)) {
													$mainservice_id = $req_arr->data->mainservice_id;
													$category_id = $req_arr->data->category_id;
													$subcategory_id = $req_arr->data->subcategory_id;
													$data = $apisupport->getServicelist ($mainservice_id,$category_id,$subcategory_id);
													$status="success";
													$status_code = 200;
												} else {
													$msg = "Subcategory id not given or Invalid";
												}
											} else {
												$msg = "Category id not given or Invalid";
											}
										} else {
											$msg = "Main service id not given or Invalid";
										}
										
										$response ['status'] = $status;
										$response ['status_code'] = $status_code;
										$response ['msg'] = $msg;
										$response ['data'] = $data;
										break;
									}
								case 'servicedetails':{
									$status = "fail";
									$status_code = "500";
									$data = array ();
									$msg = "Opps there is some technical issue ";
									if (isset($req_arr->data->subcategory_id) && !empty ($req_arr->data->subcategory_id) && is_numeric($req_arr->data->subcategory_id) && isset($req_arr->data->shop_id ) && !empty ($req_arr->data->shop_id) && is_numeric($req_arr->data->shop_id)) {
										$data = $apisupport->getShopDetails($req_arr->data->subcategory_id,$req_arr->data->shop_id);
										$status = "success";
										$status_code = 200;
										$msg="Service detail successfull";
									}else{
										$msg = "subcategory id OR shop id not given or invalid";
									}
									$response ['status'] = $status;
									$response ['status_code'] = $status_code;
									$response ['msg'] = $msg;
									$response ['data'] = $data;
									break;
								}
								case 'login':{
									$status = "fail";
									$status_code = "500";
									$data = array ();
									$msg = "Opps there is some technical issue ";
									/***** Validation ****/
									if(!isset($req_arr->data->username)){
										$status_code = 505;
										$msg = "User name required";
									}elseif (!isset($req_arr->data->password)){
										$status_code = 505;
										$msg = "Password required";
									}
									if($status_code!=505){  
										$userData = $apisupport->loginAppUser($req_arr->data->username,$req_arr->data->password);
										if(!empty($userData)){
											$status_code=200;
											$msg="Login successfully";
											$status ="success";
											$data['app_user_id'] = $userData['app_user_id'];
											$data['email'] = $userData['email'];
											$data['first_name'] = $userData['first_name'];
											$data['last_name'] = $userData['last_name'];
											$data['mobile'] = $userData['mobile'];
											$data['name'] = $userData['first_name'].' '.$userData['last_name'] ;
											$data['api_key'] =base64_encode($userData['app_user_id'].'#'.trim ( Yii::$app->params ['api_key'] ).'#'.$userData['last_password_changed']);
										}else{
											$msg = "Username or Password is wrong";
										}
									}
									$response ['status'] = $status;
									$response ['status_code'] = $status_code;
									$response ['msg'] = $msg;
									$response ['data'] = $data;
									break;
								}
								case 'register':{ 
									$status = "fail";
									$status_code = "500";
									$data = array ();
									$msg = "Opps there is some technical issue ";
									/***** Validation ****/
									if(!isset($req_arr->data->first_name)){
										$status_code = 505;
										$msg = "First name required";
									}elseif (empty($req_arr->data->first_name)){
										$status_code = 505;
										$msg = "First name required";
									}elseif (!isset($req_arr->data->last_name)){
										$status_code = 505;
										$msg = "Last name required";
									}elseif (empty($req_arr->data->last_name)){
										$status_code = 505;
										$msg = "Last name required";
									}
									elseif (!isset($req_arr->data->email)){
										$status_code = 505;
										$msg = "Email is required";
									}elseif (empty($req_arr->data->email)){
										$status_code = 505;
										$msg = "Email is required";
									}/*elseif (!isset($req_arr->data->location)){
										$status_code = 505;
										$msg = "Location is required";
									}elseif (empty($req_arr->data->location)){
										$status_code = 505;
										$msg = "Location is required";
									}*/
									elseif (!isset($req_arr->data->mobile)){
										$status_code = 505;
										$msg = "Mobile is required";
									}elseif (empty($req_arr->data->mobile)){
										$status_code = 505;
										$msg = "Mobile is required";
									}
									elseif (!isset($req_arr->data->password)){
										$status_code = 505;
										$msg = "Password is required";
									}elseif (empty($req_arr->data->password)){
										$status_code = 505;
										$msg = "Password is required";
									}
									elseif($apisupport->isDuplicateEmail($req_arr->data->email)){
										$status_code = 505;
										$msg = "Email is already registered";
									}
									$req_arr->data->last_password_changed = date('Y-m-d H:i:s'); 
									if($status_code!=505){
										$appUserId = $apisupport->registrationAppUser($req_arr->data);
										if($appUserId > 0){
											$status_code = 200;
											$status ="success";
											$msg = "Register successfully";
											$data['app_user_id'] = $appUserId;
											$data['email'] = $req_arr->data->email;
											$data['first_name'] = $req_arr->data->first_name;
											$data['last_name'] = $req_arr->data->last_name;
											$data['mobile'] = $req_arr->data->mobile;
											$data['name'] = $req_arr->data->first_name.' '.$req_arr->data->last_name ;
											
											$data['api_key'] =base64_encode($appUserId.'#'.trim ( Yii::$app->params ['api_key'] ).'#'.$req_arr->data->last_password_changed);
										}
									}
									$response ['status'] = $status;
									$response ['status_code'] = $status_code;
									$response ['msg'] = $msg;
									$response ['data'] = $data;
									break;
								}
								default :
									{
										$response ['status'] = 'fail';
										$response ['status_code'] = 500;
										$response ['msg'] = "Invalid api action";
										// TODO invalid api action
									}
							}
						} else if ($this->auth_type == 2) {
							switch ($req_arr->api_action) {
								case 'userbid':{ 
									$status = "fail";
									$status_code = "500";
									$data = array ();
									$msg = "Opps there is some technical issue ";
									if($this->isValidRequestData($req_arr->data)){
										if($apisupport->userBidApplication($req_arr->data,$this->user_id)){
											$status_code = 200;
											$status = "success";
											$msg = "User bid has been successfully submited";
										} 
									}else{
										$status_code =500;
										$msg= "May be request data like mainservice id category id subcategory id ,measurement is, shop id location or quantity not given or invalid";
									}
									$response ['status'] = $status;
									$response ['status_code'] = $status_code;
									$response ['msg'] = $msg;
									$response ['data'] = $data;
									break;
								}
								case 'bidlist':{
									$status = "success";
									$status_code = "200";
									$data = array ();
									$msg = "Bid listing success";
									$data = $apisupport->getBidList($this->user_id);
									$response ['status'] = $status;
									$response ['status_code'] = $status_code;
									$response ['msg'] = $msg;
									$response ['data'] = $data;
									break;
								}
								case 'biddetail':{
									$status = "fail";
									$status_code = "500";
									$msg= " bid id is required";
									$data = array ();
									if(isset($req_arr->data->app_user_bid_id) && !empty($req_arr->data->app_user_bid_id) && $req_arr->data->app_user_bid_id > 0){
										$status = "success";
										$status_code = "200";
										$msg = "Bid detail success";
										$data= $apisupport->getBidDetails($this->user_id,$req_arr->data->app_user_bid_id);
									}
									$response ['status'] = $status;
									$response ['status_code'] = $status_code;
									$response ['msg'] = $msg;
									$response ['data'] = $data;
									break;
								}
								case 'venderdetail':
									{
										$status ="fail";
										$status_code = "500";
										$msg= " vender id is required";
										$data = array ();
										if(isset($req_arr->data->vender_id) && !empty($req_arr->data->vender_id) && $req_arr->data->vender_id > 0){
											$status = "success";
											$status_code = "200";
											$msg = "vender list success";
											$data= $apisupport->getShopImage($req_arr->data->vender_id);
										}
										$response ['status'] = $status;
										$response ['status_code'] = $status_code;
										$response ['msg'] = $msg;
										$response ['data'] = $data;
										break;
									}
								case 'order' :
									{
										$status ="fail";
										$status_code = "500";
										$msg= " There is some technical issue";
										$data = array ();
										if(isset($req_arr->data) && !empty($req_arr->data)){
											$validatedOrderdata = $this->isValidOrderRequest($req_arr->data);
											if($validatedOrderdata['status'] == 0){
												$msg = $validatedOrderdata['msg'];
											}else{
												unset($validatedOrderdata['status']);
												$status ="success";
												$status_code = "200";
												$msg= " Order has been saved successfully";
												$data = $apisupport->userOrder($validatedOrderdata,$this->user_id);
											}
										}else{
											$msg = "Request data is not given";
										}
										$response ['status'] = $status;
										$response ['status_code'] = $status_code;
										$response ['msg'] = $msg;
										$response ['data'] = $data;
										
										break;
									}
								case 'orderlist':
									{
										$status ="success";
										$status_code = "200";
										$msg= " Order listing success";
										$data = array ();
										$data = $apisupport->userOrderList($this->user_id);
										$response ['status'] = $status;
										$response ['status_code'] = $status_code;
										$response ['msg'] = $msg;
										$response ['data'] = $data;
										
										break;
									
									}
									
								default :
									{
										$response ['status'] = "fail";
										$response ['status'] = 1;
										$response ['data'] ['msg'] = "Invalid Api Action";
										// TODO invalid api action
									}
							}
						} else if ($this->auth_type == 3) {
							switch ($req_arr->api_action) {
							
							/**
							 * TODO Write all api having user_id mean user is loged in
							 */
							}
						}
					} else {
						$response ['status'] = 1;
						$response ['msg'] = "Unauthorized access";
					}
				} else {
					$response ['status'] = 0;
					$response ['msg'] = "Invalid Api call";
				}
			} else {
				$response ['status'] = 0;
				$response ['msg'] = "Invalid Api call ";
			}
	}else{
		$response ['status'] = 0;
		$response ['msg'] = "Invalid Json data ";
	}
		echo json_encode ( $response );
	}
	
	
	
	public function isValidRequestData($data){ 
		if(!isset($data->mainservice_id)){ 
			return false;
		} 
		if(!is_numeric($data->mainservice_id) || $data->mainservice_id < 1){
			return false;
		}
		if(!isset($data->category_id)){
			return false;
		}
		if(!is_numeric($data->category_id) || $data->category_id < 1){
			return false;
		}
		if(!isset($data->subcategory_id)){
			return false;
		}
		if(!is_numeric($data->subcategory_id) || $data->subcategory_id < 0){
			return false;
		}
		if(!isset($data->measurement_id)){
			return false;
		}
		if(!is_numeric($data->measurement_id) || $data->measurement_id < 0){
			return false;
		}
		
		if(!isset($data->quantity)){
			return false;
		}
		if(!isset($data->location)){
			return false;
		}
		return true;
	}
	/**
	 * Validate Order request data
	 * @param unknown $data
	 */
	
	public function isValidOrderRequest($data){
			$validatedData = array();
			$validatedData['status']=1;
			if(!isset($data->shop_id)){
				$validatedData['status']=0;
				$validatedData['msg']="shop id  is required";
			}
			if(!is_numeric($data->shop_id) || $data->shop_id < 1){
				$validatedData['status']=0;
				$validatedData['msg']=" shop id is not valid";
				return $validatedData;
			}
			$validatedData['shop_id'] = $data->shop_id;
			
			if(!isset($data->category_id)){
				$validatedData['status']=0;
				$validatedData['msg']="Catagory is required";
				return $validatedData;
			}
			if(!is_numeric($data->category_id) || $data->category_id < 1){
				$validatedData['status']=0;
				$validatedData['msg']="Catagory is not valid";
				return $validatedData;
			}
			$validatedData['category_id'] = $data->category_id;
			if(!isset($data->subcategory_id)){
				$validatedData['status']=0;
				$validatedData['msg']="Subcatagory is required";
				return $validatedData;
			}
			if(!is_numeric($data->subcategory_id) || $data->subcategory_id < 0){
				$validatedData['status']=0;
				$validatedData['msg']="Subcatagory is not valid";
				return $validatedData;
			}
			$validatedData['subcategory_id'] = $data->subcategory_id;
			if(!isset($data->measurement_id)){
				$validatedData['status']=0;
				$validatedData['msg']="Measurement is required";
				return $validatedData;
			}
			if(!is_numeric($data->measurement_id) || $data->measurement_id < 0){
				$validatedData['status']=0;
				$validatedData['msg']="Measurement is not valid";
				return $validatedData;
			}
			$validatedData['measurement_id'] = $data->measurement_id;
			if(!isset($data->quantity)){
				$validatedData['status']=0;
				$validatedData['msg']="Quantity is required";
				return $validatedData;
			}
			$validatedData['quantity'] = $data->quantity;
			if(!isset($data->location)){
				$validatedData['status']=0;
				$validatedData['msg']="Location is required";
				return $validatedData;
			}
			$validatedData['location'] = $data->location;
			
			return $validatedData;
	}
	
	/**
	 * Validate Api and get User_id
	 *
	 * @param unknown $api_key        	
	 * @param unknown $auth_type        	
	 */
	public function validate_api($api_key, $auth_type) {
		$decoded_api_key = base64_decode ( $api_key );
		if ($auth_type == 1) {
			if (trim ( $decoded_api_key ) == trim ( Yii::$app->params ['api_key'] )) {
				$this->auth_type = $auth_type;
				return true;
			} else {
				return false;
			}
		} else if ($auth_type == 2) {
			$api_key_data = explode ( "#", $decoded_api_key );
			// $validateuser = array ();
			// $validateuser [0] = $api_key_data [0];
			$validate = AppUser::validateUser ( $api_key_data );
			if (count ( $validate ) > 0) {
				$this->user_id = $validate [0] ['app_user_id'];
				$this->auth_type = $auth_type;
				return true;
			} else {
				return false;
			}
		} else if ($auth_type == 3) {
			$api_key_data = explode ( Yii::$app->params ['api_key'], $decoded_api_key );
			if (isset ( $api_key_data [0] ) && isset ( $api_key_data [1] ) && $api_key_data [1] != '' && $api_key_data [0] != '') {
				$validate = AppUser::validateUser ( $api_key_data );
				if (count ( $validate ) > 0) {
					$this->user_id = $validate [0] ['app_user_id'];
					$this->auth_type = $auth_type;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
