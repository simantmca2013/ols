<?php

namespace frontend\controllers;


use app\models\Vendermodel;
use common\models\Shop;
use yii;
use common\models\Country;
use common\models\State;
use common\models\City;
use common\models\Subcategory;
class Venderapisupport  {
	
	private $commonModelObj;
	function __construct(){
		$this->commonModelObj = new Vendermodel();
	}
	
	public function venderRegistration($requestData){
		$response = array();
		$shopModel = new Shop();
		$requestData['last_password_changed']=date('Y-m-d H:i:d'); 
		if($this->commonModelObj->isDuplicateEmail($requestData['email'])){ 
			$response['status'] =0;
			$response['message'] = "Email is already exist";
		}else{
			foreach ($requestData as $fieldKey => $fieldValue){
				$shopModel->$fieldKey = $fieldValue;
			}
			if($shopModel->save()){
				$response['status'] =1;
				$response['vender_id'] =$shopModel->shop_id;
				$response['first_name'] =$shopModel->first_name;
				$response['last_name'] =$shopModel->last_name;
				$response['full_name'] =ucfirst($shopModel->first_name)." ".ucfirst($shopModel->last_name);
				$response['mobile'] =$shopModel->mobile_number;
				$response['email'] =$shopModel->email;
				$response['last_name'] =$shopModel->last_name;
				$response['api_key'] = base64_encode($shopModel->shop_id."#".Yii::$app->params ['vender_api_key']."#".$shopModel->last_password_changed);
				
			}else{
				$response['status'] =0;
			}
		}
		return $response;
	}
	
	public function login($email,$password){
		$loginData = $this->commonModelObj->login($email, $password);
		$response['status'] =0;
		if(!empty($loginData)){
			$response['status'] =1;
			$response['vender_id'] =$loginData->shop_id;
			$response['first_name'] =$loginData->first_name;
			$response['last_name'] =$loginData->last_name;
			$response['full_name'] =ucfirst($loginData->first_name)." ".ucfirst($loginData->last_name);
			$response['mobile'] =$loginData->mobile_number;
			$response['email'] =$loginData->email;
			$response['last_name'] =$loginData->last_name;
			$response['api_key'] = base64_encode($loginData->shop_id."#".Yii::$app->params ['vender_api_key']."#".$loginData->last_password_changed);
			
		}
		return $response;
	}
	
	public function getShopDetail($shop_id){
		$responseData = array();
		$shopDetails = Shop::findOne($shop_id);
		if(!empty($shopDetails)){
			$responseData['vender_id'] = $shopDetails->shop_id;
			$responseData['first_name'] = $shopDetails->first_name;
			$responseData['last_name'] = $shopDetails->last_name;
			$responseData['mobile'] = $shopDetails->mobile_number;
			$responseData['email'] = $shopDetails->email;
			/*$responseData['shop_name'] = $shopDetails->shop_name;
			$responseData['contact_number'] = $shopDetails->contact_number;
			$responseData['pin_code'] = $shopDetails->pin_code;
			$responseData['address'] = $shopDetails->address;
			$responseData['govt_id_proof'] = $shopDetails->govt_id_proof;
			$responseData['shop_image'] = $shopDetails->shop_image;
			$responseData['estimated_time'] = $shopDetails->estimated_time." hour";
			$responseData['country_id'] ='';
			$responseData['state_id'] ='';
			$responseData['city_id'] ='';
			if(isset($shopDetails->country_id) && $shopDetails->country_id > 0){
				$countryDetail = Country::findOne($shopDetails->country_id);
				$responseData['country_id'] = $countryDetail->country_name;
			}
			if(isset($shopDetails->state_id) &&  $shopDetails->state_id > 0){
				$stateDetail = State::findOne($shopDetails->state_id);
				$responseData['state_id'] = $stateDetail->state_name;
			}
			if($shopDetails->city_id &&  $shopDetails->city_id > 0){
				$cityDetail = City::findOne($shopDetails->city_id);
				$responseData['city_id'] = $cityDetail->city_name;
			}*/
			
		}
		return $responseData;
	}
	
	
	public function userBidList($shop_id){
		$categoryList = array();
		$subcategoryList = array();
		$measurementList = array();
		$response = array();
		$subcategoryIdsWithShop = $this->commonModelObj->getRatedSubcategoryIdByShopId($shop_id); 
		$category = $this->commonModelObj->getCategory();
		$subcategory = $this->commonModelObj->getSubcategory();
		$userBid = $this->commonModelObj->getUserBidList();
		$measurement =$this->commonModelObj->getMeasurement();
		
		foreach($category as $cat){
			$categoryList[$cat['category_id']] = $cat;
		}
		foreach($subcategory as $subcat){
			$subcategoryList[$subcat['subcategory_id']] = $subcat;
		}
		foreach ($measurement as $measur){
			$measurementList[$measur['measurement_id']] = $measur['measurement_name']."( ".$measur['measurement_sort_name']." )";
		}
		$i=0;
		foreach ($userBid as $ub){
			if(in_array($ub['subcategory_id'], $subcategoryIdsWithShop[$shop_id])){
				$response[$i]['app_user_bid_id']=$ub['app_user_bid_id'];
				$response[$i]['user_id']=$ub['app_user_id'];
				$response[$i]['user_image']='';
				$response[$i]['category_id']=$ub['category_id'];
				$response[$i]['subcategory_id']=$ub['subcategory_id'];
				if(isset($categoryList[$ub['category_id']]['category_name'])){
					$response[$i]['category_name']=$categoryList[$ub['category_id']]['category_name'];
				}
				if(isset($subcategoryList[$ub['subcategory_id']]['subcategory_name'])){
					$response[$i]['subcategory_name']=$subcategoryList[$ub['subcategory_id']]['subcategory_name'];
				}
				if(isset($measurementList[$ub['measurement_id']])){
					$response[$i]['measurement']=$measurementList[$ub['measurement_id']];
				}
				$response[$i]['location']=$ub['location'];
				$response[$i]['quantity']=$ub['quantity'];
				$i++;
			}
		}
		return $response;
	}
	
	public function getUserDetail($userId){
		$response = array();
		$userDetails = $this->commonModelObj->getUserDetails($userId);
		if(!empty($userDetails)){
			$response['first_name']= $userDetails->first_name;
			$response['last_name']= $userDetails->first_name;
			$response['mobile']= $userDetails->mobile;
			$response['email']= $userDetails->email;
			$response['user_image']= '';
		}
		return $response;
	}
	
	public function getBidDetails($app_user_bid_id){
		$categoryList = array();
		$subcategoryList = array();
		$measurementList = array();
		$shopList = array();
		$response = array();
		$bidReplyArr = array();
		$bidDetails = array();
		$category = $this->commonModelObj->getCategory();
		$subcategory = $this->commonModelObj->getSubcategory();
		$userBid = $this->commonModelObj->getUserBidList($app_user_bid_id);
		$measurement =$this->commonModelObj->getMeasurement();
		$shop = $this->commonModelObj->shoplisting();
		$shopBidReply = $this->commonModelObj->getShopBidReply($app_user_bid_id);
		
		foreach ($shop as $sh){
			$shopList[$sh['shop_id']]['first_name'] = $sh['first_name'];
			$shopList[$sh['shop_id']]['last_name'] = $sh['last_name'];
			$shopList[$sh['shop_id']]['shop_name'] = $sh['shop_name'];
		}
		foreach($category as $cat){
			$categoryList[$cat['category_id']] = $cat;
		}
		foreach($subcategory as $subcat){
			$subcategoryList[$subcat['subcategory_id']] = $subcat;
		}
		foreach ($measurement as $measur){
			$measurementList[$measur['measurement_id']] = $measur['measurement_name']."( ".$measur['measurement_sort_name']." )";
		}
		$i=0;
		foreach ($userBid as $ub){
			$bidDetails[$i]['app_user_bid_id']=$ub['app_user_bid_id'];
			$bidDetails[$i]['user_id']=$ub['app_user_id'];
			$bidDetails[$i]['user_image']='';
			$bidDetails[$i]['category_id']=$ub['category_id'];
			$bidDetails[$i]['subcategory_id']=$ub['subcategory_id'];
			if(isset($categoryList[$ub['category_id']]['category_name'])){
				$bidDetails[$i]['category_name']=$categoryList[$ub['category_id']]['category_name'];
			}
			if(isset($subcategoryList[$ub['subcategory_id']]['subcategory_name'])){
				$bidDetails[$i]['subcategory_name']=$subcategoryList[$ub['subcategory_id']]['subcategory_name'];
			}
			if(isset($measurementList[$ub['measurement_id']])){
				$bidDetails[$i]['measurement']=$measurementList[$ub['measurement_id']];
			}
			$bidDetails[$i]['location']=$ub['location'];
			$bidDetails[$i]['quantity']=$ub['quantity'];
			$i++;
		}
		$s=0;
		foreach($shopBidReply as $sbr){
			$bidReplyArr[$s]['vender_id'] =$sbr['shop_id'];
			$bidReplyArr[$s]['cost'] = $sbr['cost'];
			if($shopList[$sbr['shop_id']]['first_name']){
				$bidReplyArr[$s]['first_name'] = $shopList[$sbr['shop_id']]['first_name'];
			}
			if($shopList[$sbr['shop_id']]['last_name']){
				$bidReplyArr[$s]['last_name'] = $shopList[$sbr['shop_id']]['last_name'];
			}
			if($shopList[$sbr['shop_id']]['shop_name']){
				$bidReplyArr[$s]['shop_name'] = $shopList[$sbr['shop_id']]['shop_name'];
			}
			$s++;
		}
		$response['biddetails'] = $bidDetails;
		$response['venderlist'] = $bidReplyArr;
		return $response;
	}
	
	public function submitBidReply($app_user_bid_id,$shop_id,$cost){
		return $this->commonModelObj->submitBid($app_user_bid_id,$shop_id,$cost);
		
	}
}
