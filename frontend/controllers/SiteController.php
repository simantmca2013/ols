<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\forms;
use common\models\Vedios;
use common\models\Ideas;
use common\models\Testimonial;


/**
 * Site controller
 */
class SiteController extends FrontController{
	/**
	 * @inheritdoc
	 */
	public $enableCsrfValidation = false;
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'only' => [ 
								'logout',
								'manageform' 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'manageform' 
										],
										'allow' => true,
										'roles' => [ 
												'?' 
										] 
								],
								[ 
										'actions' => [ 
												'manageform' 
										],
										'allow' => true,
										'roles' => [ 
												'@' 
										] 
								] 
						] 
				],
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'logout' => [ 
										'post' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [ 
				'error' => [ 
						'class' => 'yii\web\ErrorAction' 
				],
				'captcha' => [ 
						'class' => 'yii\captcha\CaptchaAction',
						'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null 
				] 
		];
	}
	public function actionIndex() { 
		die("Under development");
    	
	
		//return $this->render('index',$response);
		 
	}
	
	/**
	 *  This funtion return testimonial full details and also Ideas full details
	 */
	public function actionGetmodaldata(){ 
		if(isset($_REQUEST['idwithsection'])){
		$id_sectionarray=explode('_',$_REQUEST['idwithsection']);
		$id=$id_sectionarray[1];
		$description='';
		$name='';
		$image='';
		$designation='';
		$date='';
		if(trim($id_sectionarray[0])=="testimonial"){ 
			$data=Testimonial::findOne($id);
			$description=$data->testimonial_description;
			$name=$data->testimonial_name;
			$image=$data->testimonial_image_path;
			$designation=$data->testimonial_designation;
			$date='';

			$html = "<div class='modal-content'><div class='modal-header'>
				<button type='button' class='close'' data-dismiss='modal' aria-label='Close'>
				<span aria-hidden='true'>&times;</span></button>
				<h4 class='modal-title'  id='myModalLabel'>".$name ."</h4>
				</div><div class='modal-body'>
				<div class='container-fluid'>	
				<div class='row'><div class='col-xs-12 nopadding'>
				<span class='test-modal-img'><img src='".Yii::$app->getUrlManager()->getBaseUrl()."/backend/web/". $image ."'/></span>
				<h4 class='nomt test-modal-name'>". $name."<br> <small>".$designation."</small></h4>
				<p> ". $description."</p>	
				</div>
				</div>
				</div>
				</div>
				</div>";
			
			
		}elseif(trim($id_sectionarray[0])=="ideas"){
			$data=Ideas::findOne($id);
			$description=$data->description;
			$name=$data->ideas_title;
			$image=$data->image_path;
			$designation='';
			$date=date('j M Y',strtotime($data->date));
			
			$html = "<div class='modal-content'><div class='modal-header'>
				<button type='button' class='close'' data-dismiss='modal' aria-label='Close'>
				<span aria-hidden='true'>&times;</span></button>
				<h4 class='modal-title'  id='myModalLabel'>".$name ."</h4>
				</div><div class='modal-body'>
				<div class='container-fluid'><div class='row'>	
				<div class='col-xs-12 nopadding ideas-modal-img-bg'><img src='".Yii::$app->getUrlManager()->getBaseUrl()."/backend/web/". $image ."'/></div></div>
				<div class='row'><div class='col-xs-12 nopadding'>
				<p><b> ". $name."</b>
					<b class='pull-right'>".$designation."</b>
				</p>
				<p> ". $description."</p>	
				</div>
				</div>
				</div>
				</div>
				</div>";
			
		}
		$response['status']=1;
		$response['data']=$html;
	}
	else{
		$response['status']=0;
		$response['data']="No data found";
	}
	echo  json_encode($response) ;
	
	}
	
}