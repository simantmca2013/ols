<?php

namespace frontend\controllers;

use app\models\Commonmodel;
use yii;
use app\models\User;
use app\models\AppUser;
use app\models\AppUserBid;
use common\models\Shop;
use common\models\Country;
use common\models\City;
use common\models\State;
class Apisupport  {
	
	private $commonModelObj;
	function __construct(){
		$this->commonModelObj = New Commonmodel();
	}
	/**
	 *  Main service list
	 */
	public function getMainserviceList(){
		$responseData = array();
		$mainservicelist = $this->commonModelObj->getMainservice();
		$i=0;
		foreach ($mainservicelist as $mainservice){
			$responseData[$i]['mainservice_id'] = $mainservice['mainservice_id'];
			$responseData[$i]['mainservice_name'] = $mainservice['mainservice_name'];
			$responseData[$i]['mainservice_img_path'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl())."/backend/web/". str_replace("image","thumb",$mainservice['mainservice_img_path']);
				
			$i++;
		}
		return $responseData;
	}
	
	/**** Category List *****/
	public function getCategoryList($mainservice_id){ 
		$responseData = array();
		$categorylist = $this->commonModelObj->getCategory($mainservice_id);
		$i=0;
		foreach ($categorylist as $category){
			$responseData[$i]['mainservice_id'] = $category['mainservice_id'];
			$responseData[$i]['category_id'] = $category['category_id'];
			$responseData[$i]['category_name'] = $category['category_name'];
			$responseData[$i]['category_image'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl())."/backend/web/". str_replace("image","thumb",$category['category_image']);
			
			$i++;
		}
		return $responseData;
	}
	/**
	 *  Sub category list
	 * @param unknown $category_id
	 */
	public function getSubcategorylist($category_id){
		$responseData = array();
		$subcategorylist = $this->commonModelObj->getSubcategory($category_id);
		$i=0;
		foreach ($subcategorylist as $subcategory){
			$responseData[$i]['mainservice_id'] = $subcategory['mainservice_id'];
			$responseData[$i]['category_id'] = $subcategory['category_id'];
			$responseData[$i]['subcategory_id'] = $subcategory['subcategory_id'];
			$responseData[$i]['subcategory_name'] = $subcategory['subcategory_name'];
			$responseData[$i]['subcategory_image'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl())."/backend/web/". str_replace("image","thumb",$subcategory['subcategory_image']);
				
			$i++;
		}
		return $responseData;
	}
	
	public function getServicelist($mainservice_id,$category_id,$subcategory_id){
		$response = array();
		$subcategorydetail = $this->commonModelObj->getSubcategory($category_id,$subcategory_id);
		$shopidlist = $this->commonModelObj->getShopidlist($mainservice_id,$category_id,$subcategory_id);
		$shopids = array();
		foreach ($shopidlist as $sid){
			if(!in_array($sid['shop_id'], $shopids)){
				$shopids[] = $sid['shop_id'];
			}
		}
		/** Fetch all shop by $shopids **/
		if(!empty($shopids)){
			$pricelistArr = array();
			$shoplisting = $this->commonModelObj->shoplisting($shopids);
			$pricelisting = $this->commonModelObj->priceListing($subcategory_id,$shopids);
			foreach ($pricelisting as $price){
				$pricelistArr[$price['shop_id']]['cost'] = $price['cost'];
				$pricelistArr[$price['shop_id']]['measurement_name'] = $price['measurement_name'];
			}
			$i = 0;
			foreach ($shoplisting as $shop){ 
				$cost_disp_text ='';
				$response[$i]['mainservice_id']= $mainservice_id;
				$response[$i]['category_id']= $category_id;
				$response[$i]['subcategory_id']= $subcategory_id;
				$response[$i]['shop_id']= $shop['shop_id'];
				$response[$i]['shop_name']= $shop['shop_name'];
				$response[$i]['cost']='';
				$response[$i]['measurement_name']='';
				
				if(isset($pricelistArr[$shop['shop_id']]['cost'])){
					$response[$i]['cost']=$pricelistArr[$shop['shop_id']]['cost'];
					$cost_disp_text .=$pricelistArr[$shop['shop_id']]['cost'];
				}
				if(isset($pricelistArr[$shop['shop_id']]['measurement_name'])){
					$response[$i]['measurement_name']=$pricelistArr[$shop['shop_id']]['measurement_name'];
					$cost_disp_text .= " / ".$pricelistArr[$shop['shop_id']]['measurement_name'];
				}
				$response[$i]['cost_display_text']=$cost_disp_text;
				$response[$i]['subcategory_image'] =$_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl())."/backend/web/". str_replace("image","thumb",$shop['shop_image']);
				
				$i++;
			}
		}
		return $response;
	}
	
	public function loginAppUser($username,$password){
		$userData = array();
		$user = AppUser::findOne(array("email" => $username,"password"=>md5($password)));
		if(!empty($user)){
			$userData['app_user_id'] = $user->app_user_id;
			$userData['email'] = $user->email;
			$userData['mobile'] = $user->mobile;
			$userData['first_name'] = ucfirst($user->first_name);
			$userData['last_name'] = ucfirst($user->last_name);
			$userData['last_password_changed'] = ucfirst($user->last_password_changed);
			$userData['name'] = ucfirst($user->first_name) .' '.ucfirst($user->last_name);
		}
		return $userData;
	}
	
	public function isDuplicateEmail($email){
		$appUserId =0;
		$user = AppUser::findOne(array("email" => $email));
		if(!empty($user)){
			$appUserId = $user->app_user_id;
		}
		return $appUserId;
	}
	public function registrationAppUser($userData){
		$appUserId = 0;
		$appUSerModel = new AppUser();
		$appUSerModel->first_name =$userData->first_name;
		$appUSerModel->last_name = $userData->last_name;
		$appUSerModel->email = $userData->email;
		if(isset($userData->address)){
			$appUSerModel->address = $userData->address;
		}
		if(isset($userData->location)){
			$appUSerModel->location = $userData->location;
		}
		$appUSerModel->mobile = $userData->mobile;
		$appUSerModel->password = md5($userData->password);
		$appUSerModel->last_password_changed = $userData->last_password_changed;
		$appUSerModel->created_on = date('Y-m-y H:i:s');
		if($appUSerModel->save()){
			$appUserId = $appUSerModel->app_user_id;
		}
		return $appUserId;
	}
	public function userBidApplication($bidData,$app_user_id){ 
		$userBidModel = new AppUserBid();
		$userBidModel->app_user_id =  $app_user_id;
		$userBidModel->mainservice_id =$bidData->mainservice_id;
		$userBidModel->category_id = $bidData->category_id;
		$userBidModel->subcategory_id = $bidData->subcategory_id;
		$userBidModel->measurement_id = $bidData->measurement_id;
		$userBidModel->location = $bidData->location;
		$userBidModel->quantity = $bidData->quantity;
		$userBidModel->created_on = date('Y-m-d H:i:s');
		$userBidModel->is_active = 1;
		if($userBidModel->save()){
			return true;
		}else{
			return false;
		}
	}
	
	public function getShopDetails($subcategory_id,$shop_id){
		$responseData = array();
		$shopDetail = Shop::findOne($shop_id);
		if(!empty($shopDetail)){
			$responseData['shop_name'] = ucwords($shopDetail->shop_name);
			
			$responseData['shop_image'] =$_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl())."/backend/web/". $shopDetail->shop_image;
			$responseData['price'] = '';
			$location = '';
			$sep='';
			if(!empty($shopDetail->address)){
				$location .=$sep.$shopDetail->address;
				$sep=', ';
			}
			if(!empty($shopDetail->city_id) && $shopDetail->city_id > 0 ){
				$cityDetail = City::findOne($shopDetail->city_id);
				$location .=$sep.$cityDetail->city_name;
				$sep=', ';
			}
			if(!empty($shopDetail->state_id) && $shopDetail->state_id > 0 ){
				$stateDetail = State::findOne($shopDetail->state_id);
				$location .=$sep.$stateDetail->state_name;
				$sep=', ';
			}
			if(!empty($shopDetail->country_id) && $shopDetail->country_id > 0 ){
				$countryDetail = Country::findOne($shopDetail->country_id);
				$location .=$sep.$countryDetail->country_name;
				$sep=', ';
			}
			$rateDetails = $this->commonModelObj->priceListing($subcategory_id,array($shop_id));
			if(!empty($rateDetails)){
				foreach ($rateDetails as $rate){
					$responseData['price'] = $rate['cost']." / ".$rate['measurement_name'];
				}
			}
			$responseData['location'] = $location;
		}
		return $responseData;
	}
	
	public function getMeasurementlist($category_id){
		$response = array();
		$measurementList = $this->commonModelObj->getMeasurement($category_id,1);
		foreach ($measurementList as $measurement){
			$response[$measurement['measurement_id']]= $measurement['measurement_name'];
		}
		return $response;
	}
	
	public function getBidList($app_user_id){
		$response = array();
		$allUserBid = $this->commonModelObj->getBid($app_user_id);
		if(!empty($allUserBid)){
			
			$categoryList = array();
			$subcategoryList = array();
			$measurementList = array();
			$response = array();
			$category = $this->commonModelObj->getCategory();
			$subcategory = $this->commonModelObj->getSubcategory();
			$measurement =$this->commonModelObj->getMeasurementList();
			
			foreach($category as $cat){
				$categoryList[$cat['category_id']] = $cat;
			}
			foreach($subcategory as $subcat){
				$subcategoryList[$subcat['subcategory_id']] = $subcat;
			}
			foreach ($measurement as $measur){
				$measurementList[$measur['measurement_id']] = $measur['measurement_name']."( ".$measur['measurement_sort_name']." )";
			}
			$i=0; 
			foreach ($allUserBid as $userbid){  
				$response[$i]['app_user_bid_id'] = $userbid->app_user_bid_id;
				$response[$i]['app_user_id'] = $userbid->app_user_id;
				$response[$i]['category_id'] = $userbid->category_id;
				$response[$i]['subcategory_id'] = $userbid->subcategory_id;
				$response[$i]['measurement_id'] = $userbid->measurement_id;
				if(isset($categoryList[$userbid->category_id]['category_name'])){
					$response[$i]['category_name'] = $categoryList[$userbid->category_id]['category_name'];
				}
				if(isset($subcategoryList[$userbid->subcategory_id]['subcategory_name'])){
					$response[$i]['subcategory_name'] = $subcategoryList[$userbid->subcategory_id]['subcategory_name'];
				}
				if(isset($measurementList[$userbid->measurement_id])){
					$response[$i]['measurement_name'] = $measurementList[$userbid->measurement_id];
				}
				
				$response[$i]['location'] = $userbid->location;
				$response[$i]['quantity'] = $userbid->quantity;
				$i++;
			}
		}
		return $response;
	}
	
	public function getBidDetails($app_user_id,$app_user_bid_id){
		$categoryList = array();
		$subcategoryList = array();
		$measurementList = array();
		$shopList = array();
		$response = array();
		$bidReplyArr = array();
		$category = $this->commonModelObj->getCategory();
		$subcategory = $this->commonModelObj->getSubcategory();
		$measurement =$this->commonModelObj->getMeasurementList();
		$shopBidReply = $this->commonModelObj->getShopBidReply($app_user_bid_id);
		$shop = $this->commonModelObj->getShop();
		foreach($category as $cat){
			$categoryList[$cat['category_id']] = $cat;
		}
		foreach($subcategory as $subcat){
			$subcategoryList[$subcat['subcategory_id']] = $subcat;
		}
		foreach ($measurement as $measur){
			$measurementList[$measur['measurement_id']] = $measur['measurement_name']."( ".$measur['measurement_sort_name']." )";
		}
		foreach ($shop as $sh){
			$shopList[$sh['shop_id']]['first_name'] = $sh['first_name'];
			$shopList[$sh['shop_id']]['last_name'] = $sh['last_name'];
			$shopList[$sh['shop_id']]['shop_name'] = $sh['shop_name'];
		}
		$userBidDetails = $this->commonModelObj->getUserBidListByBidId($app_user_id,$app_user_bid_id);
		
		$bidDetails['app_user_bid_id']=$userBidDetails->app_user_bid_id;
		$bidDetails['app_user_id']=$userBidDetails->app_user_id;
		$bidDetails['user_image']='';
		$bidDetails['category_id']=$userBidDetails->category_id;
		$bidDetails['subcategory_id']=$userBidDetails->subcategory_id;
		$bidDetails['measurement_id']=$userBidDetails->measurement_id;
		if(isset($categoryList[$userBidDetails->category_id]['category_name'])){
			$bidDetails['category_name']=$categoryList[$userBidDetails->category_id]['category_name'];
		}
		if(isset($subcategoryList[$userBidDetails->subcategory_id]['subcategory_name'])){
			$bidDetails['subcategory_name']=$subcategoryList[$userBidDetails->subcategory_id]['subcategory_name'];
		}
		if(isset($measurementList[$userBidDetails->measurement_id])){
			$bidDetails['measurement_name']=$measurementList[$userBidDetails->measurement_id];
		}
		$bidDetails['location']=$userBidDetails->location;
		$bidDetails['quantity']=$userBidDetails->quantity;
		
		$s=0;
		foreach($shopBidReply as $sbr){
			$bidReplyArr[$s]['vender_id'] =$sbr['shop_id'];
			$bidReplyArr[$s]['cost'] = $sbr['cost'];
			if($shopList[$sbr['shop_id']]['first_name']){
				$bidReplyArr[$s]['first_name'] = $shopList[$sbr['shop_id']]['first_name'];
			}
			if($shopList[$sbr['shop_id']]['last_name']){
				$bidReplyArr[$s]['last_name'] = $shopList[$sbr['shop_id']]['last_name'];
			}
			if($shopList[$sbr['shop_id']]['shop_name']){
				$bidReplyArr[$s]['shop_name'] = $shopList[$sbr['shop_id']]['shop_name'];
			}
			$s++;
		}
		$response['biddetail'] = $bidDetails;
		$response['venderlist'] = $bidReplyArr;
		return $response;
	}
	
	public function getShopImage($shop_id){
		$response = array();
		$subcategoryImageList = array();
		$subcategoryIdsWithShop = $this->commonModelObj->getRatedSubcategoryIdByShopId($shop_id);
		$shopDetail = $this->commonModelObj->getShopDetail($shop_id);
		$response['vender_id'] = $shopDetail->shop_id;
		$response['first_name'] = $shopDetail->first_name;
		$response['last_name'] = $shopDetail->last_name;
		$response['shop_name'] = $shopDetail->shop_name;
		$response['email'] = $shopDetail->email;
		$response['mobile'] = $shopDetail->mobile_number;
		if(isset($subcategoryIdsWithShop[$shop_id]) && !empty($subcategoryIdsWithShop[$shop_id])){
			$subcategoryids= $subcategoryIdsWithShop[$shop_id];
			$subcategoryImage = $this->commonModelObj->getSubcategoryByIds($subcategoryids);
			$i=0;
			foreach ($subcategoryImage as $subcateImg){
				$subcategoryImageList[$i]['subcategory_image'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].str_replace("/admin","", Yii::$app->getUrlManager()->getBaseUrl())."/backend/web/". str_replace("image","thumb",$subcateImg['subcategory_image']);
				$i++;
			}
		}
		 $response['imagelist']=$subcategoryImageList;
		 return $response;
	}
	
	public function userOrder($orderData,$app_user_id){
		$response = array();
		$orderId = $this->commonModelObj->saveOrderData($orderData,$app_user_id);
		$response = $this->userOrderList($app_user_id);
		return $response;
	}
	
	public function userOrderList($app_user_id){
		$categoryList = array();
		$subcategoryList = array();
		$measurementList = array();
		$response = array();
		$orderlist= array();
		$category = $this->commonModelObj->getCategory();
		$subcategory = $this->commonModelObj->getSubcategory();
		$measurement =$this->commonModelObj->getMeasurementList();
		foreach($category as $cat){
			$categoryList[$cat['category_id']] = $cat;
		}
		foreach($subcategory as $subcat){
			$subcategoryList[$subcat['subcategory_id']] = $subcat;
		}
		foreach ($measurement as $measur){
			$measurementList[$measur['measurement_id']] = $measur['measurement_name']."( ".$measur['measurement_sort_name']." )";
		}
		$orderlist = $this->commonModelObj->getOrderList($app_user_id);
		
		$i=0;
		foreach ($orderlist as $orderData){
			$response[$i]['order_id']=$orderData->order_id;
			$response[$i]['category_id']=$orderData->category_id;
			$response[$i]['subcategory_id']=$orderData->subcategory_id;
			$response[$i]['measurement_id']=$orderData->measurement_id;
			if(isset($categoryList[$orderData->category_id]['category_name'])){
				$response[$i]['category_name']=$categoryList[$orderData->category_id]['category_name'];
			}
			if(isset($subcategoryList[$orderData->subcategory_id]['subcategory_name'])){
				$response[$i]['subcategory_name']=$subcategoryList[$orderData->subcategory_id]['subcategory_name'];
			}
			if(isset($measurementList[$orderData->measurement_id])){
				$response[$i]['measurement_name']=$measurementList[$orderData->measurement_id];
			}
			$response[$i]['quantity'] = $orderData->quantity;
			$response[$i]['location'] = $orderData->location;
			$i++;
		}
		return $response;
	}
}
