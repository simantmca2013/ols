<?php

namespace app\models;

use Yii;
use common\models\Driver;


class Drivermodelsupport extends \yii\db\ActiveRecord
{
	public function isDuplicateEmail($email){
		$driver_id =0;
		$driver = Driver::findOne(array("email" => trim($email)));
		if(!empty($driver)){
			$driver_id = $driver->driver_id;
		}

		return $driver_id;
	}

	public function login($email,$password){
		$driver = Driver::findOne(array("email" => trim($email),"password"=>trim(md5($password))));
		return $driver;
	}
	
	public function doRegistration($requestData){
		$response = array();
		$driverModel = new Driver();
		foreach ($requestData as $fieldKey => $fieldValue){
			$driverModel->$fieldKey = $fieldValue;
		}
		if($driverModel->save()){
			$response['status'] =1;
			$response['driver_id'] =$driverModel->driver_id;
			$response['first_name'] =$driverModel->first_name;
			$response['last_name'] =$driverModel->last_name;
			$response['full_name'] =ucfirst($driverModel->first_name)." ".ucfirst($driverModel->last_name);
			$response['mobile'] =$driverModel->mobile_number;
			$response['email'] =$driverModel->email;
			$response['last_name'] =$driverModel->last_name;
			$response['api_key'] = base64_encode($driverModel->driver_id."#".Yii::$app->params ['vender_api_key']."#".$driverModel->last_password_changed);
		
		}else{
			$response['status'] =0;
		}
		return $response;
	}
	
	public function getProfileDetails($driver_id) {
		return Driver::findOne($driver_id);
	}
}