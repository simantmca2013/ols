<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property string $comment_id
 * @property string $news_id
 * @property string $user_id
 * @property string $comment_text
 * @property string $created_on
 * @property integer $is_active
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'user_id', 'is_active'], 'integer'],
            [['comment_text'], 'string'],
            [['created_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Comment ID',
            'news_id' => 'News ID',
            'user_id' => 'User ID',
            'comment_text' => 'Comment Text',
            'created_on' => 'Created On',
            'is_active' => 'Is Active',
        ];
    }
}
