<?php

namespace app\models;

use Yii;
use common\models\Shop;
use common\models\UserOrder;

class Commonmodel extends \yii\db\ActiveRecord
{
	
	public function getMainservice(){
		$params=array();
		$join = "" ;
		$condition = "" ;
		
		$condition .=  $join.'m.is_active=:is_active' ;
		$params[':is_active'] = 1;
		$join = ' and ' ;
		$mainservicelist = (new \yii\db\Query())
		->select('m.mainservice_id,m.mainservice_name,m.mainservice_img_path')
		->from('mainservice m')
		->where($condition,$params)
		->all();
		return $mainservicelist;
	}
    /**
     *  All active category
     */
    public  function getCategory($mainservice_id=null){ 
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	 
    	$condition .=  $join.'c.is_active=:is_active' ;
    	$params[':is_active'] = 1;
    	$join = ' and ' ;
    	if(!empty($mainservice_id)){
    		$condition .=  $join.'c.mainservice_id=:mainservice_id' ;
    		$params[':mainservice_id'] = $mainservice_id;
    		$join = ' and ' ;
    	}else{
    		$condition .=  $join.'m.is_defaultservice=:is_defaultservice' ;
    		$params[':is_defaultservice'] = 1;
    		$join = ' and ' ;
    		
    	}
    	$categorylist = (new \yii\db\Query())
    	->select('c.category_id,c.category_name,c.category_image,c.mainservice_id')
    	->from('category c')
    	->Leftjoin('mainservice m','m.mainservice_id=c.mainservice_id')
    	->where($condition,$params)
    	->all();
    	return $categorylist;
    }
    /**
     * Subcategory list
     * @param unknown $category_id
     */
    public function getSubcategory($category_id=null,$subcategory_id=null){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	
    	$condition .=  $join.'sc.is_active=:is_active' ;
    	$params[':is_active'] = 1;
    	$join = ' and ' ;
    	
    	if(!empty($category_id)){
	    	$condition .=  $join.'sc.category_id=:category_id' ;
	    	$params[':category_id'] = $category_id;
	    	$join = ' and ' ;
    	}
    	if(!empty($subcategory_id)){
    		$condition .=  $join.'sc.subcategory_id=:subcategory_id' ;
    		$params[':subcategory_id'] = $subcategory_id;
    		$join = ' and ' ;
    	}
    	$subcategorylist = (new \yii\db\Query())
    	->select('sc.subcategory_id,sc.subcategory_name,sc.subcategory_image,sc.mainservice_id,sc.category_id')
    	->from('subcategory sc')
    	->where($condition,$params)
    	->all();
    	return $subcategorylist;
    }
    
    public function getSubcategoryByIds($subcategoryIds){
    	$join = "" ;
    	$condition = "" ;
    	
    	$condition .=  $join.'s.is_active=:is_active' ;
    	$params[':is_active'] = 1;
    	$join = ' and ' ;
    	 
    	$condition .=  $join.'s.subcategory_id IN ('. implode(',', $subcategoryIds) .' )';
    	$join = ' and ' ;
    	
    	$subcategorylist = (new \yii\db\Query())
    	->select('s.subcategory_id,s.subcategory_image')
    	->from('subcategory s')
    	->where($condition,$params)
    	->all();
    	return $subcategorylist;
    }
    /**
     * 
     * @param unknown $mainservice_id
     * @param unknown $category_id
     * @param unknown $subcategory_id
     */
    public function getShopidlist($mainservice_id,$category_id,$subcategory_id){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    	
    	$condition .=  $join.'s.mainservice_id=:mainservice_id' ;
    	$params[':mainservice_id'] = $mainservice_id;
    	$join = ' and ' ;
    	 
    	$condition .=  $join.'s.category_id=:category_id' ;
    	$params[':category_id'] = $category_id;
    	$join = ' and ' ;
    	
    	$condition .=  $join.'s.subcategory_id=:subcategory_id' ;
    	$params[':subcategory_id'] = $subcategory_id;
    	$join = ' and ' ;
    	
    	$shopidlist = (new \yii\db\Query())
    	->select('s.mainservice_id,s.category_id,s.subcategory_id,s.shop_id')
    	->from('service s')
    	->where($condition,$params)
    	->all();
    	return $shopidlist;
    }
    
    public function shoplisting($shopids){
    	$join = "" ;
    	$condition = "" ;
    	 
    	$condition .=  $join.'s.is_active=:is_active' ;
    	$params[':is_active'] = 1;
    	$join = ' and ' ;
    	
    	$condition .=  $join.'s.shop_id IN ('. implode(',', $shopids) .' )';
    	$join = ' and ' ;
    	
    	$shoplist = (new \yii\db\Query())
    	->select('s.shop_id,s.shop_name,s.shop_image')
    	->from('shop s')
    	->where($condition,$params)
    	->all();
    	return $shoplist;
    }
    
    public function priceListing($subcategory_id,$shopids){
    	$join = "" ;
    	$condition = "" ;
    	
    	$condition .=  $join.'r.subcategory_id=:subcategory_id' ;
    	$params[':subcategory_id'] = $subcategory_id;
    	$join = ' and ' ;
    	
    	$condition .=  $join.'r.shop_id IN ('. implode(',', $shopids) .' )';
    	$join = ' and ' ;
    	 
    	$pricelist = (new \yii\db\Query())
    	->select('r.measurement_id,r.shop_id,r.subcategory_id,r.cost,m.measurement_name')
    	->from('rate r')
    	->Leftjoin('measurement m','m.measurement_id=r.measurement_id')
    	->where($condition,$params)
    	->all();
    	return $pricelist;
    }
    
    
    public function getMeasurement($category_id=null,$is_active=null){
    	$join = "" ;
    	$condition = "" ;
    	 
    	$condition .=  $join.'sm.category_id=:category_id' ;
    	$params[':category_id'] = $category_id;
    	$join = ' and ' ;
    	 
    	if(!empty($is_active)){
    		$condition .=  $join.'m.is_active=:is_active' ;
    		$params[':is_active'] = $is_active;
    		$join = ' and ' ;
    	}
    	
    	$measurementlist = (new \yii\db\Query())
    	->select('m.measurement_name,m.measurement_id')
    	->from('servicemeasurement sm')
    	->Leftjoin('measurement m','m.measurement_id=sm.measurement_id')
    	->where($condition,$params)
    	->all();
    	return $measurementlist;
    }
   
    public function getMeasurementList(){
    	$params=array();
    	$join = "" ;
    	$condition = "" ;
    
    	$condition .=  $join.'m.is_active=:is_active' ;
    	$params[':is_active'] = 1;
    	$join = ' and ' ;
    	$measurementlist = (new \yii\db\Query())
    	->select('m.measurement_id,m.measurement_name,m.measurement_sort_name')
    	->from('measurement m')
    	->where($condition,$params)
    	->all();
    	return $measurementlist;
    }
    
    public function getBid($app_user_id){
    	return AppUserBid::findAll(array('app_user_id'=>$app_user_id));
    }
    
    public function getUserBidListByBidId($app_user_id,$app_user_bid_id){
    	return AppUserBid::findOne(array("app_user_id"=>$app_user_id,"app_user_bid_id"=>$app_user_bid_id));
    }
    public function getShopBidReply($app_user_bid_id){
    	$params =array();
    	$join = "" ;
    	$condition = "" ;
    
    	$condition .=  $join.'sbr.app_user_bid_id=:app_user_bid_id' ;
    	$params[':app_user_bid_id'] = $app_user_bid_id;
    	$join = ' and ' ;
    
    	$bidreplyList = (new \yii\db\Query())
    	->select('sbr.*')
    	->from('shop_bid_reply sbr')
    	->where($condition,$params)
    	->all();
    	return $bidreplyList;
    }
    public function getShop(){
    	$params =array();
    	$join = "" ;
    	$condition = "" ;
    
    	$condition .=  $join.'s.is_active=:is_active' ;
    	$params[':is_active'] = 1;
    	$join = ' and ' ;
    
    		
    	$shoplist = (new \yii\db\Query())
    	->select('s.shop_id,s.shop_name,s.first_name,s.last_name')
    	->from('shop s')
    	->where($condition,$params)
    	->all();
    	return $shoplist;
    }
    
    public function getShopDetail($shop_id){
    	return Shop::findOne($shop_id);
    }
    public function getRatedSubcategoryIdByShopId($shop_id){
    	$subcategoryDetail = array();
    	$condition='';
    	$join='';
    	$condition .=  $join.'r.shop_id=:shop_id' ;
    	$params[':shop_id'] = $shop_id;
    	$join = ' and ' ;
    
    	$subcategory = (new \yii\db\Query())
    	->select('r.subcategory_id,r.shop_id')
    	->from('rate r')
    	->where($condition,$params)
    	->groupBy('subcategory_id')
    	->all();
    	 
    	if(!empty($subcategory)){
    		foreach ($subcategory as $subcat){
    			$subcategoryDetail[$subcat['shop_id']][]=$subcat['subcategory_id'];
    		}
    	}
    	return $subcategoryDetail;
    }
    public function saveOrderData($orderData,$app_user_id){ 
    	$orderModel = new UserOrder();
    	$orderModel->shop_id =  $orderData['shop_id'];
    	$orderModel->app_user_id = $app_user_id;
    	$orderModel->category_id =  $orderData['category_id'];
    	$orderModel->subcategory_id =  $orderData['subcategory_id'];
    	$orderModel->measurement_id =  $orderData['measurement_id'];
    	$orderModel->quantity =  $orderData['quantity'];
    	$orderModel->location =  $orderData['location'];
    	if($orderModel->save()){
    		return $orderModel->order_id;
    	}else{
    		return false;
    	}
    }
    
    public function getOrderList($app_user_id){ 
    	return UserOrder::findAll(array("app_user_id"=>$app_user_id));
    }
}
