<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_user".
 *
 * @property string $app_user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $mobile
 * @property string $location
 * @property string $address
 * @property integer $is_active
 */
class AppUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['first_name', 'last_name', 'email', 'password', 'mobile', 'location', 'address'], 'string', 'max' => 156]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_user_id' => 'App User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'mobile' => 'Mobile',
            'location' => 'Location',
            'address' => 'Address',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     * Validate user and return user data
     * @param unknown $api_key_data
     * @return unknown
     */
    public static function validateUser($api_key_data)
    {
    	$condition='';
    	$params=array();
    	$join='';
    	if(isset($api_key_data[0]) && $api_key_data[0]){
    		$condition.=$join.'au.app_user_id=:app_user_id';
    		$params[':app_user_id']=trim($api_key_data[0],'#');
    		$join=' AND ';
    	}
    	if(isset($api_key_data[2]) && $api_key_data[2]){
    		$condition.=$join.'au.last_password_changed=:last_password_changed';
    		$last_login_date=trim($api_key_data[2],"#");
    		$params[':last_password_changed']=date('Y-m-d H:i:s',strtotime($last_login_date));
    		$join=' AND ';
    	}
    	$validateuser = (new \yii\db\Query())
    	->select('au.*')
    	->from('app_user au')
    	->where($condition,$params)
    	->all();
    	return $validateuser;
    }
}
