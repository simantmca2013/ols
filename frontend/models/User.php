<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $is_active
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['name', 'email'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'is_active' => 'Is Active',
        ];
    }
    
    /**
     * Validate user and return user data
     * @param unknown $api_key_data
     * @return unknown
     */
    public function validateUser($api_key_data)
    { 
	    $condition='';
	    $params=array();
	    $join='';
	    if(isset($api_key_data[0]) && $api_key_data[0]){
	    	$condition.=$join.'r.user_id=:user_id';
	    	$params[':user_id']=trim($api_key_data[0],'#');
	    	$join=' AND ';
	    }
	    if(isset($api_key_data[2]) && $api_key_data[2]){
	    	$condition.=$join.'r.last_login=:last_login';
	    	$last_login_date=trim($api_key_data[2],"#"); 
	    	$params[':last_login']=date('Y-m-d H:i:s',strtotime($last_login_date));
	    	$join=' AND ';
	    }
	    $validateuser = (new \yii\db\Query())
	    ->select('r.*')
	    ->from('user r')
	    ->where($condition,$params)
	    ->all();
	    return $validateuser;
    }
}
