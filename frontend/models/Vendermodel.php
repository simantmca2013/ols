<?php

namespace app\models;

use Yii;
use common\models\Shop;
use common\models\Rate;
use common\models\ShopBidReply;

class Vendermodel extends \yii\db\ActiveRecord
{
	public function isDuplicateEmail($email){
		$shop_id =0;
		$shop = Shop::findOne(array("email" => trim($email)));
		if(!empty($shop)){
			$shop_id = $shop->shop_id;
		}
		
		return $shop_id;
	}
	
	public function login($email,$password){
		$shop = Shop::findOne(array("email" => trim($email),"password"=>trim(md5($password))));
		return $shop;
	}
	
	public function getRatedSubcategoryIdByShopId($shop_id){
		$subcategoryDetail = array();
		$condition='';
		$join='';
		$condition .=  $join.'r.shop_id=:shop_id' ;
		$params[':shop_id'] = $shop_id;
		$join = ' and ' ;
		
		$subcategory = (new \yii\db\Query())
	    	->select('r.subcategory_id,r.shop_id')
	    	->from('rate r')
	    	->where($condition,$params)
	    	->groupBy('subcategory_id')
	    	->all();
    	
		if(!empty($subcategory)){
			foreach ($subcategory as $subcat){
				$subcategoryDetail[$subcat['shop_id']][]=$subcat['subcategory_id']; 
			}
		}
		return $subcategoryDetail;
	}
	
	/**
	 *  All active category
	 */
	public  function getCategory($mainservice_id=null){
		$params=array();
		$join = "" ;
		$condition = "" ;
	
		$condition .=  $join.'c.is_active=:is_active' ;
		$params[':is_active'] = 1;
		$join = ' and ' ;
		if(!empty($mainservice_id)){
			$condition .=  $join.'c.mainservice_id=:mainservice_id' ;
			$params[':mainservice_id'] = $mainservice_id;
			$join = ' and ' ;
		}else{
			$condition .=  $join.'m.is_defaultservice=:is_defaultservice' ;
			$params[':is_defaultservice'] = 1;
			$join = ' and ' ;
	
		}
		$categorylist = (new \yii\db\Query())
		->select('c.category_id,c.category_name,c.category_image,c.mainservice_id')
		->from('category c')
		->Leftjoin('mainservice m','m.mainservice_id=c.mainservice_id')
		->where($condition,$params)
		->all();
		return $categorylist;
	}
	/**
	 * Subcategory list
	 * @param unknown $category_id
	 */
	public function getSubcategory($category_id=null,$subcategory_id=null){
		$params=array();
		$join = "" ;
		$condition = "" ;
		 
		$condition .=  $join.'sc.is_active=:is_active' ;
		$params[':is_active'] = 1;
		$join = ' and ' ;
		 
		if(!empty($category_id)){
			$condition .=  $join.'sc.category_id=:category_id' ;
			$params[':category_id'] = $category_id;
			$join = ' and ' ;
		}
		if(!empty($subcategory_id)){
			$condition .=  $join.'sc.subcategory_id=:subcategory_id' ;
			$params[':subcategory_id'] = $subcategory_id;
			$join = ' and ' ;
		}
		$subcategorylist = (new \yii\db\Query())
		->select('sc.subcategory_id,sc.subcategory_name,sc.subcategory_image,sc.mainservice_id,sc.category_id')
		->from('subcategory sc')
		->where($condition,$params)
		->all();
		return $subcategorylist;
	}
	
	public function getUserBidList($app_user_bid_id=null){
		$params=array();
		$join = "" ;
		$condition = "" ;
		if(!empty($app_user_bid_id)){ 
			$condition .=  $join.'ub.app_user_bid_id=:app_user_bid_id' ;
			$params[':app_user_bid_id'] = $app_user_bid_id;
			$join = ' and ' ;
		}
		$userbidlist = (new \yii\db\Query())
		->select('ub.*')
		->from('app_user_bid ub')
		->where($condition,$params)
		->all();
		return $userbidlist;
	}
	
	public function getMeasurement(){
		$params=array();
		$join = "" ;
		$condition = "" ;
		
		$condition .=  $join.'m.is_active=:is_active' ;
		$params[':is_active'] = 1;
		$join = ' and ' ;
		$measurementlist = (new \yii\db\Query())
		->select('m.measurement_id,m.measurement_name,m.measurement_sort_name')
		->from('measurement m')
		->where($condition,$params)
		->all();
		return $measurementlist;
	}
	
	public function getUserDetails($userId){
		return AppUser::findOne($userId);
	}
	
	public function shoplisting(){
		$params =array();
		$join = "" ;
		$condition = "" ;
	
		$condition .=  $join.'s.is_active=:is_active' ;
		$params[':is_active'] = 1;
		$join = ' and ' ;
		
		 
		$shoplist = (new \yii\db\Query())
		->select('s.shop_id,s.shop_name,s.first_name,s.last_name')
		->from('shop s')
		->where($condition,$params)
		->all();
		return $shoplist;
	}
	
	public function getShopBidReply($app_user_bid_id){
		$params =array();
		$join = "" ;
		$condition = "" ;
		
		$condition .=  $join.'sbr.app_user_bid_id=:app_user_bid_id' ;
		$params[':app_user_bid_id'] = $app_user_bid_id;
		$join = ' and ' ;
		
		$bidreplyList = (new \yii\db\Query())
		->select('sbr.*')
		->from('shop_bid_reply sbr')
		->where($condition,$params)
		->all();
		return $bidreplyList;
	}
	
	public function submitBid($app_user_bid_id,$shop_id,$cost){
		$vendre_bid_reply_id =0;
		$isBidExistForVender = ShopBidReply::findOne(array('app_user_bid_id'=>$app_user_bid_id,"shop_id"=>$shop_id));
		if(!empty($isBidExistForVender)){
			$bidReplyModel = ShopBidReply::findOne($isBidExistForVender->vendre_bid_reply_id);
			$bidReplyModel->app_user_bid_id= $app_user_bid_id;
			$bidReplyModel->shop_id = $shop_id;
			$bidReplyModel->cost= $cost;
			$bidReplyModel->update();
			$vendre_bid_reply_id = $isBidExistForVender->vendre_bid_reply_id;
		}else{ 
			$bidReplyModel = new ShopBidReply();
			$bidReplyModel->cost = $cost;
			$bidReplyModel->app_user_bid_id= $app_user_bid_id;
			$bidReplyModel->shop_id = $shop_id;
			if($bidReplyModel->save()){
				$vendre_bid_reply_id = $bidReplyModel->vendre_bid_reply_id;
			}
		}
		return $vendre_bid_reply_id;
	}
}
