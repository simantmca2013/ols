<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_user_bid".
 *
 * @property string $app_user_bid_id
 * @property string $app_user_id
 * @property string $mainservice_id
 * @property string $category_id
 * @property string $subcategory_id
 * @property string $shop_id
 * @property string $location
 * @property string $created_on
 * @property integer $is_active
 */
class AppUserBid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user_bid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['app_user_id', 'mainservice_id', 'category_id', 'subcategory_id'], 'integer'],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_user_bid_id' => 'App User Bid ID',
            'app_user_id' => 'App User ID',
            'mainservice_id' => 'Mainservice ID',
            'category_id' => 'Category ID',
            'subcategory_id' => 'Subcategory ID',
           
            'location' => 'Location',
            
            
        ];
    }
}
